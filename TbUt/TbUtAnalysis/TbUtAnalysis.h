/*! \brief TbUtAnalysis class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
 *  \author Wojtek Krupa
*   \date November-2018
*/

#ifndef TB_UTX_ANALYSIS_H
#define TB_UTX_ANALYSIS_H

#include "TbGaudi.h"
#include "TbAnalysis.h"
#include "TbResults.h"
#include "DutBinning.h"
#include "ChargeFitter.h"
#include <vector>
#include <map>

////////////////////////////////////////////////////////////////////////////////////
///
class TbUtAnalysis : public TbAnalysis {
	private:
		///
        void CCE_MpvMapMaker();

	public:
		///
		TbUtAnalysis()=default;

		///
		~TbUtAnalysis()=default;

        //___________________________________________________________
		/// Perform CCE analysis
		void Ana_CCE();


};
#endif	// TB_VELOPIX_ANALYSIS_H