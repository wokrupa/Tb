//
// Created by brachwal on 16.10.18.
//

#ifndef TBLHCBANA_IRRAD_H
#define TBLHCBANA_IRRAD_H

// std libriaries
#include <iostream>
#include <memory>

#include "IRRADProfile.h"

class IRRAD : public IRRADProfile {

    public:
        IRRAD();
        ~IRRAD()=default;

        virtual void Initialize() override;
};
#endif //TBLHCBANA_IRRAD_H
