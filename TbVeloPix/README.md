# The LHCb VeloPix test beam analysis code

## Implemented utilities

### IRRAD fluence profile mapping
By inheriting from the TbGaudi general IRRAD profile, the per sensor details are introduced.
That handle the elliptical fluence profile alignment.

### Charge fitter
By handling the data trough the RooDataSets a dedicated charge fitter is implemented 
(inheriting from the TbGaudi general Fitter). This implements the Landau-Gaussian pdf.

## Implemented analysis methods

### Charge Collection Efficiency
* [JIRA board](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=6440&projectKey=TBVELOPIX)
* TWIKI page: to be defined...
