#include "TbVeloPixAnalysis.h"
#include "IRRAD.h"
#include "TbGaudi.h"
#include "DutUtils.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::CCE_IRRAD_MpvMapMaker()
{

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::MPVMapMaker is executed.");

    for (auto iTbJob : TbJobs() ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Create appropriate fluence profile for the DUT binning
        iTbJob->DutPtr()->DutBinningPtr()->SetFluenceProfile(new IRRAD()); // use here GetDutInfo("FluenceProfileType")

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinningHist("Charge",500,0.,50000.);
        iTbJob->DefineDutBinningDataSet("Charge",0.,50000.,"electrons");
        iTbJob->DefineDutBinningDataSetType("Charge","Signal",1);   // RooCategory::defineType(...)
        iTbJob->DefineDutBinningDataSetType("Charge","Noise",0);    // RooCategory::defineType(...)

        //_______________________________________________________________________________________
        // Define loop counters
        //iTbJob->DefineEventCounter("Spill untagged",0);
        //iTbJob->DefineEventCounter("Out of spill",1);
        //_______________________________________________________________________________________

        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("Clusters");
        Int_t var_row[200];   // max from KEPLER::m_maxclustersize
        Int_t var_col[200];
        UInt_t var_clSize;
        Int_t var_clisInSpill;
        Bool_t var_clIsTrk;
        Double_t var_clCharge;

        tree->SetBranchAddress("hRow", var_row);
        tree->SetBranchAddress("hCol", var_col);
        tree->SetBranchAddress("clSize", &var_clSize);
        tree->SetBranchAddress("clCharge", &var_clCharge);
        tree->SetBranchAddress("clIsTracked", &var_clIsTrk);
        //tree->SetBranchAddress("clisInSpill", &var_clisInSpill);


        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = tree->GetEntriesFast();
        Nentries /= 100;
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetConfig("RunNumber");

        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            if (var_clCharge < 1000) continue;    // unphysical entries
            if (var_clSize > 20)     continue;    // max clusters size
            if (!var_clIsTrk)        continue;    // process only associated clusters

            //Take into account the cluster size requirement
            //if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;

            // Take into account each pixel from the cluster, but keep events where all pixes belongs
            // to single DUT bin (cluster is laying in the single DUT bin)
            //if (!iTbJob->IsClusterInsideDutBin(var_col, var_row, var_clSize)) continue;
            // UWAGA: to generuje zanizona statystyke na liniach binowania,
            // widac ze dla elliptic jest odwrocony x z y ! todo: debug

            if(it%10000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }

            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/pixel statistic hit map

            // Fill histograms, RooDataSets, etc.
            auto dutBinNumber = iTbJob->DutBinNumber(var_row[0],var_col[0]); //all pixels lay in the same bin!
            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_clCharge);
                iTbJob->DutDataSetRCatPtr("Charge", dutBinNumber)->setIndex(1);   //temporary, set everything as signal
                iTbJob->DutDataSetRRVarPtr("Charge", dutBinNumber)->setVal(var_clCharge);
                iTbJob->AddToDutDataSet("Charge", dutBinNumber);
            }
        }

        iTbJob->PrintAllCountersStatistics();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        iTbJob->CreateDutStatisticHist();       // hists are automatically exported to TbResults

        //_______________________________________________________________________________________
        // write histograms into .root file, if requested by the User.
        if(DutRootUtils::WriteHistToNTuple()) {
            // ... all types, TODO:: includ TH2D of statistic if it was being created
            iTbJob->WriteEventLoopHistToNTuple("Charge");
        }

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>();
        chargeFitter->SetData(iTbJob, "Charge", FitDataType::RooDataSet);
        chargeFitter->Roofit(); // run fitting

        // fitting of other data and routines can be called here, simply:
        // 1. SetData
        // 2. Run fitting method, e.g. Roofit() or other...

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobs() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::MPVMapMaker THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///