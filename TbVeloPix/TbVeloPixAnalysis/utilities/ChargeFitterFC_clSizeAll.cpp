#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaryies
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "TLegend.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::FC_clSizeAll() {
    //gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: FC (all cluster sizes) is executed...");

    // _____________________________________________________________
    // baza danych rozkladu Landaua
    std::ifstream fin;
    fin.open(TbGaudi::ProjectLocation()+"/TbVeloPix/TbVeloPixAnalysis/utilities/Landau2.txt");
    int row=1980;
    int col=800;
    double** Landau=new double*[row];
    for(int i=0; i<row; i++)
        Landau[i]=new double[col];
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++)
            fin>>Landau[i][j];
    }
    fin.close();

    // ____________________________________________________________
    // Get data to be fitted.
    auto myHist = ITHistPtr();

    auto canTest = new TCanvas(TString("c"+TString(myHist->GetName())), myHist->GetTitle(),700,600);
    myHist->Draw();

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    std::cout << "[INFO]:: ChargeFitter:: FC:: Fit nEntries threshold: " << TbFitter::NEntriesFitThreshold() << std::endl;

    //
    // _____________________________________________________________
    // FC routines come here...

int n=50;  //ilosc kwiatow
double pop[50][6]; double pop2[50][6];
double koszt[n]; double koszt2; double kosztEnd[n];

int Nbins=myHist->GetNbinsX();
double cl_size=2;
//double Skx=hist->GetBinCenter(Nbins)/100;
double Skx=1; double Sky=1;
double bins[Nbins]; double values[Nbins];
double Xt[Nbins]; double Yt[Nbins];
std::cout<<"Width of the bin:    "<<myHist->GetBinWidth(0)<<std::endl;
std::cout<<"Center of the 1st bin:    "<<myHist->GetBinCenter(0)<<std::endl;
for(int i=0; i<Nbins; i++){  //uzupelniamy tablice robocze + skalowanie
	bins[i]=myHist->GetBinCenter(i);
	values[i]=myHist->GetBinContent(i);
	Xt[i]=bins[i]/Skx;
	Yt[i]=values[i]/Sky;
}
std::cout<<"Processing... "<<std::endl;

double binMPV; int itMPV; double yMPV; int FirstEffective;
//Update - szukamy MPV
for(int i=2; i<(Nbins-10); i++){
if(Yt[i]>Yt[i+1] && Yt[i]>Yt[i+2] && Yt[i]>Yt[i+3]){
binMPV=Xt[i];
yMPV=Yt[i];
itMPV=i;
break;
}}
FirstEffective=0;//itMPV-1;
int P=40;
if(Nbins>100) P=(int)(((double)Nbins)/100 *(double)P);
for(int i=0; i<P; i++){  //w procesie dopasowania bierze udzial co czwarty rekord
	Xt[i]=Xt[FirstEffective+2*i];
	Yt[i]=Yt[FirstEffective+2*i];
}

for(int i=0; i<n; i++){           //inicjujemy poczatkowa populacje parametrow
	pop[i][0]=3+2*round(100*((double)rand()/RAND_MAX))/100;
	pop[i][1]=1000+500*((double)rand()/RAND_MAX-0.5);
	pop[i][2]=2+2*((double)rand()/RAND_MAX-0.5);  //sigma gauss
	pop[i][3]=10+10*((double)rand()/RAND_MAX-0.5);  //mi gauss
	pop[i][4]=100000+100000*((double)rand()/RAND_MAX-0.5);  //first conv LG
	pop[i][5]=100000+100000*((double)rand()/RAND_MAX-0.5);  //second conv LG
}
double Iy[P];
double It[P];
double kosztT;
for(int j=0; j<n; j++){                
	kosztT=0;
	for(int k=0; k<P; k++){            //funkcja obciazenia poczatkowych parametrow
		double I=0;
		for(int q=0; q<800; q++){
		I=I+pop[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop[j][3])*(Xt[k]+10.2-0.2*q-pop[j][3])/(2*pop[j][2]*pop[j][2]))* Landau[(int)(100*pop[j][0]-10)][q]*0.2;
		}Iy[k]=I;
              }
		for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=Iy[i]*Iy[k-i]/pop[j][4];}
		It[k]=I;              
		}
		for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=It[i]*Iy[k-i]/pop[j][5];}
		kosztT=kosztT+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		}
		for(int k=0;k<P;k++){
		Iy[k]=0;
		It[k]=0;}
	koszt[j]=kosztT;
}
for(int i=0; i<600; i++){

	for(int j=0; j<n; j++){
        	double e=2*(double)rand()/RAND_MAX-1;
                int z1=round((n-1)*(double)rand()/RAND_MAX); int z2=round((n-1)*(double)rand()/RAND_MAX);
                
		for(int p=0; p<6; p++)
                    pop2[j][p]=pop[j][p]+e*(pop[z1][p]-pop[z2][p]);   //<-rownanie algorytmu
                
		pop2[j][0]=round(pop2[j][0]*100)/100;            
                if(pop2[j][0]<0.2)     //Landau c
                    pop2[j][0]=0.2;
                if(pop2[j][0]>12)
                    pop2[j][0]=12;
                if(pop2[j][1]>6000)    //gain
                    pop2[j][1]=6000;
                if(pop2[j][1]<10)
                    pop2[j][1]=10;
                if(pop2[j][2]>11)     //Gauss sigma
                    pop2[j][2]=11;          
                if(pop2[j][2]<0)
                    pop2[j][2]=0;
                if(pop2[j][3]>120)      //Gauss mean
                    pop2[j][3]=120;
                if(pop2[j][3]<5)
                    pop2[j][3]=5;
		if(pop2[j][4]>10000000)      //Gauss mean
                    pop2[j][4]=10000000;
                if(pop2[j][4]<100)
                    pop2[j][4]=100;
                if(pop2[j][5]>1000000)      //Gauss mean
                    pop2[j][5]=1000000;
                if(pop2[j][5]<10)
                    pop2[j][5]=10;
                koszt2=0;
                for(int k=0; k<P; k++){
                    double I=0;
                    for(int q=0; q<800; q++){
                        I=I+pop2[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop2[j][3])*(Xt[k]+10.2-0.2*q-pop2[j][3]) /(2*pop2[j][2]*pop2[j][2]))*Landau[(int)(100*pop2[j]	[0]-10)][q]*0.2;}
		Iy[k]=I;
                }
		for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=Iy[i]*Iy[k-i]/pop2[j][4];}
		It[k]=I;              
		}
		for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=It[i]*Iy[k-i]/pop2[j][5];}
		koszt2=koszt2+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		}
		for(int k=0;k<P;k++){
		Iy[k]=0;
		It[k]=0;}
		if(koszt2<koszt[j]){
                for(int it=0; it<5; it++)    //aktualizacja lepszych rozwiazan
                    pop[j][it]=pop2[j][it];                
		    koszt[j]=koszt2;	
                }
	} //konczy petle przebiegu po populacji
} //konczy petle iteracji algorytmu

double value= koszt[0];
double wskaznik= 0;
int no;
for(int j=0; j<n; j++){
	double kosztT=0;
        for(int k=0; k<P; k++){
            	double I=0;
            	for(int q=0; q<800; q++){
                	I=I+pop[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop[j][3])*(Xt[k]+10.2-0.2*q-pop[j][3])/(2*pop[j][2]*pop[j][2]))* Landau[(int)(100*pop[j][0]-10)][q]*0.2;}
		Iy[k]=I;}
				for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=Iy[i]*Iy[k-i]/pop[j][4];}
		It[k]=I;              
		}
		for(int k=0;k<P;k++){
		double I=0;
		for(int i=0;i<P;i++){
			if(k-i+1>0)
				I+=It[i]*Iy[k-i]/pop[j][5];}
		kosztT=kosztT+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		}
		
	kosztEnd[j]=kosztT;
	if(kosztEnd[j]< value){
                value= kosztEnd[j];
                no= j;
        }
}
double c=pop[no][0];
double B=pop[no][1];
double sigma=pop[no][2];
double mi=pop[no][3];
double A=pop[no][4];
double A2=pop[no][5];

std::cout<<"Variance: "<<value/P<<std::endl;
std::cout<<"Landau width: "<<c<<std::endl;
std::cout<<"L-G gain: "<<B<<std::endl;
std::cout<<"Gauss mean(shift): "<<mi<<std::endl;
std::cout<<"Gauss std: "<<sigma<<std::endl;
std::cout<<"Amplitude:  "<<A<<std::endl;

double xx[1000]; double yy[1000]; double dy[900]; double yy2[1000]; double yy3[1000]; double yy4[1000]; double LG2[1000]; double LG3[1000]; double LG4[1000];
for(int i=0;i<1000;i++)
	LG2[i]=0;
double max= Xt[Nbins-1];
int range=(int)(0+1000*(max/1000));
double I=0;
for(int i=0;i<1000;i++){
	I=0;
	xx[i]=0+i*(double(Nbins)/1000);
	for(int q=0;q<800;q++){
		I=I+B*exp(-(xx[i]+10.2-0.2*q-mi)*(xx[i]+10.2-0.2*q-mi) /(2*sigma*sigma))*Landau[(int)round(100*c-10)][q]*0.2;
	}
	yy3[i]=I*Sky;
	yy[i]=I*Sky;

	//yy[i]=TMath::Landau(x,MPV,;
	//cout<<(int)(100*3.4581-10)<<"   "<<(int)(5*(xx[i]+10.2))<<endl;
	xx[i]=xx[i]*Skx;
	//yy3[i]=yy[i]-yy2[i];
	
	yy4[i]=Yt[int(floor(double(i)*Nbins/1000/2))]-yy3[i];
	//cout<<i<<" "<<xx[i]<<"  "<<floor(double(i)*Nbins/1000)<<" "<<Yt[int(floor(double(i)*Nbins/1000))]<<endl;
	}
double globalfit[1000];
for(int t=0;t<1000;t++){
	for(int i=0;i<1000;i++){
	if(t-i+1>0)
	LG2[t]+=yy3[i]*yy3[t-i]/A/25;
	}
}
for(int t=0;t<1000;t++){
	for(int i=0;i<1000;i++){
	if(t-i+1>0)
	LG3[t]+=LG2[i]*yy3[t-i]/A2/25;
	//cout<<yy3[i]<<" "<<yy3[j]<<"  "<<LG2[i]<<endl; 
	}
	globalfit[t]=LG3[t]+LG2[t]+yy3[t];
	//cout<<LG2[t]<<endl;
	
}

for(int i=0;i<1000;i++){
	for(int j=0;j<1000;j++){
		if(i+j<1000)
		LG4[j+i]+=yy3[i]*LG3[j]*(yy3[i]+LG3[j])/2000000000;
		//cout<<yy3[i]<<" "<<yy3[j]<<"  "<<LG2[i]<<endl; 
	}
	//cout<<LG2[i]<<endl;
}
int wsk=0;
for(int i=0; i<900; i++){
	dy[i]=(yy3[49+i]-yy3[51+i])/(2*(double)range/1000);
	if(dy[i-1]<0 && dy[i]>0){wsk=i; break;}
}
double MPV=Skx*((double)(wsk+50)/1000)*range - dy[wsk-1]/(-dy[wsk-1]+dy[wsk]);
double rozdzielczosc=(Xt[2]-Xt[1])/2;
double errMPV=rozdzielczosc/sqrt(12)/sqrt((double)P/4.)*sqrt(value/P/yMPV);
std::cout<<"MPV of fitted LG distribution: "<<MPV<<" Error +/- "<<errMPV<<"  (variance^1/2)"<<std::endl;


TGraph* fit3= new TGraph(1000,xx,yy3);
	fit3->SetName("fit3");
	
        fit3->SetLineColor(6   );
        fit3->SetLineWidth(2);
	fit3->SetMarkerColor(6   );
	fit3->Draw("C");
	TGraph* fit5= new TGraph(1000,xx,LG2);
        fit5->SetLineColor(9   );
	fit5->SetMarkerColor(9   );
        fit5->SetLineWidth(2);
	fit5->SetName("fit5");
	fit5->Draw("C");
	TGraph* fit6= new TGraph(1000,xx,LG3);
        fit6->SetLineColor(kGreen+3   );
	fit6->SetMarkerColor(kGreen+3);
        fit6->SetLineWidth(2);
	fit6->SetName("fit6");
	fit6->Draw("C");
	TGraph* fit7= new TGraph(1000,xx,globalfit);
	//fit7->SetLineStyle(2);
        fit7->SetLineColor(kOrange+9   );
	fit7->SetMarkerColor(kOrange+9   );
        fit7->SetLineWidth(3);
	fit7->SetName("fit7");
	fit7->Draw("C");
  auto legend2 = new TLegend(0.46,0.7,0.95,0.95);
   //legend2->SetHeader("","C"); // option "C" allows to center the header
	legend2->AddEntry(myHist,"ToT histogram for all cluster sizes","f");
	legend2->AddEntry("fit3","Main LG distribution","l");
	legend2->AddEntry("fit3","Single photon","l");
//   legend->AddEntry("fit5","LG #otimes LG","l");
//   legend->AddEntry("fit6","LG #otimes LG #otimes LG","l");
   legend2->AddEntry("fit5","Double photon conversion","l");
   legend2->AddEntry("fit6","Tripple photon conversion","l");
   legend2->AddEntry("fit7","Sum of all","l");
   legend2->Draw("SAME");
	canTest->Update();

    //gROOT->SetBatch(false);
}