/*! \brief ChargeFitter factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date October-2018
*/

#ifndef TB_VELOPIX_CHARGE_FITTER_H
#define TB_VELOPIX_CHARGE_FITTER_H

// std libriaryies
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <memory>

// TbGaudi libraries
#include "TbFitter.h"
#include "Globals.h"

class TbJob;

class ChargeFitter: public TbFitter{

	private:
		///
		void FC_clSizeAll();
		void FC_clSize1();
		void FC_clSize2();
		void FC_clSize3();

	public:

        /// \brief Definition of LanGau fit with RooFit framework, including background modelling
        void RoofitWithOutOfSpillNoise();

    	/// \brief Definition of LanGau fit with RooFit framework
        void Roofit();

		void FC(int clSize){
			if(clSize==-1) FC_clSizeAll();
			if(clSize==1) FC_clSize1();
			if(clSize==2) FC_clSize2();
			if(clSize==3) FC_clSize3();
		}

		/// Standard constructor
		ChargeFitter() = default;

		///
		~ChargeFitter() = default;


};
#endif	// TB_VELOPIX_CHARGE_FITTER_H
