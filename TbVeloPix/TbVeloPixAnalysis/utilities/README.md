# Available Fitters definitions

### Roofit() - for Charge fitting (by Bartek)
TODO: description comes here...

### RoofitWithOutOfSpillNoise() - for Charge fitting (by Kazu)
TODO: description comes here...

### FC(int) - Flower Constancy for Charge fitting (by Pawel Kopciewicz)
* [Flower constancy paper ???](https://google.pl)

Within the **TbGaudi** framework it is available/implemented for the _TH1D_ data type and different cluster sizes:
```
auto chargeFitter =  std::make_unique<ChargeFitter>();
chargeFitter->SetData(iTbJobSharedPtr, "HistName", FitDataType::TH1);
```
```
chargeFitter->FC(-1); // run fitting for all clSize
```
```
chargeFitter->FC(1); // run fitting for clSize = 1
```
```
chargeFitter->FC(2); // run fitting for clSize = 2
```
```
chargeFitter->FC(3); // run fitting for clSize = 3
```

*  FC algorithm for all custer size

TODO: description comes here...

*  FC algorithm for custer size = 1 

TODO: description comes here...

*  FC algorithm for custer size = 2

TODO: description comes here...

*  FC algorithm for custer size = 3

TODO: description comes here...

