#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaryies
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::Roofit() {
    gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: Roofit is executed...");

    // prepare container for the plots
    auto plots = TCanvasMap();

    // prepare container for the set of results
    auto dut_binning_mpv = new DutBinningResultsMap();
    auto dut_binning_LandauSigma = new DutBinningResultsMap();

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    auto nFits = NGoodDataSets();
    auto nBins = TbFitter::RooFitBinning();
    std::cout << "[INFO]:: ChargeFitter:: Fit nEntries threshold: " << TbFitter::NEntriesFitThreshold() << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: Number of DUT bins with nEntries above threshold: " << nFits << std::endl;

    for (int iFit = 0; iFit < nFits; ++iFit) {
        std::cout << "[debug]:: >> iFit " << iFit << std::endl;
        auto rds = IRooDataSetPtr(iFit);
        auto rrv = IRooRealVarPtr(iFit);
        TbGaudi::PrintBanner("INFO", " ChargeFitter:: Start fitting:: "+std::string(rds->GetName())+" (DUT bin "+itos(DutBin(iFit))+")");

        double factor = TbVoltage()/800.;

        // Prepare signal data hist to extract initial numbers for the fitting
        // TODO : make this as a TbFitter method:
        auto hist = std::unique_ptr<TH1F>(static_cast<TH1F*>(rds->createHistogram("SignalHist",
                                                                                  *rrv,
                                                                                  RooFit::Binning(nBins),
                                                                                  RooFit::CutRange("signal") ) ) );

        double MPV = 0.8*hist->GetMean();
        std::cout << "[INFO]:: >> mpv estimate " <<  MPV << std::endl;
        double sigma = hist->GetRMS();
        double mean_low = MPV - 0.2*MPV;
        double mean_high = MPV + 0.4*sigma;

        auto RRV_Gmean =  std::make_unique<RooRealVar>("Gmean", "mean",0);
        auto RRV_Lmean =  std::make_unique<RooRealVar>("MPV", "MPV",MPV, mean_low, mean_high);
        auto RRV_Lsigma = std::make_unique<RooRealVar>("Lsigma", "Lsigma", 250*factor*1.2, 50*factor*0.8,1075*factor*1.2 );
        auto RRV_Gsigma = std::make_unique<RooRealVar>("Gsigma", "Gsigma", 1000*factor*1.2, 500*factor*0.8, 3000*factor*1.2 );
        auto RRV_Lsigma2 =std::make_unique<RooRealVar>("Lsigma2", "Lsigma2", 150*factor*1.2, 80*factor*0.8, 1200*factor*1.2 );
        auto RRV_Gsigma2 =std::make_unique<RooRealVar>("Gsigma2", "Gsigma2", 50*factor*1.2, 40*factor*0.8, 600*factor*1.2 );

        auto frac    = std::make_unique<RooRealVar>("frac","frac",0.95,0.5,1);
        auto gauss   = std::make_unique<RooGaussian>("GaussPdf","gaussian PDF", *rrv,*RRV_Gmean,*RRV_Gsigma);
        auto landau  = std::make_unique<RooLandau>("LandauPdf","landau PDF", *rrv,*RRV_Lmean,*RRV_Lsigma);
        auto lxg     = std::make_unique<RooFFTConvPdf>("lxg","landau (X) gauss",*rrv, *landau, *gauss);
        auto gauss2  = std::make_unique<RooGaussian>("GaussPdf2","gaussian2 PDF", *rrv,*RRV_Gmean,*RRV_Gsigma2);
        auto landau2 = std::make_unique<RooLandau>("LandauPdf2","landau2 PDF", *rrv,*RRV_Lmean,*RRV_Lsigma2);
        auto lxg2    = std::make_unique<RooFFTConvPdf>("lxg2","landau (X) gauss",*rrv, *landau2, *gauss2);

        auto RRV_Gmean_bkg =  std::make_unique<RooRealVar>("bkg_shift", "bkg_shift", 120.*factor, -500*factor,2000*factor);
        auto RRV_Gsigma_bkg = std::make_unique<RooRealVar>("bkg_sigma", "bkg_sigma", 10*factor, 1.*factor, 600*factor );

        auto rdsSignal = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("Signal"))));
        auto rdhSignal = std::unique_ptr<RooDataHist>(rdsSignal->binnedClone());

        auto modelPdf    = std::make_unique<RooAddPdf>("Signal", "double langaus", RooArgList(*lxg,*lxg2), RooArgList(*frac));

        modelPdf->fitTo(*rdhSignal, NumCPU(4), Minos(true), Extended(false));

        //________________________________________________________________________
        // Export results
        std::string iresult = itos(iFit+1)+"/"+itos(nFits);
        TbGaudi::PrintBanner("INFO", " ChargeFitter:: Exporting results ("+iresult+")");
        // Export individual plot as a canvas
        auto cname = ThisTbJobPtr()->Label()+"_bin"+itos(DutBin(iFit));
        auto can = std::make_unique<TCanvas>(TString("Can"+cname),TString(cname),660,600);
        can->cd();
        RooPlot* frame = rrv->frame();
        rdsSignal->plotOn(frame, MarkerStyle(8), MarkerSize(0.8), RooFit::Name("DATA"), Binning(200) );
        modelPdf->plotOn(frame, LineColor(kBlue), LineWidth(3), LineStyle(2), Components("lxg"));
        modelPdf->plotOn(frame, LineColor(kBlue), LineWidth(3), LineStyle(2), Components("lxg2"));
        modelPdf->plotOn(frame, LineColor(kAzure), LineWidth(3),  Components("Signal"));
        modelPdf->plotOn(frame, LineColor(kRed), LineWidth(3), RooFit::Name("MODEL") );
        modelPdf->paramOn(frame);
        frame->getAttText()->SetTextSize(0.023);
        frame->Draw();
        plots.insert(std::make_pair(cname,std::move(can)));

        // Export fitted model parameters
        double mpv = RRV_Lmean->getVal();
        double mpvErr = RRV_Lmean->getError();
        dut_binning_mpv->insert(std::make_pair(DutBin(iFit), std::make_pair(mpv,mpvErr)));

        double lSig    = RRV_Lsigma->getVal();
        double lSigErr = RRV_Lsigma->getError();
        dut_binning_LandauSigma->insert(std::make_pair(DutBin(iFit), std::make_pair(lSig,lSigErr)));

    }  // nFits loop
    TbResultsPtr()->AddPlots(ThisTbJobPtr(), std::move(plots));
    TbResultsPtr()->AddMappedResult<DutBinningResultsMap>(ThisTbJobPtr(),"MPV",dut_binning_mpv);
    TbResultsPtr()->AddMappedResult<DutBinningResultsMap>(ThisTbJobPtr(),"LandauSigma",dut_binning_LandauSigma);

    gROOT->SetBatch(false);
}