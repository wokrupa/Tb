#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaryies
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::RoofitWithOutOfSpillNoise(){
	gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: RoofitWithOutOfSpillNoise is executed...");

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    auto nFits = NGoodDataSets();
    auto nBins = TbFitter::RooFitBinning();
    std::cout << "[INFO]:: ChargeFitter:: Fit nEntries threshold: " << TbFitter::NEntriesFitThreshold() << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: Number of DUT bins with nEntries above threshold: " << nFits << std::endl;

    for (int iFit = 0; iFit < nFits; ++iFit) {
        auto rds = IRooDataSetPtr(iFit);
        auto rrv = IRooRealVarPtr(iFit);
        std::cout<< "[INFO]:: ChargeFitter:: Start fitting:: " << rds->GetName() << std::endl;

        // Prepare signal data hist to extract initial numbers for the fitting
        // TODO : make this as a TbFitter method:
        auto hist = std::unique_ptr<TH1F>(static_cast<TH1F*>(rds->createHistogram("SignalHist",
                                                                                   *rrv,
                                                                                   RooFit::Binning(nBins),
                                                                                   RooFit::CutRange("signal") ) ) );

    	// double factor = this->voltage/800.;
    	double factor = 600/800.;
	    double MPV = 0.8*hist->GetMean();
	    double sigma = hist->GetRMS();
	    double mean_low = MPV - 0.2*MPV;
	    double mean_high = MPV + 0.4*sigma;
    	std::cout<< "[DEBUG]::ChargeFitter:: bin statistics ::  "<<  hist->GetEntries() << std::endl;
    	std::cout<< "[DEBUG]::ChargeFitter:: MPV estimate ::  "<<  MPV << std::endl;
    	std::cout<< "[DEBUG]::ChargeFitter:: Sigma estimate ::  "<<  sigma << std::endl;

    	auto RRV_Gmean = std::make_unique<RooRealVar>(RooRealVar("Gmean", "Gmean",0));
    	auto RRV_Lmean = std::make_unique<RooRealVar>(RooRealVar("MPV","MPV",MPV, mean_low, mean_high));
    	auto RRV_Lsigma = std::make_unique<RooRealVar>(RooRealVar("Lsigma","Lsigma", 250*factor*1.2, 50*factor*0.8,1075*factor*1.2 ));
    	auto RRV_Gsigma = std::make_unique<RooRealVar>(RooRealVar("Gsigma","Gsigma", 1000*factor*1.2, 500*factor*0.8, 3000*factor*1.2 ));
    	auto RRV_Lsigma2 = std::make_unique<RooRealVar>(RooRealVar("Lsigma2","Lsigma2", 150*factor*1.2, 80*factor*0.8, 1200*factor*1.2 ));
    	auto RRV_Gsigma2 = std::make_unique<RooRealVar>(RooRealVar("Gsigma2","Gsigma2", 50*factor*1.2, 40*factor*0.8, 600*factor*1.2 ));

    	auto frac = std::make_unique<RooRealVar>(RooRealVar("frac","frac",0.95,0.5,1));

    	auto gauss =  std::make_unique<RooGaussian>(RooGaussian("GaussPdf","Gaussian PDF", *rrv,*RRV_Gmean,*RRV_Gsigma));
    	auto landau =  std::make_unique<RooLandau>(RooLandau("LandauPdf","Landau PDF", *rrv,*RRV_Lmean,*RRV_Lsigma));
    	auto lxg =  std::make_unique<RooFFTConvPdf>(RooFFTConvPdf("lxg","landau (X) gauss",*rrv, *landau, *gauss));

    	auto gauss2 =  std::make_unique<RooGaussian>(RooGaussian("GaussPdf2","Gaussian2 PDF", *rrv,*RRV_Gmean,*RRV_Gsigma2));
    	auto landau2 =  std::make_unique<RooLandau>(RooLandau("LandauPdf2"," Landau2 PDF", *rrv,*RRV_Lmean,*RRV_Lsigma2));
    	auto lxg2 =  std::make_unique<RooFFTConvPdf>(RooFFTConvPdf("lxg2","landau (X) gauss",*rrv, *landau2, *gauss2));
    	
    	auto signalPdf =  std::make_unique<RooAddPdf>(RooAddPdf("Signal", "Double langaus", RooArgList(*lxg,*lxg2), RooArgList(*frac)));


		//////////////////////////////////////////////////////////////////////////////////////
		//number of signal and background events to be fitted
		double nEntries = hist->GetEntries();
		double inisig = 0, maxsig=0, minsig=0;
		double inibkg = 0, maxbkg=0, minbkg=0;
		if (nEntries > 1100000){
			inisig = nEntries;
			minsig= 0.6*nEntries;
			maxsig= nEntries;
			inibkg =0.15*nEntries;
			minbkg=0.01*nEntries;
			maxbkg = 0.3*nEntries;
		}
		if (nEntries > 30000 && nEntries < 1100000){
			inisig = 0.7*nEntries;
			minsig= 0.5*nEntries;
			maxsig= nEntries;
			inibkg =0.2*nEntries;
			minbkg=0.01*nEntries;
			maxbkg = 0.3*nEntries;
		}
		if (nEntries < 30000 ){
			inisig = 0.5*nEntries;
			minsig= 0.5*nEntries;
			maxsig= nEntries;
			inibkg =0.2*nEntries;
			minbkg=0.005*nEntries;
			maxbkg = 0.3*nEntries;
		}
		auto N_Sig = std::make_unique<RooRealVar>( RooRealVar("N_Sig","# of signal events ", inisig, minsig, maxsig));
		auto N_Bkg = std::make_unique<RooRealVar>( RooRealVar("N_Bkg","# of background events ", inibkg, minbkg, maxbkg));

		auto rdsSignal = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("signal") ) ));
		auto rdsNoise = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("noise") ) ));


		if (rdsNoise->sumEntries() < 100){
        	std::cout << "[WARNING]::ChargeFitter:: OOPS entries on bkg too low"  << rdsNoise->sumEntries() << std::endl;
        	continue; 
      	}
      	std::cout <<"[DEBUG]::ChargeFitter:: Number of out-of-spill noise entries "  << rdsNoise->sumEntries() << std::endl;
      	std::cout<< "[INFO]::ChargeFitter:: Making kernel estimation... "  << std::endl;
      	
      	auto kestBgd = std::make_unique<RooKeysPdf>(RooKeysPdf("kestBgd","kestBgd",*rrv,*rdsNoise,RooKeysPdf::NoMirror));
      	auto RRV_Gmean_bkg = std::make_unique<RooRealVar>(RooRealVar("Bkg_shift","Bkg_shift", 120.*factor, -500*factor,2000*factor));
      	auto RRV_Gsigma_bkg = std::make_unique<RooRealVar>(RooRealVar("Bkg_sigma","Bkg_sigma", 10*factor, 1.*factor, 600*factor ));
      	auto gauss_bkg = std::make_unique<RooGaussian>(RooGaussian("Bkg_GaussPdf","Gaussian PDF", *rrv,*RRV_Gmean_bkg,*RRV_Gsigma_bkg));
      	auto bkg_adapt = std::make_unique<RooFFTConvPdf>(RooFFTConvPdf("keysConvGausbkg","noise (x) gauss",*rrv, *kestBgd, *gauss_bkg));

      	auto rdhSignal = std::unique_ptr<RooDataHist>(rdsSignal->binnedClone());
      	
      	auto modelPdf = std::make_unique<RooAddPdf>(RooAddPdf("modelPdf","totalpdf", RooArgList(*signalPdf,*bkg_adapt), RooArgList(*N_Sig,*N_Bkg)));
      	modelPdf->fitTo(*rdhSignal, RooFit::NumCPU(4), RooFit::Minos(true), RooFit::Extended());

      	// TODO :: export fit results
      	// TODO :: export fit plots
      	
 	} // nFits loop
	gROOT->SetBatch(false);
}