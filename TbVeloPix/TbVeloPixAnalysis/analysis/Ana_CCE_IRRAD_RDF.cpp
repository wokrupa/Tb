#include "TbVeloPixAnalysis.h"
#include "TbResults.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_CCE_IRRRAD_RDF()
{
    std::cout<<"[INFO]::TbVeloPixAnalysis::CCE with RDataFrame usage is executed... "<<std::endl;

    // *** DEFINE DATA ***
    RootData::UseRDataFrame(true);
    TbAnalysis::TbDataDB(TbGaudi::ProjectLocation()+"/TbVeloPix/data/TbDB_tutorial.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    TbJob::GlobalClusterSize(2);
    DutBinning::GlobalType("Uniform");
    //DutBinning::GlobalType("Elliptic");
    DutBinning::GlobalNBins(20);

    TbAnalysis::AddRdfFilter("Clusters","clCharge > 1000"); // unphysical entries
    TbAnalysis::AddRdfFilter("Clusters","clSize < 20");     // max clusters size
    TbAnalysis::AddRdfFilter("Clusters","clIsTracked");     // process only associated clusters
    TbAnalysis::PrintAllRdfFilters();

    // *** LOAD DATA ***
    LoadTbData();

    // *** RUN ANALYSIS ***
    CCE_IRRAD_RDF_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    tbResults->DrawDutMap("MPV");

    //TbAnalysis::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    tbResults->Write();
}