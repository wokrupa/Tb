#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbData.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_ECS_General()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::ECS data analysis is executed...");

    // *** DEFINE DATA ***
    TbAnalysis::TbDataDB(TbGaudi::ProjectLocation()+"/TbVeloPix/TbVeloPixAnalysis/data/Ecs-Dec18-IkrumScan.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    TbJob::GlobalClusterSize(-1);

    // Event loop related data
    DutRootUtils::WriteHistToNTuple(true);

    // *** LOAD DATA ***
    TbData::Type(DataType::ECS);
    LoadTbData();

    // *** RUN METHOD DEDICATED TO THIS ANALYSIS ***
    ECS_General_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    // tbResults->DrawDutMap("MPV");
    //tbResults->DrawDutProfile("MPV");
    //tbResults->DrawDutProfileCombined("MPV","column");
    //tbResults->DrawDutProfileCombined("MPV","row");

    //TbAnalysis::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    tbResults->Write(); // export everything to .pdf files

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::ECS data analysis is done.");
}



