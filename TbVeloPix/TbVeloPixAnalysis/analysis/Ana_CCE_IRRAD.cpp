#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbJobCollector.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_CCE_IRRAD()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::CCE of IRRAD sensors is executed...");

    // *** DEFINE DATA ***
    TbAnalysis::TbDataDB(TbGaudi::ProjectLocation()+"/TbVeloPix/TbVeloPixAnalysis/data/S8_binningStudies.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    TbJob::GlobalClusterSize(2);
    DutBinning::GlobalType("Elliptic"); // "Uniform"
    DutBinning::GlobalNBins(4);

    DutBinning::GlobalFluenceProfile("IRRAD");
    DutBinning::FluenceMapping(true); // perform fluence mapping computation
                                      // -> needs to register a dedicated fluence profile
    // Event loop related data
    DutRootUtils::WriteHistToNTuple(true);

    // *** LOAD DATA ***
    LoadTbData();

    // *** RUN METHOD DEDICATED TO THIS ANALYSIS ***
    CCE_IRRAD_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    tbResults->DrawDutMap("MPV");
    tbResults->DrawDutProfile("MPV");
    tbResults->DrawDutProfileCombined("MPV","column");
    tbResults->DrawDutProfileCombined("MPV","row");
    tbResults->DrawDutMap("Fluence");
    tbResults->DrawCombined("MPV","Fluence");

    //TbAnalysis::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    tbResults->Write(); // export everything to .pdf files

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::CCE is done.");
}



