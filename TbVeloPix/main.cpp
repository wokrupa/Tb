// std libriaries
#include <iostream>
#include <memory>

// ROOT libriaries
#include <TROOT.h>
#include <TApplication.h>


// TbGaudi libriaries
#include "TbGaudi.h"
#include "TbStyle.h"
#include "TbAnalysis.h"
#include "TbJob.h"
#include "TbResults.h"

// Project libriaries
#include "TbVeloPixAnalysis.h"

int main(int argc, char **argv)
{
    TApplication* rootapp = new TApplication("App", &argc, argv);

    TbGaudi::Severity("Debug");

    auto MyAnalysis = std::make_unique<TbVeloPixAnalysis>();
    MyAnalysis->Ana_CCE_IRRAD();
//    MyAnalysis->Ana_ECS_General();

    //PressEnterToQuit();
    rootapp->Run(!gROOT->IsBatch());
    return 0;
}

