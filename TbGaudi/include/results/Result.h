/*! \brief Results class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef RESULTS_H
#define RESULTS_H

#include <vector>
#include <string>
#include <iostream>
#include <memory>

#include "TH1D.h"
#include "TH2D.h"

#include "TbJob.h"
#include "Dut.h"
#include "Run.h"
#include "Globals.h"
#include "TbGaudi.h"

////////////////////////////////////////////////////////////////////////////////////
///
class ResultInterface {
	public:
		ResultInterface() = default;
		virtual ~ResultInterface() = default;
};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
class ResultInstance : public ResultInterface {
	public:
		ResultInstance(std::shared_ptr<T> res):m_result(res){};
		~ResultInstance()= default;
		std::shared_ptr<T> m_result;
		std::shared_ptr<T> Get(){return m_result;}
};

////////////////////////////////////////////////////////////////////////////////////
///
class Result {
	private:

		///
		std::map<std::string, std::shared_ptr<ResultInterface> > m_results;

	public:

		///
		Result()=default;

        ///
        ~Result()=default;

		template <typename T>
		void Add(std::string name, T* result);

		template <typename T>
        std::shared_ptr<T> Get(std::string name);

		///
		void Reset(){}; // TODO: implement me.

		///
		bool Exists(std::string name){
			return Includes<std::string>(m_results, name);
		}

};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
void Result::Add(std::string name, T* result){
	if(!Includes<std::string>(m_results, name)){
		ResultInterface* iresult = new ResultInstance<T>(std::shared_ptr<T>(result));
		m_results.insert(std::make_pair(name, std::shared_ptr<ResultInterface>(iresult)));
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
std::shared_ptr<T> Result::Get(std::string name){
	if(Includes<std::string>(m_results, name)){
	    auto resultInstance = std::dynamic_pointer_cast<ResultInstance<T>>(m_results.at(name));
	    return resultInstance->Get();
	}
//    std::cout << "[ERROR]:: Result:: You are trying to extract result"
//              << " ("<<name << ") which doesn't exists!"<< std::endl;
//    TbGaudi::RunTimeError("Error in analysis workflow.");
    else return std::shared_ptr<T>(nullptr);
}
#endif	// RESULTS_H
