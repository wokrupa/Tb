/*! \brief Results class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef TB_RESULTS_H
#define TB_RESULTS_H

#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include "Result.h"
#include "Globals.h"
#include "TCanvas.h"
#include "TGraphErrors.h"

typedef std::map<std::string,std::unique_ptr<TCanvas>> TCanvasMap;
typedef std::map<unsigned, std::pair<double,double>> DutBinningResultsMap;

using TbJobSharedPtr = std::shared_ptr<TbJob>;
////////////////////////////////////////////////////////////////////////////////////
///
class TbResults {
	private:
        ///
        TbResults();

        ///
        ~TbResults();

        // Delete the copy and move constructors
        TbResults(const TbResults&) = delete;
        TbResults& operator=(const TbResults&) = delete;
        TbResults(TbResults&&) = delete;
        TbResults& operator=(TbResults&&) = delete;

        std::unique_ptr<Result> m_results;

		///
        std::map<TbJobSharedPtr, std::unique_ptr<Result>> m_tbjob_results;

        ///
        TCanvasMap m_compilation_plots;

        ///
        std::map<TbJobSharedPtr, TCanvasMap> m_tbjob_plots;

        ///
        void ErrorIfNotExists(TbJobSharedPtr tbjob);

        ///
        void DrawHitMap(TbJobSharedPtr tbjob,std::string hname);

        ///
        void DrawDutPixelHitMap(TbJobSharedPtr tbjob);

        ///
        void DrawDutStripHitMap(TbJobSharedPtr tbjob);

        ///
        void DrawDutBinningHitMap(TbJobSharedPtr tbjob);

        ///
        void DrawDutMpvMap(TbJobSharedPtr tbjob);

        ///
        void DrawDutMPVProfile(TbJobSharedPtr tbjob);

        ///
        void DrawDutMpvProfileCombined(std::string axis);

        ///
        void DrawDutMap(TbJobSharedPtr tbjob, std::string name=std::string());

        ///
        void DrawDutProfile(TbJobSharedPtr tbjob, std::string name=std::string());

        /// \brief Create the TH2D Dut map, based on the existing results
        /// \param tbjob The pointer to the TbJob of interest
        /// \param name The name of the map (the same name should exists in the results!)
        std::shared_ptr<TH2D> CreateDutBinningMap(TbJobSharedPtr tbjob, std::string name);

        ///
        std::shared_ptr<TGraphErrors> GetDutProfile(TbJobSharedPtr tbjob, std::string name, std::string axis);

	public:

        static TbResults* GetInstance(){
            static TbResults instance;
            return &instance;
        }

        /// \brief Add single values to the vector of results specified by the name.
        void AddMappedResult(TbJobSharedPtr tbjob, std::string name, double val, double err);

        /// \brief Add any single object (e.g. TObject, or std::vector like) specified by the name.
        /// \param tbjob The tbjob number the results are related to.
        /// \param name The name of the given results
        /// \param val The pointer to the object containing results.
        template <typename T>
        void AddMappedResult(TbJobSharedPtr tbjob, std::string name, T* val);

        ///
        template <typename T>
        void AddResult(std::string name, T* val);

        /// \brief Add any single object (e.g. TObject, or std::vector like).
        /// \param tbjob The tbjob number the results belong to.
        /// \param name specific name of the results of interest.
        template <typename T>
        std::shared_ptr<T> GetMappedResult(TbJobSharedPtr tbjob, std::string name);

        ///
        template <typename T>
        std::shared_ptr<T> GetResult(std::string name);

        bool ResultExits(TbJobSharedPtr tbjob, std::string name);

        ///
        void AddPlots(TbJobSharedPtr tbjob, TCanvasMap plots);

		///
		void Reset(){};	// TODO: implement me.

        ///
        void DrawDutMap(std::string name, unsigned run=0,std::string runIdentifier=std::string());

        ///
        void DrawDutProfile(std::string name,unsigned run=0,std::string runIdentifier=std::string());

        ///
        void DrawDutProfileCombined(std::string name, std::string axis=std::string());

        ///
        void DrawCombined(std::string xAxis, std::string yAxis);

		///
		void Write();

		///
        double GetMininumResult(TbJobSharedPtr tbjob, std::string name);

		///
        double GetMaximumResult(TbJobSharedPtr tbjob, std::string name);

        ///
        void PlotInteractive(TbJobSharedPtr tbjob, std::string name);

};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
void TbResults::AddMappedResult(TbJobSharedPtr tbjob, std::string name, T* val){

    if(!Includes<TbJobSharedPtr>(m_tbjob_results,tbjob))
        m_tbjob_results[tbjob] = std::make_unique<Result>();

    m_tbjob_results.at(tbjob)->Add(name,val);

}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
void TbResults::AddResult(std::string name, T* val){
        m_results->Add(name,val);
}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
std::shared_ptr<T> TbResults::GetMappedResult(TbJobSharedPtr tbjob, std::string name){
    if(tbjob) ErrorIfNotExists(tbjob);
    return m_tbjob_results.at(tbjob)->Get<T>(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
std::shared_ptr<T> TbResults::GetResult(std::string name){
        return m_results->Get<T>(name);
}

#endif	// TB_RESULTS_H
#define gTbResults TbResults::GetInstance()
