/*! \brief TbStyle factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef TBSTYLE_H
#define TBSTYLE_H

// std libriaryies
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

////////////////////////////////////////////////////////////////////////////////////
///
enum class tStyle {LHCb, Kazu, Bartek};

////////////////////////////////////////////////////////////////////////////////////
/// The TbStyle singleton class definition.
class TbStyle{
	private:
		TbStyle();
		~TbStyle() = default;

		// Delete the copy and move constructors
		TbStyle(const TbStyle&) = delete;
		TbStyle& operator=(const TbStyle&) = delete;
		TbStyle(TbStyle&&) = delete;
		TbStyle& operator=(TbStyle&&) = delete;

		/// List of colors which can being added iteratively e.g. for vector of graphs;
		static std::vector<int> MyColors;

		/// List of markers which can being added iteratively e.g. for vector of graphs;
		static std::vector<int> MyMarkers;

		///
		static int m_i_color;

		///
		static int m_i_marker;
		///
		void SetLHCbStyle();
		void SetKazuStyle();
		void SetBartekStyle();

		///
		int  chart1, chart2, chart3, chart4, chart6, chart8, chart12;
		int  grey40, grey60;
		int  sun3, sun4;
		int  BlueGrey;
		int  DarkPink;
		int  Azure;
		int  Purple;
		int  Violet;
		int  Pink;
		int  Teal;
		int  LighterBlueGrey;
		int  LightBlueGrey;
		int  DarkGrey;
		int  LightGrey;
		int  CoolBlue;
		int  AvocadoGreen;
		int  LightBlue;
		int  DarkGreen;
		int  Yellow;
		int  Orange;
		int  DarkBlue;

		//
		static tStyle m_Style;

	public:
		static TbStyle* GetInstance(){
			static TbStyle instance;
  		return &instance;
		}

		///
		static tStyle Style() {return m_Style;};
		static void Style(tStyle val);

		///
		static int Color(int i);

		///
		static int iColor();

		///
		static int Marker(int i);

		///
		static int iMarker();

};
#endif	// TBSTYLE_H
