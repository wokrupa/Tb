/*! \brief DutBinning class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef DDUTBINING_H
#define DDUTBINING_H

#include <vector>
#include <string>
#include <memory>
#include "DutBin.h"
//#include "Dut.h"
#include <iostream>

class Dut;
class FluenceProfile;

////////////////////////////////////////////////////////////////////////////////////
///
enum class DutBinningType {Uniform, Elliptic, Strip};

////////////////////////////////////////////////////////////////////////////////////
///
class DutBinning {

	protected:
		///
		Dut* m_owner;

		///
		std::vector<DutBin*> m_dut_bins;

        /// \brief Pointer to the fluence profile associated to the given instantiation of the DutBinning.
        std::shared_ptr<FluenceProfile> m_fluence_profile;

        ///
        std::vector<std::vector<unsigned >> m_dut_pixel_binning_counter;

        ///
		std::vector<double> m_middle_bin_column;

		///
		std::vector<double> m_middle_bin_row;

		///
        static DutBinningType m_binning_type;

        ///
        static bool m_fluence_mapping;

        ///
        static std::string m_global_fluence_profile_type;

        ///
        std::string m_fluence_profile_type;

        ///
        static unsigned m_global_nbins;

        ///
        unsigned m_nbins;

		///
		virtual void pixels_mapping()=0;

		///
		bool m_is_binning_stat_filled;

		///
		bool m_is_pixel_stat_filled;

		///
		bool m_is_initialized;


	public:
        ///
		DutBinning(Dut* owner, std::string name, unsigned nBins);

        ///
        virtual ~DutBinning();

		///
		static void GlobalNBins(unsigned val){m_global_nbins = val;}

		///
		static unsigned GlobalNBins(){return m_global_nbins;}

		///
		unsigned NBins() const {return m_nbins;}

		///
		static void GlobalType(std::string type);

		///
        static bool FluenceMapping(){return m_fluence_mapping;}

        ///
        static void FluenceMapping(bool val){m_fluence_mapping=val;}

        ///
        static void GlobalFluenceProfile(std::string val){m_global_fluence_profile_type = val;}

        ///
        static std::string GlobalFluenceProfile(){return m_global_fluence_profile_type;}

        ///
        void SetFluenceProfile(FluenceProfile* fluenceProfile);

		///
		virtual void InitializeDutPitchHitCounter() = 0;

        ///
        virtual void ResetDutPitchHitCounter() = 0;

		///
		static std::string GlobalType();

		///
		int GetBinNumber(unsigned X, unsigned Y=0);

        ///
        void CountDutBin(unsigned X, unsigned Y=0);

        ///
        void CountDutPixel(unsigned X, unsigned Y);

		///
		void CountDutBin(int binNumber);

        ///
        unsigned BinStatistic(unsigned binNumber);

        ///
        inline bool IsBinningStatFilled() const {return m_is_binning_stat_filled;}

        ///
        inline bool IsPixelStatFilled() const {return m_is_pixel_stat_filled;}

        ///
        void ResetStatistic();

        ///
        void PrintStatistic();

        ///
        inline const std::vector<DutBin*>& DutBins() {return m_dut_bins;}

        ///
        inline const std::vector<std::vector<unsigned >>& PixelBinningCounter() {return m_dut_pixel_binning_counter;}

        ///
        std::vector<double>& GetMiddleBinVector(std::string m_axis);

        ///\brief Initialization of the actual binning being created.
        virtual void Initialize() = 0;

        ///
        inline bool IsInitialized() const { return m_is_initialized; }

        ///
        inline Dut* OwnerDut() const { return m_owner;}

};
#endif	// DDUTBINING_H
