//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_DUTPIX_H
#define TBGAUDI_DUTPIX_H

#include "Dut.h"

class DutPix : public Dut {
    private:

    public:
        DutPix() = default;
        ~DutPix() = default;
};
#endif //TBGAUDI_DUTPIX_H
