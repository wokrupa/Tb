/*! \brief Dut class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef Dut_H
#define Dut_H

#include "DutBinning.h"

// std libriaries
#include <iostream>
#include <string>
#include <map>

////////////////////////////////////////////////////////////////////////////////////
///
class Dut {

	private:
		///
		DutBinning* m_dut_binning;

		///
		bool m_is_binned;

		/// The read-out pixel/strip pitch size in [nm]
		static unsigned m_pitch_size;

		/// \brief The read-out dut pixels in X
		static unsigned m_n_pix_x;

		/// \brief The read-out dut pixels in Y
        static unsigned m_n_pix_y;

		/// \brief The read-out dut strips
        static unsigned m_n_strip;

        /// Length of read-out strip in [mm]
        static float m_strip_length;

        /// \brief DUT size in X in [mm]
        float m_size_x;

        /// \brief DUT size in Y in [mm]
        float m_size_y;

        ///
        static bool m_pix_study;
        static bool m_intra_pix_study;
        static bool m_strip_study;
        static bool m_intra_strip_study;

        ///
        void DutSizeInitialization();

	public:
		Dut();
		virtual ~Dut();

		///
		bool IsStripBinning() const;

		///
		bool IsUniformBinning() const;

		///
		bool IsEllipticBinning() const;

		///
		unsigned NBins() const;

		///
		static unsigned PitchSize(){return m_pitch_size;}

		///
		static void PitchSize(unsigned val){ m_pitch_size = val;}

		///
		static unsigned NPixelsX(){return m_n_pix_x;}

        ///
        static void NPixelsX(unsigned val){ m_n_pix_x = val;}

        ///
        static unsigned NPixelsY(){return m_n_pix_y;}

        ///
        static void NPixelsY(unsigned val){m_n_pix_y = val;}

		///
		static unsigned NStrips(){return m_n_strip;}

		///
		static void NStrips(unsigned val){ m_n_strip = val;}

		///
		static float StripLength(){return m_strip_length;}

		///
		static void StripLength(float val){ m_strip_length = val;}

        ///
        static void PixStudy(bool val);

        ///
        static bool PixStudy() {return m_pix_study;}

		///
		static void IntraPixStudy(bool val);

		///
		static bool IntraPixStudy() {return m_intra_pix_study;}

		///
		static void StripStudy(bool val);

		///
		static bool StripStudy() {return m_strip_study;}

		///
		static void IntraStripStudy(bool val);

		///
		static bool IntraStripStudy() {return m_intra_strip_study;}

        ///
		inline DutBinning* DutBinningPtr(){ return m_dut_binning;}

		/// \brief The DUT binning definition according to the DutBinningType assigned to given TbJob.
		void CreateBinning();

        ///
        bool IsBinned() const {return m_is_binned;}

        ///
		float DutSizeX() { return m_size_x; }

		///
		float DutSizeY() { return m_size_y; }

};
#endif	// Dut_H
