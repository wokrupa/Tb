/*! \brief DutEllipticBinning class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2018
*/

#ifndef DUT_ELLIPTIC_BINNING_H
#define DUT_ELLIPTIC_BINNING_H

// std libriaryies
#include <vector>

// project libraries
#include "DutBinning.h"

// ROOT libriaryies
#include "TEllipse.h"
#include "TF2.h"

class Dut;

////////////////////////////////////////////////////////////////////////////////////
///
class DutEllipticBinning : public DutBinning {

	private:
		///binnig boundaries:
		std::vector<TEllipse*> m_bin_boundary_ellipses;

        ///
        void pixels_mapping();

        /// \brief The centre x of the elliptical binning
        double m_sx;

		/// \brief The centre x of the elliptical binning
        double m_sy;

        /// \brief The r_x / r_y ratio of the ellipses radiuses
        double m_width_factor;

	public:
        ///
		DutEllipticBinning(Dut* owner, std::string name, unsigned nBins);

        ///
        virtual ~DutEllipticBinning();

        ///
        void Initialize();

		///
        double BinIntegral(TF2* func, int bin, double normalization, double phimin, double phimax);

        ///
        double BinArea(int bin, double phimin, double phimax);

        ///
        double GetDutBinCentreX();

		///
		double GetDutBinCentreY();

        ///
        void InitializeDutPitchHitCounter() override;

        ///
        void ResetDutPitchHitCounter() override;

};
#endif	// DUT_ELLIPTIC_BINNING_H
