//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_DUTINTRAPIX_H
#define TBGAUDI_DUTINTRAPIX_H

#include "DutPix.h"

class DutIntraPix : public DutPix {
    private:

    public:
        DutIntraPix() = default;
        ~DutIntraPix() = default;
};
#endif //TBGAUDI_DUTINTRAPIX_H
