/*! \brief TbAnalysis class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef TB_ANALYSIS_H
#define TB_ANALYSIS_H

#include "TbGaudi.h"
#include "RootData.h" // needed for UseRDataFrame
#include <vector>
#include <map>

class TbJobCollector;
class TbJob;

////////////////////////////////////////////////////////////////////////////////////
///
class TbAnalysis {
	private:

		///
		TbGaudi* m_tbGaudi;

		///
		static bool m_save_control_plots;

		///
		static std::string m_output_location;

        ///
        static std::string m_db_file;

        /// \brief Collection of TbJob objects, and corresponding services.
        TbJobCollector* m_job_collector;

        /// \brief The RDataFrame and filters to be applied on it automatically.
        static std::map<std::string, std::vector<std::string>> m_rdf_filters;

	public:
		///
		TbAnalysis();

		///
		virtual ~TbAnalysis();

		///
        static const std::string& TbDataDB() {return m_db_file;}

        ///
        static void TbDataDB(const std::string& db_file) { m_db_file=db_file;}

		///
		static std::string OutputLocation() {return m_output_location;}

		///
		static void OutputLocation(const std::string& path) { m_output_location=path;}

		///
		static void ClearAllRdfFilters(){m_rdf_filters.clear();}

		///
		static void ClearRdfFilters(std::string name){m_rdf_filters.at(name).clear();}

		///
		static void AddRdfFilter(std::string name, std::string filter);

		///
		static void AddRdfFilters(std::string name, std::vector<std::string> filters);

        ///
        static std::vector<std::string>* RdfFilters(std::string name);

		///
		static void PrintAllRdfFilters();

		///
		static void PrintRdfFilters(std::string name);

		///
		bool LoadTbData();

        ///
		const std::vector<std::shared_ptr<TbJob>>& TbJobs();

        /// TODO:
        /// ana = "MPV"
        /// ana = "HIT"
//        void Compute(const std::string name);
};
#endif	// TB_ANALYSIS_H