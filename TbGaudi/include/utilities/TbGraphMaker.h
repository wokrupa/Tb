//
// Created by brachwal on 05.02.19.
//

#ifndef TBGAUDI_TBGRAPHMAKER_H
#define TBGAUDI_TBGRAPHMAKER_H

#include <memory>
#include "TbJob.h"
#include "TbResults.h"

class TH2D;

using TbJobSharedPtr  = std::shared_ptr<TbJob>;
using TbResultsPtr  = TbResults*;
using TH2DSharedPtr  = std::shared_ptr<TH2D>;

class TbGraphMaker {
    private:
        ///
        TbJobSharedPtr m_tbjob;

        ///
        TbResultsPtr m_tbresults;

        ///
        template <typename T>
        void AddToTbResults(std::shared_ptr<T> graph, bool draw) const;

    public:

        ///
        TbGraphMaker() = delete;

        ///
        TbGraphMaker(TbJobSharedPtr tbrun);

        ///
        //void MakeXYZGraph(bool draw = false) const;
};

template <typename T>
void TbGraphMaker::AddToTbResults(std::shared_ptr<T> graph, bool draw) const {
        auto name = graph->GetName();
        m_tbresults->AddMappedResult<T>(m_tbjob,"DutBinningStatistics",static_cast<T*>(graph->Clone()));
        std::cout << "[INFO]:: TbGraphMaker:: "<<name<<" TGraph added to TbResults."<<std::endl;
        if(draw) m_tbresults->PlotInteractive(m_tbjob,name);
}

#endif //TBLHCBANA_TBHISTMAKER_H
