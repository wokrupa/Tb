/*! \brief TbFitter factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date October-2018
*/

#ifndef TB_FITTER_H
#define TB_FITTER_H

// std libriaryies
#include <stdio.h>
#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <map>
#include <string>



class TbJob;
class TbResults;
class TH1D;
class RooDataSet;
class RooRealVar;

enum class FitDataType {RooDataSet, TH1};

using TbJobSharedPtr = std::shared_ptr<TbJob>;

class TbFitter{

    private:
        ///
        std::string m_data_name;

        /// The histogram data
        std::vector<TH1D*>* m_dut_binning_th1_data;

        ///
        TH1D* m_dut_th1_data;

        /// The dut binning roodataset data, and corresponding roorealval
        std::vector<RooDataSet*>* m_dut_binning_rds_data;
        std::vector<RooRealVar*>* m_dut_binning_rrv_variable;

        /// The single roodataset data, and corresponding roorealval
        RooDataSet* m_dut_rds_data;
        RooRealVar* m_dut_rrv_variable;

        ///
        TbJobSharedPtr m_tbjob;

        ///
        TbResults* m_tbResults;

        ///
        static unsigned m_roofit_binning;

        ///
        static unsigned m_nentries_fit_threshold;

        /// \brief Mpping of the simple indexing with the indexes of the data pointers containers
        std::map<unsigned, unsigned> m_good_dataset;

        ///
        bool IsGoodDataSet(unsigned bin) const;

        ///
        bool IsGoodDataSet() const;

        ///
        bool IsGoodTHist(unsigned bin) const;

        ///
        bool IsGoodTHist() const;

        //
        FitDataType m_fit_data_type;

        //
        std::function<void()> m_fit_algorithm;

        ///
        void InitializeRooDataSet();

        ///
        void InitializeTH1Data();

	public:

        ///
        TbFitter();

		///
		~TbFitter()=default;

		///
		void SetData(TbJobSharedPtr tbJob, std::string dataName, FitDataType dataType);

		///
		void SetFitDataType(FitDataType dataType) {m_fit_data_type=dataType;}

        ///
        static void RooFitBinning(unsigned val){ m_roofit_binning = val; }

        ///
        static unsigned RooFitBinning(){ return m_roofit_binning; }

        ///
        static void NEntriesFitThreshold(unsigned val){ m_nentries_fit_threshold = val; }

        ///
        static unsigned NEntriesFitThreshold(){ return m_nentries_fit_threshold; }

        ///
        void LinkTbJobData(std::string name, TbJobSharedPtr tbrun);

        ///
        size_t NGoodDataSets() const;

        ///
        RooDataSet* IRooDataSetPtr(unsigned bin);

        ///
        RooDataSet* IRooDataSetPtr() {return m_dut_rds_data;}

        ///
        RooRealVar* IRooRealVarPtr(unsigned bin);

        ///
        RooRealVar* IRooRealVarPtr() {return m_dut_rrv_variable;}

        ///
        TH1D* ITHistPtr(unsigned bin);

        ///
        TH1D* ITHistPtr() {return m_dut_th1_data;}

        ///
        double TbVoltage() const;

        ///
        unsigned DutNBins() const;

        ///
        TbJobSharedPtr ThisTbJobPtr() const {return m_tbjob;}

        ///
        TbResults* TbResultsPtr() {return m_tbResults;}

        ///
        std::string FitLabel() const;

        ///
        unsigned DutBin(unsigned goodDataIdx) const;



};
#endif	// TB_FITTER_H
