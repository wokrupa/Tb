//
// Created by brachwal on 11.10.18.
//

#ifndef TBLHCBANA_DUTROOTUTILS_H
#define TBLHCBANA_DUTROOTUTILS_H

#include "DutRooFitUtils.h"
#include <vector>
#include <map>

class TH1D;
class TH2D;
class TCanvas;
class TbResults;

enum class HistUnit{DutSize, PitchSize};

class DutRootUtils : public DutRooFitUtils {

    private:

        ///
        std::map<std::string, std::vector<TH1D*>> m_dut_binning_data_hist;

        ///
        std::map<std::string, TH1D*> m_dut_data_hist;

        ///
        std::map<std::string, TH2D*> m_dut_data_2dhist;

        ///
        std::vector<TCanvas*> m_tcanvas;

        ///
        TbResults* tbResults;

        ///
        static bool m_dumpBinningHistToNTuple;

        ///
        static HistUnit m_hist_unit;


public:
        DutRootUtils();
        virtual ~DutRootUtils();

        /// \brief Define (add to the std::map) the number of TH1D histograms for a given TbJob.
        /// The number of TH1D corresponds to the number of Dut bins defined by the User.
        /// \param name The name of the set of histograms (used also in the TH1D title and name).
        /// \param nBinsX The number of bins in particular TH1D instance.
        /// \param xLow The lower range for the particular TH1D instance.
        /// \param xUp The upper range for the particular TH1D instance.
        void DefineDutBinningHist(std::string name, unsigned nBinsX, double xLow, double xUp);

        /// \brief Define (add to the std::map) the single TH1D histogram for a given TbJob.
        /// \param name The name of the set of histograms (used also in the TH1D title and name).
        /// \param nBinsX The number of bins in particular TH1D instance.
        /// \param xLow The lower range for the particular TH1D instance.
        /// \param xUp The upper range for the particular TH1D instance.
        void DefineDutHist(std::string name, unsigned nBinsX, double xLow, double xUp);

        /// \brief Define (add to the std::map) the single TH2D histogram for a given TbJob.
        /// \param name The name of the set of histograms (used also in the TH2D title and name).
        /// \param nBinsX The number of bins in particular TH2D instance on x-axis.
        /// \param xLow The lower range for the particular TH2D instance on x-axis.
        /// \param xUp The upper range for the particular TH2D instance on x-axis.
        /// \param nBinsY The number of bins in particular TH2D instance on y-axis.
        /// \param yLow The lower range for the particular TH2D instance on y-axis.
        /// \param yUp The upper range for the particular TH2D instance on y-axis.
        void Define2DDutHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                            unsigned nBinsY, double yLow, double yUp);

        /// \brief SetBinContent to the TH2D histogram using pixel coordinates
        /// \param name The name of the TH2D to be filled.
        /// \param x x-coordinate of the pixel
        /// \param y y-coordinate of the pixel
        /// \param value the actual value to be set in the TH2D
        void SetDutBinContent2DHist(std::string name, unsigned x, unsigned y, double value);

        /// \brief SetBinContent to the TH2D histogram using global Dut binning scheme
        /// \param name The name of the TH2D to be filled.
        /// \param dutBin The global DUT bin number
        /// \param value the actual value to be set in the TH2D
        void SetDutBinContent2DHist(std::string name, unsigned dutBin, double value);

        /// \brief Remove the set of histograms from the std::map in a given TbJob
        /// \param name The name of the set to be removed.
        void ClearDutBinningHist(std::string name);

        /// \brief Remove all stored histograms;
        void ClearAllDutHist();

        /// \brief Get pointer to the particular TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        /// \param dutBinNumber The DUT bin number it is being identified with.
        /// The container if interest is m_dut_binning_data_hist
        TH1D* DutHistPtr(std::string name, unsigned dutBinNumber);

        /// \brief Get pointer to the particular TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to
        /// The container if interest is m_dut_data_hist
        TH1D* DutHistPtr(std::string name);

        /// \brief Get pointer to the whole container of TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        std::vector<TH1D*>* DutBinningTH1VectorPtr(std::string name);

        /// \brief Export to ROOT file all DUT binning histograms
        /// \param name The name of set of TH1D histograms
        void WriteEventLoopHistToNTuple(std::string name);

        ///
        static bool WriteHistToNTuple(){return m_dumpBinningHistToNTuple;}

        ///
        static void WriteHistToNTuple(bool val){m_dumpBinningHistToNTuple=val;}

        ///
        static HistUnit GetHistUnit() {return m_hist_unit;}

        ///
        static void SetHistUnit(HistUnit unit) {m_hist_unit =unit;}

        ///
        double TH2Xup() const;

        ///
        double TH2Yup() const;

        ///
        unsigned TH2NBinsX() const;

        ///
        unsigned TH2NBinsY() const;

        ///
        unsigned TH2PitchNBinsX() const;

        ///
        unsigned TH2PitchNBinsY() const;

        ///
        std::string TH2AxesTitles() const;
};

#endif //TBLHCBANA_DUTROOTUTILS_H
