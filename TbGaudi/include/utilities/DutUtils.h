//
// Created by brachwal on 12.10.18.
//

#ifndef TBLHCBANA_DUTUTILS_H
#define TBLHCBANA_DUTUTILS_H

#include "Dut.h"
#include "DutRootUtils.h"
#include "Rtypes.h"

class DutUtils : public DutRootUtils {
    private:

        std::map<unsigned, unsigned>    m_event_counter;
        std::map<unsigned, std::string> m_event_counter_name;
        std::vector<unsigned>           m_event_counter_priv_idx;

    public:
        DutUtils()=default;

        virtual ~DutUtils()=default;

        bool IsGoodClusterSize(UInt_t clSize);

        bool IsClusterInsideDutBin(Int_t* col,Int_t* row, UInt_t size);

        void DefineEventCounter(std::string name, unsigned idx);

        unsigned& EventCounter(unsigned idx);

        const unsigned& GetCounterStatistics(unsigned idx);

        void PrintAllCountersStatistics();

        ///
        void CountDutHit(unsigned col, unsigned row=0);

        ///
        int DutBinNumber(unsigned X, unsigned Y);

        ///
        int DutBinNumber(unsigned X);
};
#endif //TBLHCBANA_DUTUTILS_H
