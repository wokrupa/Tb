//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_TBHISTMAKER_H
#define TBGAUDI_TBHISTMAKER_H

#include <memory>
#include "TbJob.h"
#include "TbResults.h"

class TH2D;

using TbJobSharedPtr  = std::shared_ptr<TbJob>;
using TbResultsPtr  = TbResults*;
using TH2DSharedPtr  = std::shared_ptr<TH2D>;

class TbHistMaker {
    private:
        ///
        TbJobSharedPtr m_tbjob;

        ///
        TbResultsPtr m_tbresults;

        ///
        TH2DSharedPtr DefineNewTH2D(const char* name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp) const;

        ///
        template <typename T>
        void AddToTbResults(std::shared_ptr<T> hist, bool draw) const;

    public:

        ///
        TbHistMaker() = delete;

        ///
        TbHistMaker(TbJobSharedPtr tbrun);

        ///
        void MakeDutBinningStatisticHist(bool draw = false) const;

        ///
        void MakeDutPixelStatisticHist(bool draw = false) const;

        ///
        void MakeDutStripStatisticHist(bool draw = false) const;

        ///
        void MakeDutIntraPixelStatisticHist(bool draw = false) const;

        ///
        void MakeDutIntraStripStatisticHist(bool draw = false) const;

};
template <typename T>
void TbHistMaker::AddToTbResults(std::shared_ptr<T> hist, bool draw) const {
        auto name = hist->GetName();
        m_tbresults->AddMappedResult<T>(m_tbjob,"DutBinningStatistics",static_cast<T*>(hist->Clone()));
        std::cout << "[INFO]:: TbHistMaker:: "<<name<<" TH2D histogram added to TbResults."<<std::endl;
        if(draw) m_tbresults->PlotInteractive(m_tbjob,name);
}

#endif //TBLHCBANA_TBHISTMAKER_H
