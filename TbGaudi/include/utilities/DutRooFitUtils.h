//
// Created by brachwal on 11.10.18.
//

#ifndef TBLHCBANA_DUTROOFITUTILS_H
#define TBLHCBANA_DUTROOFITUTILS_H

#include <vector>
#include <map>
#include <memory>
#include "Dut.h"

class RooDataSet;
class RooRealVar;
class RooArgSet;
class RooCategory;

using  DutSharedPtr = std::shared_ptr<Dut>;

class DutRooFitUtils {
    protected:

        ///
        DutSharedPtr m_dut;

    private:
        ///
        std::map<std::string, std::vector<RooDataSet*>> m_dut_binning_data_set;

        ///
        std::map<std::string, std::vector<RooRealVar*>> m_dut_binning_data_var;

        ///
        std::map<std::string, std::vector<RooArgSet*>> m_dut_binning_data_arg_set;

        ///
        std::map<std::string, std::vector<RooCategory*>> m_dut_binning_data_category;

        ///
        std::map<std::string, RooDataSet*> m_dut_data_set;

        ///
        std::map<std::string, RooRealVar*> m_dut_data_var;

        ///
        std::map<std::string, RooArgSet*> m_dut_data_arg_set;

        ///
        std::map<std::string, RooCategory*> m_dut_data_category;

    public:
        DutRooFitUtils() = default;
        virtual ~DutRooFitUtils();

        ///
        void SetDut(DutSharedPtr dut) { m_dut = dut;}

        /// \brief Define (add to the std::map) the number of RooDataSets for a given TbJob.
        /// The number of RooDataSets corresponds to the number of Dut bins defined by the User.
        /// Currently, the RooDataSet here is limited only to single RooRealVal.
        /// \param name The name of the set of RooDataSets.
        /// \param unit The unit of a given observable
        /// \param xLow The lower range for the particular observable.
        /// \param xUp The upper range for the particular observable.
        void DefineDutBinningDataSet(std::string name, double xLow, double xUp, std::string unit=std::string());

        /// \brief Define (add to the std::map) the RooDataSet for a given TbJob.
        /// Currently, the RooDataSet here is limited only to single RooRealVal.
        /// \param name The name of the set of RooDataSets.
        /// \param unit The unit of a given observable
        /// \param xLow The lower range for the particular observable.
        /// \param xUp The upper range for the particular observable.
        void DefineDutDataSet(std::string name, double xLow, double xUp, std::string unit=std::string());

        /// \brief Remove all the RooDataSets
        void ClearAllDutDataSet();

        /// \brief Remove the set of RooDataSets from the std::map in a given TbJob
        /// \param name The name of the set to be removed.
        void ClearDutDataSet(std::string name);

        /// \brief Get pointer to the whole container of RooDataSets, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        std::vector<RooDataSet*>* DutBinningRDSVectorPtr(std::string name);

        /// \brief Get pointer to the whole container of RooRealVar, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        std::vector<RooRealVar*>* DutBinningRRVVectorPtr(std::string name);

        /// \brief Get pointer to the particular RooDataSets, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        /// \param dutBinNumber The DUT bin number it is being identified with.
        RooDataSet* DutDataSetPtr(std::string name, unsigned dutBinNumber);

        ///
        RooDataSet* DutDataSetPtr(std::string name);

        /// \brief Add RooCategory type to the already existing RooDataSet for a given TbJob.
        /// \param name The name of the type
        /// \param idx The indexed label for a given type
        void DefineDutBinningDataSetType(std::string rdsName, std::string typeName, int idx);

        /// \brief Add RooCategory type to the already existing RooDataSet for a given TbJob.
        /// \param name The name of the type
        /// \param idx The indexed label for a given type
        void DefineDutDataSetType(std::string rdsName, std::string typeName, int idx);

        ///
        RooCategory* DutDataSetRCatPtr(std::string name, unsigned dutBinNumber);

        ///
        RooCategory* DutDataSetRCatPtr(std::string name);

        ///
        RooRealVar* DutDataSetRRVarPtr(std::string name, unsigned dutBinNumber);

        ///
        RooRealVar* DutDataSetRRVarPtr(std::string name);

        ///
        void AddToDutDataSet(std::string name, unsigned dutBinNumber);

        ///
        void AddToDutDataSet(std::string name);
};
#endif //TBLHCBANA_DUTROOFITUTILS_H
