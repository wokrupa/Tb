#ifndef FP_FluenceProfile_H
#define FP_FluenceProfile_H

// std libriaryies
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <array>

// my libriaryies
#include "TbGaudi.h"
#include "Globals.h"

// ROOT libriaryies
#include "TPad.h"

////////////////////////////////////////////////////////////////////////////////////
///
class DutBinning;
class FluenceProfile;
typedef std::shared_ptr<FluenceProfile> FlnProfileShPtr;
typedef std::map<std::string, FlnProfileShPtr> FluenceUniqueProfilesMap;

class FluenceProfile {

	private:
		/// \brief Global fluence profiles map.
		/// Each entry is associated to the given profile type defined by the set of parameters:
		/// 1) DUT name (e.g. S8), profile type (e.g. IRRAD), number of bins, etc;
		/// 2) The default unique label is taken from Dut::DutLabel()
		static FluenceUniqueProfilesMap m_unique_fluence_profiles;

		/// \brief The DutBinning pointer the given FluenceProfile instantiation is related to.
		/// The unique FluenceProfile instance needs to have this to be initialized once,
		//  once need to remember, single unique FluenceProfile is linked to different TbJobs
		std::shared_ptr<DutBinning> m_dut_binning;

		///
		void SetDutBinning(DutBinning* ptr);

	protected:

		/// \brief The name of the fluence profile instantiation
		std::string m_name;

		/// \brief The value of the total fluence associated to the given profile instantiation
		double m_total_fluence;

		///
		bool m_isDefault;

		///
		bool m_isInitialized;

	public:

		/// \brief Constructor
		FluenceProfile(std::string name);

		/// \brief Destructor
		virtual ~FluenceProfile() = default;

		virtual void Initialize()=0;

		/// \brief Info whether the default parameterization was overridden
		bool IsDefault() const { return m_isDefault; };

		///
		static FluenceUniqueProfilesMap& MapOfUniqueProfiles(){return m_unique_fluence_profiles;}

		/// \brief Add a new fluence profile to the unique profiles map
		static FlnProfileShPtr AddProfile(DutBinning* m_dutBinning, FluenceProfile* newProfile);

        /// \brief Get the pointer to the specified profile
		static FlnProfileShPtr GetPointer(std::string name);

        /// \brief Get the total fluence value
		inline double GetTotalFluence() const { return m_total_fluence; }

		/// \brief Get the name of the fulence profile instantation
		inline std::string GetName() const { return m_name; }

		///
		std::string LinkedDutName() const;

		///
		std::shared_ptr<DutBinning> DutBinningPtr() const {return m_dut_binning; }

		// ______________________________________________
		// Dosimetry measurement results interface:

		/// \biref Get the fluence value associated to the given dosimetry bin number
		virtual double GetDosimetryFluence(int) = 0;

		/// \biref Get the fluence value associated to the dosimetry bin number mapped with the given pixel
		virtual double GetDosimetryFluence(int, int) = 0;

		/// \biref Get the value of the dosimetry bin area for the given bin number
		virtual double GetDosimetryBinArea(int) = 0;

		/// \biref Get the dosimetry bin number mapped to the given DUT pixel
		virtual int GetDosimetryBin(int, int) = 0;

		/// \brief Draw the dosimetry measurement map.
		virtual void DrawDosimetryMeasurement() = 0;

		// ______________________________________________
		// The actual fluence profile interface:

		/// \brief Get the fluence bin number corresponding to the given fluence level
		virtual int GetBin(double) = 0;

		/// \brief Get the fluence bin number corresponding to the given DUT pixel
		virtual int GetBin(int, int) = 0;

		/// \brief Get the fluence value from the given fluence bin number
		virtual double GetFluence(int) = 0;

		/// \brief Get the fluence value associated to the fluence bin mapped with the given pixel
		virtual double GetFluence(int, int) = 0;

		/// \brief Get the fluence value associated to the given DUT pixel
		virtual double GetPixFluence(int Lx, int Ly) = 0;

		/// \brief Draw the fluence profile binning
		virtual void DrawBinning(TPad*  pad, std::string opts) = 0;
};

#endif // FP_FluenceProfile_H