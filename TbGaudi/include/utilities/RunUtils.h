//
// Created by brachwal on 12.10.18.
//

#include "Run.h"

#ifndef TBLHCBANA_RUNUTILS_H
#define TBLHCBANA_RUNUTILS_H

class RunUtils {
    private:
        std::shared_ptr<Run> m_run;

    public:
        ///
        RunUtils() = default;

        ///
        virtual ~RunUtils() = default;

        ///
        void SetRun(std::shared_ptr<Run> run) {m_run = run;}

};

#endif //TBLHCBANA_RUNUTILS_H
