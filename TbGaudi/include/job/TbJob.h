/*! \brief TbJob class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2018
*/

#ifndef TB_RUN_H
#define TB_RUN_H

#include "TbGaudi.h"
#include "DutUtils.h"
#include "RunUtils.h"
#include "TbFitter.h"
#include <string>
#include <vector>
#include <functional>
#include "TTree.h"
#include "ROOT/RDataFrame.hxx"
#include <type_traits>

/// TODO:: TbJob::ReduceHotPixels(true);

class RootData;
class EcsData;
class Dut;
class Run;
class TbHistMaker;
using DutSharedPtr = std::shared_ptr<Dut>;
using RunSharedPtr = std::shared_ptr<Run>;
using TbHMakerSharedPtr = std::shared_ptr<TbHistMaker>;
using ConfigMap = std::map<std::string,std::string>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbJob: public RunUtils,
             public DutUtils,
			 public std::enable_shared_from_this<TbJob>{

	private:

		///
		RootData* m_root_data;

		///
        DutSharedPtr m_dut;

		///
		RunSharedPtr m_run;

		///
		TbHMakerSharedPtr m_hmaker;

		//
		ConfigMap m_config_map; // this will replace m_dut_details and m_run_details

		///
		static TbJobSharedPtr m_current_tbjob;

		///
		static int m_global_cluster_size;

		///
		bool AcknowledgeStudies() const;

		///
		bool AcknowledgeConfig() const;

	public:
        ///
		TbJob();

        ///
		~TbJob();

		/// \brief Set the current TbJob within any processing loop
		static void CurrentTbJob(TbJobSharedPtr run) {m_current_tbjob = run;}

        /// \brief Get the current TbJob within any processing loop
		static TbJobSharedPtr CurrentTbJob() {return m_current_tbjob;}

		///
		static void GlobalClusterSize(int size) {m_global_cluster_size = size;}

		///
		static int GlobalClusterSize() {return m_global_cluster_size;}

		///
		void AddConfig(const std::string& name, const std::string& value); 	// replace AddInfo

		///
		const std::string GetConfig(const std::string& name) const;					// replace GetInfo

		///
		template <typename T> const T GetConfig(const std::string& name) const;		// replace GetInfo

		///
		void Initialize();

		///
        DutSharedPtr DutPtr(){ return m_dut; }

        ///
        RunSharedPtr RunPtr(){ return m_run; }

	    /// \brief Link the test-beam data stored in the NTuple to the RDataFrame
		/// \param fullPathToNTuple the full path to the file (file name and its extension included).
		/// \param ttree_tbranches_map Map of the TTree names and corresponding container of default TBranches.
		bool LoadTbNTuple(const std::string fullPathToNTuple, std::map<std::string,std::vector<std::string>> ttree_tbranches_map);

        /// \brief Link the test-beam data stored in the NTuple to the list of pointers to the TTree objects
        /// \param fullPathToNTuple the full path to the file (file name and its extension included).
        /// \param ttrees list of TTree names (existing in the NTuple)
        bool LinkTbTTree(const std::string fullPathToNTuple, std::vector<std::string> ttrees);

        ///
        bool AddTbEcsFrame(const std::string& fullPathToEcsFrameFile);

		/// \brief Get the TTree pointer for a given TbJob
		/// \param ttreeName the TTree name of interest.
		//TTree* GetTTree(const std::string ttreeName) const;

		///
		void PrintInfo() const;

		///
		void PrintAllInfo() const;

		///
		const std::string Label(const std::string type=std::string()) const;

        ///
        const std::string DutLabel() const;

        ///
        const std::string RunLabel() const;

		///
		std::string Title();

		///
		RootData* RootDataPtr();

		///
		EcsData* EcsDataPtr();

		///
		ROOT::RDataFrame* RDataFramePtr(std::string name);

        ///
		void ClearEventLoopCollectedData();

		///
		void CreateDutStatisticHist(bool draw = false);

};


template <typename T>
const T TbJob::GetConfig(const std::string& name) const {
	const std::string config = GetConfig(name);
	if (std::is_same<T, int>::value) 			return std::stoi(config);
	else if (std::is_same<T, unsigned>::value) 	return static_cast<unsigned>(std::stoi(config));
	else if (std::is_same<T, float>::value) 	return std::stof(config);
	else if (std::is_same<T, double>::value) 	return std::stod(config);
	else TbGaudi::RunTimeError("Wrong GetConfig template argument given. Error in analysis workflow.");
}

#endif	// TbJob_H