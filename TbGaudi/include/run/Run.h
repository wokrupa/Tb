/*! \brief Run class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef RUNINFO_H
#define RUNINFO_H

// std libriaries
#include <iostream>
#include <string>
#include <map>

////////////////////////////////////////////////////////////////////////////////////
///
class Run {

	private:

	public:
		Run();
		virtual ~Run();

};
#endif	// RUNINFO_H
