//
// Created by brachwal on 24.09.18.
//

#ifndef TBDATAHANDLER_H
#define TBDATAHANDLER_H

// project libraries
#include "Globals.h"

////////////////////////////////////////////////////////////////////////////////////
///
class TbDataHandler {
    private:
        bool m_initialised;

        /// \brief The NTuple naming specification.
        std::string m_ntuple_name;

        /// \brief Indexes for the User's specified NTuple naming specification.
        std::vector<unsigned> m_ntuple_name_idx;

        /// \brief The list of the TTrees to be read-in from the NTuple file.
        std::vector<std::string> m_ttree_list;

        /// \brief The list of the default TBranches within the TTree
        std::vector<std::string> m_tbranch_list;

        /// \brief Map of a given TTree and vector od branches belong to it.
        std::map<std::string,std::vector<std::string>> m_ttree_branch_mapping;

        /// \brief List of variables specified in the <data> header of the .dat file.
        /// You should specify at least two obligatory columns: RunNumber and DutName.
        /// There is a standardisation behind - if the column specify
        /// - the Dut info you should add the Dut prefix
        /// - the test beam tbjob info you should add Run prefix
        /// If needed the unit can be specified within the squared brackets.
        /// The order doesn't matter.
        std::vector<std::string> m_variables;

        /// \brief List of units assigned to the variables and specified in the .dat file
        std::vector<std::string> m_units;

        /// \brief Vector of values mapped with the named variables.
        std::map<std::string,std::vector<std::string>> m_values;

        /// \brief Locations of the test beam ntuples.
        std::vector<std::string> m_data_location; // todo: clean TbGausi::m_DATA_LOCATION

        /// \brief Mapping of tbjob number with the actual idx of data location in the m_data_location container.
        std::map<std::string,unsigned> m_data_location_mapping;

        ///
        std::string m_file_extension;

        ///
        bool GetTbEcsData(const std::string& dbfile);

        ///
        bool GetTbNTupleData(const std::string& dbfile);

        /// \brief Parse the user data file specifying the test beam data to be analysed.
        /// \param dbfile user data file specifying the test beam data to be analysed.
        bool ParseNTupleDbFile(const std::string& dbfile);

        ///
        bool ParseEcsDbFile(const std::string& dbfile);

        /// \brief Find the idx-es of NTuple file naming, according to the NTuple naming specification.
        bool ParseDataFileName();

        /// \brief Get the idx-ed NTuple file name, according to the NTuple naming specification.
        const std::string GetFileName(unsigned ifile) const;

        ///
        const std::string GetFileName(unsigned ifile, unsigned iframe) const;

        /// \brief Get the actual path for the given NTuple file name.
        const std::string GetFilePath(unsigned ifile) const;

    public:
        /// Standard ctor
        TbDataHandler(): m_initialised(false){}

        /// Standard destructor
        ~TbDataHandler() = default;

        /// \brief Read-in the test beam data to be analysied.
        /// \param dbfile user data file specifying the test beam data to be analysed.
        bool GetTbData(const std::string& dbfile);

        /// \brief Print out the data specified be the user.
        void PrintDbInfo() const;

        /// \brief Print out the data sets specified be the user.
        void PrintTbDataInfo() const;
};

#endif //TBDATAHANDLER_H
