//
// Created by brachwal on 06.02.19.
//

#ifndef TBGAUDI_TBDATA_H
#define TBGAUDI_TBDATA_H

enum class DataType {ROOT, ECS};

////////////////////////////////////////////////////////////////////////////////////
///
class TbData {

    private:
        ///
        static DataType m_data_type;

    public:
        TbData()=default;
        virtual ~TbData()=default;

        ///
        static void Type(DataType type) { m_data_type = type;}

        ///
        static DataType Type() { return m_data_type;}
};
#endif //TBGAUDI_TBDATA_H
