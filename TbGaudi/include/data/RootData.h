//
// Created by brachwal on 24.09.18.
//

#ifndef ROOTDATA_H
#define ROOTDATA_H

// project libraries
#include "Globals.h"
#include "TbGaudi.h"

#include "ROOT/RDataFrame.hxx"


class TFile;
class TTree;
class TString;

typedef ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void> RDataFrameFiltered;

////////////////////////////////////////////////////////////////////////////////////
///
class RootData {

    protected:
        ///
        const std::string m_NTuple;

        ///
        TFile* m_TFile;

        /// \brief Map of the default TBranches and the TTree they belongs to.
        const std::map<std::string,std::vector<std::string>> m_TTree_TBranches;

        ///
        std::map<std::string,ROOT::RDataFrame*> m_RDataFrames;

        ///
        std::map<std::string,RDataFrameFiltered*> m_RDataFramesFiltered;

        ///
        std::map<std::string,TTree*> m_TTrees;

        ///
        static bool m_use_RDataFrame;

        ///
        bool LinkTTrees();

        /// \brief Extract the TTree name in case the TDirectory is given as well.
        /// \param ttree The TTree name (with or w/o the TDirectory specified)
        std::string TTreeName(std::string ttree);

    public:
        RootData() = default;
        RootData(const std::string ntuple);
        RootData(const std::string ntuple, std::map<std::string,std::vector<std::string>> ttree_tbranches);
        ~RootData();

        static void UseRDataFrame(bool flag){ m_use_RDataFrame=flag; }

        static bool UseRDataFrame(){ return m_use_RDataFrame; }

        bool LinkTTrees(std::vector<std::string> ttrees);

        ///
        ROOT::RDataFrame* RDataFramePtr(std::string name);

        ///
        void ListRDataFrames() const;

        ///
        RDataFrameFiltered* ApplyFilters(std::string name, std::vector<std::string>* filters);

        ///
        RDataFrameFiltered* ApplyFilter(std::string name, std::string filter);

        TTree* GetTTree(std::string name);
};

#endif //ROOTDATA_H
