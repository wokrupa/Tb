#include "TbResults.h"
#include "TbJobCollector.h"
#include "TbAnalysis.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "DutEllipticBinning.h"
#include "DutStripBinning.h"
#include "TbStyle.h"

// NEXT:
// 1) implement method DrawCombined(string, string);
////////////////////////////////////////////////////////////////////////////////////
///
TbResults::TbResults()
{
    m_results = std::make_unique<Result>();
}

////////////////////////////////////////////////////////////////////////////////////
///
TbResults::~TbResults()
{
    m_tbjob_results.clear();
    m_tbjob_plots.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::AddMappedResult(TbJobSharedPtr tbjob, std::string name, double val, double err){
    if(!Includes<TbJobSharedPtr>(m_tbjob_results,tbjob))
        m_tbjob_results[tbjob] = std::make_unique<Result>();
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::ErrorIfNotExists(TbJobSharedPtr tbjob){
    if(!Includes<TbJobSharedPtr>(m_tbjob_results,tbjob)){
        std::cout << "[ERROR]:: TbResults:: You call for the tbjob (RunNumber:"
                  <<tbjob->GetConfig("RunNumber")<<") which is not present in the results container."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}


////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawHitMap(TbJobSharedPtr tbjob, std::string hname){
    if(tbjob) ErrorIfNotExists(tbjob);
    auto hist = m_tbjob_results.at(tbjob)->Get<TH2D>(hname);
    if(hist) {
        if (!Includes<TbJobSharedPtr>(m_tbjob_plots, tbjob))
            m_tbjob_plots[tbjob] = TCanvasMap();
        // check if corresponding TCanvas was already created
        auto cname = hname + "_" + tbjob->Label();
        if (Includes<std::string>(m_tbjob_plots.at(tbjob), cname)) return;
        auto ctitle = hname + " " + tbjob->Title();
        auto can = std::make_unique<TCanvas>(TString("Can" + cname), TString(ctitle), 650, 600);
        can->cd();
        can->SetRightMargin(0.15);

        hist->SetStats(false);
        hist->Draw("COLZ");
        m_tbjob_plots.at(tbjob).insert(std::make_pair(cname, std::move(can)));
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutPixelHitMap(TbJobSharedPtr tbjob) {
    if(tbjob) ErrorIfNotExists(tbjob);
    // The name as defined in DutRootUtils::::CreateDutPixelStatisticHist()
    DrawHitMap(tbjob,"DutPixelStatistics");

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutStripHitMap(TbJobSharedPtr tbjob) {
    if(tbjob) ErrorIfNotExists(tbjob);
    // The name as defined in DutRootUtils::::CreateDutPixelStatisticHist()
    DrawHitMap(tbjob,"DutStripStatistics");

}
////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutBinningHitMap(TbJobSharedPtr tbjob){
    if(tbjob) ErrorIfNotExists(tbjob);
    // The name as defined in DutRootUtils::::CreateDutPixelStatisticHist()
    DrawHitMap(tbjob,"DutBinningStatistics");

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutMap(std::string name, unsigned run, std::string runIdentifier){

    if(run!=0) { // call for particular run
        // get pointer to TbJob of interest:
        auto tbJob = TbJobCollector::GetInstance()->GetTbJob(itos(run),runIdentifier);
        DrawDutMap(tbJob, name);
    }
    else {
        // call for all available runs
        for(auto& itbjob : m_tbjob_results) {
            DrawDutMap(itbjob.first,name);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutMap(TbJobSharedPtr tbjob, std::string name){
    if(tbjob) {
        if(name.empty()) {
            DrawDutPixelHitMap(tbjob);
            DrawDutBinningHitMap(tbjob);
            DrawDutMpvMap(tbjob);
        }
        else {
            if (name.compare("MPV") == 0)
                DrawDutMpvMap(tbjob);
            else if (name.compare("BinningHitMap") == 0)
                DrawDutBinningHitMap(tbjob);
            else if (name.compare("PixelHitMap") == 0)
                DrawDutPixelHitMap(tbjob);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutMpvMap(TbJobSharedPtr tbjob){
    // Note: The 2D map is binned per pixel
    if(tbjob) ErrorIfNotExists(tbjob);
    if(!ResultExits(tbjob, "MPV")) return;
    for (auto &irr : m_tbjob_results) {
        if(!Includes<TbJobSharedPtr>(m_tbjob_plots,tbjob))
            m_tbjob_plots[tbjob] = TCanvasMap();
        // check if corresponding TCanvas was already created
        auto cname = "CanMPV_"+tbjob->Label();
        if(Includes<std::string>(m_tbjob_plots.at(tbjob),cname)) continue;
        auto ctitle = "MPV "+tbjob->Title();
        auto can = std::make_unique<TCanvas>(TString(cname),TString(ctitle),650,600);
        auto hist = CreateDutBinningMap(tbjob,"MPV");
        can->cd();
        can->SetRightMargin(0.15);
        hist->SetStats(false);
        hist->Draw("COLZ");
        m_tbjob_plots.at(tbjob).insert(std::make_pair(cname,std::move(can)));
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutProfile(std::string name, unsigned run,std::string runIdentifier){

    if(run!=0) { // call for particular run
        // get pointer to TbJob of interest:
        auto tbJob = TbJobCollector::GetInstance()->GetTbJob(itos(run),runIdentifier);
        DrawDutProfile(tbJob, name);
    }
    else {
        // call for all available runs
        for(auto& itbjob : m_tbjob_results) {
            DrawDutProfile(itbjob.first,name);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutProfile(TbJobSharedPtr tbjob, std::string name){
    if(tbjob) {
        if(name.empty()) {
            DrawDutMPVProfile(tbjob);
            //
            // All other type of Profiles, to be implemented.
            //
        }
        else {
            if (name.compare("MPV") == 0)
                DrawDutMPVProfile(tbjob);
            //else if (name.compare("BinningHit") == 0)
            //    DrawDutBinningHitProfile(tbjob);
            // else if (name.compare("PixelHit") == 0)
            //    DrawDutPixelHitProfile(tbjob);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutMPVProfile(TbJobSharedPtr tbjob){
    if(tbjob) ErrorIfNotExists(tbjob);
    if(!ResultExits(tbjob, "MPV")) return;
    for (auto &irr : m_tbjob_results) {
        if(!Includes<TbJobSharedPtr>(m_tbjob_plots,tbjob))
            m_tbjob_plots[tbjob] = TCanvasMap();
        // check if corresponding TCanvas was already created
        auto cname = "CanMPVProfile_"+tbjob->Label();
        if(Includes<std::string>(m_tbjob_plots.at(tbjob),cname)) continue;
        auto ctitle = "MPV Profile "+tbjob->Title();
        auto can = std::make_unique<TCanvas>(TString(cname),TString(ctitle),650,600);
        auto columnProfile = GetDutProfile(tbjob,"MPV","column");
        auto rowProfile = GetDutProfile(tbjob,"MPV","row");
        auto color1 = TbStyle::iColor();
        columnProfile->SetMarkerColor(color1);
        columnProfile->SetLineColor(color1);
        auto color2 = TbStyle::iColor();
        rowProfile->SetMarkerColor(color2);
        rowProfile->SetLineColor(color2);

        std::string mgrName = "Mgr"+tbjob->Label();
        auto multiGraph = new TMultiGraph(TString(mgrName),TString("MPV profile"));
        multiGraph->Add(columnProfile.get(),"LPE1");
        multiGraph->Add(rowProfile.get(),"LPE1");
        multiGraph->SetTitle("MPV profile;Pixel number;MPV [electrons]");
        multiGraph->GetXaxis()->SetLimits(0,256);
        multiGraph->GetHistogram()->SetMinimum(0.95*GetMininumResult(tbjob,"MPV"));
        multiGraph->GetHistogram()->SetMaximum(1.15*GetMaximumResult(tbjob,"MPV"));

        auto legend = new TLegend(0.18,0.75,0.75,0.88);
        legend->AddEntry(columnProfile.get(),TString(ctitle + " column"),"p");
        legend->AddEntry(rowProfile.get(),TString(ctitle + " row"),"p");

        // export to the results container to make the TMultiGraph alive.
        AddMappedResult<TMultiGraph>(tbjob,mgrName,multiGraph);
        AddMappedResult<TLegend>(tbjob,"Legend"+mgrName,legend);
        can->SetLeftMargin(0.15);
        can->cd();
        multiGraph->Draw("A");
        legend->Draw("SAME");
        m_tbjob_plots.at(tbjob).insert(std::make_pair(cname,std::move(can)));
    }
}
////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutProfileCombined(std::string name, std::string axis){

    if (name.compare("MPV") == 0) DrawDutMpvProfileCombined(axis);
    //
    // Add here any other type of profiles to be combined...
    //

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawDutMpvProfileCombined(std::string axis){
    if(!ResultExits(m_tbjob_results.begin()->first,"MPV")) return;
    std::string name = "DutMpvProfileCombined_"+axis;
    if(!Includes<std::string>(m_compilation_plots,name)) {
        std::string mgrName = "Mgr"+name;
        auto multiGraph = new TMultiGraph(TString(mgrName),TString("MPV profile compilation"));
        auto legend = new TLegend(0.18,0.75,0.75,0.88);
        double minY(GetMininumResult(m_tbjob_results.begin()->first,"MPV"));
        double maxY(0);
        for (auto &irr : m_tbjob_results) {

            auto igraph = GetDutProfile(irr.first,"MPV",axis);
            multiGraph->Add(igraph.get(),"LPE1");
            legend->AddEntry(igraph.get(),TString(irr.first->Title()),"p");

            auto rMinY = GetMininumResult(irr.first,"MPV");
            auto rMaxY = GetMaximumResult(irr.first,"MPV");
            minY = rMinY < minY ? rMinY : minY;
            maxY = rMaxY > maxY ? rMaxY : maxY;

        }
        multiGraph->SetTitle(TString("MPV profile compilation ("+axis+");Pixel number;MPV [electrons]"));
        multiGraph->GetXaxis()->SetLimits(0,256);
        multiGraph->GetHistogram()->SetMinimum(0.95*minY);
        multiGraph->GetHistogram()->SetMaximum(1.15*maxY);

        // export to the results container to make the TMultiGraph alive.
        AddResult<TMultiGraph>(mgrName,multiGraph);
        AddResult<TLegend>("Legend"+mgrName,legend);

        auto cname = "Can"+name;
        auto ctitle = "MPV Profile Compilation ("+axis+")";
        auto can = std::make_unique<TCanvas>(TString(cname),TString(ctitle),650,600);
        can->SetLeftMargin(0.15);
        can->cd();
        multiGraph->Draw("A");
        legend->Draw("SAME");
        m_compilation_plots.insert(std::make_pair(name,std::move(can)));
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::DrawCombined(std::string xAxis, std::string yAxis){}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::Write(){
    TbGaudi::PrintBanner("INFO","TbResults::Exporting all results to .pdf files.");
    std::string path = TbAnalysis::OutputLocation();
    if(path.at(path.length()-1)=='/') path+=currentDate();
    else                        path+="/"+currentDate();
    namespace fs = boost::filesystem;
    fs::path dp (path);
    if(!fs::exists (dp)){
        std::cout << "[INFO]:: TbResults:: Created output directory "<<std::endl;
        std::cout << "[INFO]:: TbResults:: "<< path <<std::endl;
        fs::create_directories(dp);
    }

    //____________________________________________
    // Per tbjob plotting
    if(!m_tbjob_results.empty())
        std::cout << "[INFO]:: TbResults:: Exporting per tbjob results... " <<std::endl;
    for(auto& irr : m_tbjob_results){
        auto label = irr.first->Label();
        std::string fname = label+"_TbResults.pdf";
        fs::path fp = path+"/"+fname;
        if(fs::exists(fp)){
            std::cout << "[INFO]:: TbResults:: Remove existing results: " << fname <<std::endl;
            fs::remove(fp);
        }
        //____________________________________________
        // Create per tbjob plots if not created yet.
        gROOT->SetBatch(true);
        DrawDutBinningHitMap(irr.first);
        DrawDutStripHitMap(irr.first);
        DrawDutPixelHitMap(irr.first);
        DrawDutMpvMap(irr.first);
        DrawDutMPVProfile(irr.first);
        //
        // ...add any other plots to be created here.
        //
        gROOT->SetBatch(false);
        //____________________________________________

        if(!Includes<TbJobSharedPtr>(m_tbjob_plots,irr.first)) continue; // none plot to be exported
        auto nPlots = m_tbjob_plots.at(irr.first).size();
        switch (nPlots){
            case 0 : // shouldn't happen
                break;
            case 1:  // only one plot exists for given tbjob
                m_tbjob_plots.at(irr.first).begin()->second->Print(TString(path.c_str())+"/"+TString(fname.c_str()), "pdf");
                break;
            default:
                int ican = 0;
                for (auto& can : m_tbjob_plots.at(irr.first)){
                    if (ican == 0) {
                        can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "[", "pdf");
                    }
                    if (ican <= nPlots-1 ) {
                        can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()), "pdf");
                    }
                    if (ican == nPlots-1 ) {
                        can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "]", "pdf");
                    }
                    ican++;
                }
        }
    }
    //____________________________________________
    // All runs compilation plots
    std::string fname = "TbResults_AllRunsCompilation.pdf";
    fs::path fp = path+"/"+fname;
    if(fs::exists(fp)){
        std::cout << "[INFO]:: TbResults:: Remove existing results: " << fname <<std::endl;
        fs::remove(fp);
    }
    //____________________________________________
    // Create plots if not created yet.
    gROOT->SetBatch(true);
    if(ResultExits(m_tbjob_results.begin()->first,"MPV")) {
        DrawDutProfileCombined("MPV", "column");
        DrawDutProfileCombined("MPV", "row");
    }

    //
    // ...add any other compilation plots to be created here.
    //
    gROOT->SetBatch(false);
    //____________________________________________
    auto nPlots = m_compilation_plots.size();
    switch (nPlots){
        case 0 : // shouldn't happen
            break;
        case 1:  // only one plot exists for given run
            m_compilation_plots.begin()->second->Print(TString(path.c_str())+"/"+TString(fname.c_str()), "pdf");
            break;
        default:
            int ican = 0;
            for (auto& can : m_compilation_plots){
                if (ican == 0) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "[", "pdf");
                }
                if (ican <= nPlots-1 ) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()), "pdf");
                }
                if (ican == nPlots-1 ) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "]", "pdf");
                }
                ican++;
            }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::AddPlots(TbJobSharedPtr run, TCanvasMap plots){
    auto runNumber = run->GetConfig("RunNumber");
    std::cout << "[INFO]:: TbResults:: Adding new plots for the run  " << runNumber <<std::endl;
    if(!Includes<TbJobSharedPtr>(m_tbjob_plots,run))
        m_tbjob_plots[run] = TCanvasMap();
    for(auto& iplot:plots){
        m_tbjob_plots.at(run).insert(std::make_pair(iplot.first,std::move(iplot.second)));
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::shared_ptr<TH2D> TbResults::CreateDutBinningMap(TbJobSharedPtr run,std::string name){
    std::cout << "[INFO]:: TbResults:: Creating the DUT binning MAP: " << name << " for: "<<std::endl;
    run->PrintAllInfo();
    auto results = GetMappedResult<DutBinningResultsMap>(run, name);

    std::string hname = "DutBinning"+name+run->Label();
    std::string htitle = "Dut binning "+name+" "+run->Title();
    htitle+=";column;row";
    //unsigned nX(Dut::NPixelsX()), nY(Dut::NPixelsY());
    //double xLow(0.), xUp(Dut::NPixelsX());
    //double yLow(0.), yUp(Dut::NPixelsY());

    auto isDutPitchMapping = run->DutPtr()->IsEllipticBinning() || DutStripBinning::Is2DimMapping();

    unsigned nX  = isDutPitchMapping ? run->TH2PitchNBinsX() : run->TH2NBinsX();
    double xLow = 0.;
    double xUp = run->TH2Xup();

    unsigned nY  = isDutPitchMapping ? run->TH2PitchNBinsY() : run->TH2NBinsY();
    double yLow = 0.;
    double yUp = run->TH2Yup();


    auto histMap = std::make_unique<TH2D>(TString(hname),TString(htitle),nX,xLow,xUp,nY,yLow,yUp);

    auto dutBinning = run->DutPtr()->DutBinningPtr();
    double val,valErr;
    for (unsigned x = 0; x < nX; x++) {
        for (unsigned y = 0; y < nY; y++) {
            auto binNumber = isDutPitchMapping ? dutBinning->GetBinNumber(x, y) : x+y;
            if(Includes<unsigned>(*results,binNumber)){
                val    = results->at(binNumber).first;
                valErr = results->at(binNumber).second;
            } else {
                val = 0.;
                valErr = 0.;
            }
            histMap->SetBinContent(x + 1, y + 1, val);
            histMap->SetBinError(x + 1, y + 1, valErr);
        }
    }
    AddMappedResult<TH2D>(run,hname,static_cast<TH2D*>(histMap->Clone()));
    return GetMappedResult<TH2D>(run, hname);
}

////////////////////////////////////////////////////////////////////////////////////
///
std::shared_ptr<TGraphErrors> TbResults::GetDutProfile(TbJobSharedPtr run,std::string name, std::string axis){
    std::string gname = "DutProfile"+name+"_"+axis+run->Label();

    // check if not already created:
    if(GetMappedResult<TGraphErrors>(run, gname)) return GetMappedResult<TGraphErrors>(run, gname);

    // Create the TGraphErrors instance from the scratch:
    std::cout << "[INFO]:: TbResults:: Creating the DUT Profile: " << name << "("<<axis<<")" << " for: "<<std::endl;
    run->PrintInfo();
    auto results = GetMappedResult<DutBinningResultsMap>(run, name);
    auto dutBinning = run->DutPtr()->DutBinningPtr();

    // Create/get x-axis
    std::vector<double>& xVal = dutBinning->GetMiddleBinVector(axis);
    std::vector<double> xErr;
    for (int i = 0; i < xVal.size(); ++i) xErr.push_back(0.0001); // arbitrary small number

    std::cout << "[INFO]:: TbResults:: X-axis "<<std::endl;
    for (int i = 0; i < xVal.size(); ++i)
        std::cout << "[INFO]:: >> bin "<< i << ") x= "<<xVal.at(i)<<" +/- "<< xErr.at(i) <<std::endl;

    // Create y-axis
    std::vector<double> yVal;
    std::vector<double> yErr;
    double val, valErr;
    unsigned nSum;
    unsigned iDutBin;
    unsigned nDutBinsToAverage;
    for (unsigned x = 0; x < xVal.size(); x++) {
        val=0;
        valErr=0;
        nSum=1;
        if(run->GetConfig("DutBinningType").compare("Uniform")==0) {
            nDutBinsToAverage = sqrt(dutBinning->NBins());
            nSum=0;
            for (unsigned y = 0; y < nDutBinsToAverage; y++) {
                if(axis.compare("column")==0)
                    iDutBin = x+y*nDutBinsToAverage;
                else if(axis.compare("row")==0)
                    iDutBin = x*nDutBinsToAverage+y;
                std::cout << "[INFO]:: >> iDutBin "<< iDutBin <<std::endl;
                if(Includes<unsigned>(*results,iDutBin)){
                    std::cout << "[INFO]:: >> val "<< results->at(iDutBin).first << " +/- "<< results->at(iDutBin).second <<std::endl;
                    val    += results->at(iDutBin).first;
                    valErr += results->at(iDutBin).second*results->at(iDutBin).second;
                    ++nSum;
                }
            }
        }
        else if(run->GetConfig("DutBinningType").compare("Elliptic")==0) {
            unsigned col, row;
            if(axis.compare("column")==0){
                col = static_cast<unsigned>(xVal.at(x));
                row = dynamic_cast<DutEllipticBinning*>(dutBinning)->GetDutBinCentreY();

            }
            else if(axis.compare("row")==0){
                col = dynamic_cast<DutEllipticBinning*>(dutBinning)->GetDutBinCentreX();
                row = static_cast<unsigned>(xVal.at(x));
            }
            iDutBin = dutBinning->GetBinNumber(col,row);
            val    = results->at(iDutBin).first;
            valErr = results->at(iDutBin).second*results->at(iDutBin).second;
        }
        else {
            val = 0.;
            valErr = 0.;
        }
        yVal.push_back(val/nSum);
        yErr.push_back(sqrt(valErr));
    }

    std::cout << "[INFO]:: TbResults:: Y-axis "<<std::endl;
    for (int i = 0; i < yVal.size(); ++i)
        std::cout << "[INFO]:: >> bin "<< i << ") x= "<<yVal.at(i)<<" +/- "<< yErr.at(i) <<std::endl;

    std::string gtitle = "Dut Profile "+name+"("+axis+") "+run->Title()+";pixel number";

    auto graphProfile = std::make_unique<TGraphErrors>(xVal.size(),&xVal[0], &yVal[0], &xErr[0], &yErr[0]);
    graphProfile->SetName(TString(gname));
    graphProfile->SetTitle(TString(gtitle));
    graphProfile->SetMarkerStyle(20);
    graphProfile->GetXaxis()->SetLimits(0,256);
    AddMappedResult<TGraphErrors>(run,gname,static_cast<TGraphErrors*>(graphProfile->Clone()));
    return GetMappedResult<TGraphErrors>(run, gname);
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbResults::ResultExits(TbJobSharedPtr run, std::string name){
    if(run) ErrorIfNotExists(run);
    return m_tbjob_results.at(run)->Exists(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
double TbResults::GetMininumResult(TbJobSharedPtr run, std::string name){
    auto results = GetMappedResult<DutBinningResultsMap>(run, name);
    double min = results->begin()->second.first;
    for(auto& ir : *results){
        min = min < ir.second.first ? min : ir.second.first;
    }
    return min;
}

////////////////////////////////////////////////////////////////////////////////////
///
double TbResults::GetMaximumResult(TbJobSharedPtr run, std::string name){
    auto results = GetMappedResult<DutBinningResultsMap>(run, name);
    double max(0.);
    for(auto& ir : *results){
        max = max > ir.second.first ? max : ir.second.first;
    }
    return max;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbResults::PlotInteractive(TbJobSharedPtr run, std::string name){

}