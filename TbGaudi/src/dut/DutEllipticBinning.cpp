#include "DutEllipticBinning.h"
#include "IRRADProfile.h"
#include "TbAnalysis.h"
#include "DutBinning.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "Dut.h"
#include "TbJob.h"

////////////////////////////////////////////////////////////////////////////////////
///
DutEllipticBinning::DutEllipticBinning(Dut* owner, std::string name, unsigned nBins)
    : DutBinning(owner,name,nBins)
    , m_sx(static_cast<double>(Dut::NPixelsX())/2)
    , m_sy(static_cast<double>(Dut::NPixelsY())/2)
    , m_width_factor(2./3.)
{}

////////////////////////////////////////////////////////////////////////////////////
///
DutEllipticBinning::~DutEllipticBinning(){
	for (auto ie : m_bin_boundary_ellipses) delete ie;
    m_bin_boundary_ellipses.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::Initialize(){
    if(!IsInitialized()){
        if(m_fluence_profile) {
            //m_fluence_profile->Initialize();
            // NOTE: the elliptical binning make sense only for IRRAD type!
            auto irradProfile = dynamic_cast<IRRADProfile*>(m_fluence_profile.get());
            // verify the fluence profile correctness
            if (!irradProfile) {
                std::cout<<"[ERROR]:: DutEllipticBinning:: Trying to initialize"
                           " the elliptical binning for the profile other than \"IRRAD\"."
                           " It make no sense." << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }

            // redefine binning alignment with the values from the defined profile
            std::cout<<"[INFO]:: DutEllipticBinning:: Taking the ellipses alignemnt based on"
                       " the defined fluence profile"<<std::endl;

            if(irradProfile->IsDefault())
                std::cout<<"[WARNING]:: FluenceProfile:: It seems that the default parameterization is used!"<<std::endl;

            m_sx = irradProfile->GetSx();
            m_sy = irradProfile->GetSy();
            m_width_factor = irradProfile->GetWidthFactor();
        }
        else {
            std::cout << "[WARNING]:: DutEllipticBinning:: Initialization will be performed "
                         "w/o any fluence profile being defined, it means that the "
                         "default parameterization is used." << std::endl;
        }

        std::cout<<"[INFO]:: DutEllipticBinning:: Initialization starts..."<<std::endl;
        double dx = static_cast<double>(Dut::NPixelsX())/NBins() * m_width_factor;
        double dy = static_cast<double>(Dut::NPixelsY())/NBins();
        double middleBin;
        m_bin_boundary_ellipses.clear();
        m_middle_bin_column.clear();
        m_middle_bin_row.clear();
        for (int ie=1; ie <= NBins(); ie++){
            m_bin_boundary_ellipses.push_back(new TEllipse(m_sx,m_sy, dx*ie, dy*ie, 0., 360., 0.));
            if(ie<2){
                m_middle_bin_column.push_back(m_sx);
                m_middle_bin_row.push_back(m_sy);
            } else {
                // NOTE: add middleBin only if it's localized within DUT area!
                middleBin = m_sx + ie * dx - dx / 2; // x-axis: up from the centre
                if(middleBin < Dut::NPixelsX())
                    m_middle_bin_column.push_back(middleBin);
                middleBin = m_sx - ie * dx + dx / 2; // x-axis: down from the centre
                if(middleBin>0)
                    m_middle_bin_column.push_back(middleBin);
                middleBin = m_sy + ie * dy - dy / 2;  // y-axis: up from the centre
                if(middleBin<Dut::NPixelsY())
                    m_middle_bin_row.push_back(middleBin);
                middleBin = m_sy - ie * dy + dy / 2;  // y-axis: down from the centre
                if(middleBin>0)
                    m_middle_bin_row.push_back(middleBin);
            }

            if(TbGaudi::Debug()) {
                std::cout << "[DEBUG]:: #Ellipse " << ie;
                std::cout << " :: Centre: Sx: "<< std::setprecision(4) << m_sx << " Sy: "<< m_sy;
                std::cout << "  Radiuses: Rx: "<< std::setprecision(4) << dx * ie << " Ry: "<< dy * ie << std::endl;
            }
        }

        std::sort (m_middle_bin_column.begin(),m_middle_bin_column.end());
        std::sort (m_middle_bin_row.begin(),m_middle_bin_row.end());
        pixels_mapping();
        for(int i=0;i<NBins();++i)
            m_dut_bins.at(i)->DumpDutBinningMapping();
        m_is_initialized = true;
        std::cout<<"[INFO]:: DutEllipticBinning:: Initialization:: DONE."<<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::pixels_mapping(){
    std::cout<<"[INFO]:: DutEllipticBinning:: pixel mapping..."<<std::endl;
    unsigned bin=0;
    unsigned nEllipses = m_bin_boundary_ellipses.size();
    for (unsigned x = 0; x < Dut::NPixelsX(); x++) {
        for (unsigned y = 0; y < Dut::NPixelsY(); y++) {
            for (int i=nEllipses-1; i>=0;--i){ // check from outside to inside
                if (IsInsideTEllipse(m_bin_boundary_ellipses.at(i), x, y))
                    bin = i;
                else break;
            }
            m_dut_bins.at(bin)->AddPixel(x,y);

        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::BinIntegral(TF2* func, int bin, double normalization, double phimin, double phimax){

    // per pixel step
    double fdx = 1.;
    double fdy = 1.;
    double pix_area = 0.0055*0.0055; // cm^2;                   //TODO: to be defined globally
    double bin_area = 0.;			 // #pixels * pix_area


    TEllipse* el_low = nullptr;
    TEllipse* el_high= nullptr;

    if (bin > m_bin_boundary_ellipses.size()){
        std::cout<< "[ERROR]::DutEllipticBinning::BinIntegral:: to highest bin given" << std::endl;
        return 0.;
    }

    double x0, y0;
    double r1_low, r2_low, r1_high, r2_high;

    if (bin>1){
        el_low = m_bin_boundary_ellipses[bin-2];
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = el_low->GetR1();
        r2_low = el_low->GetR2();
    }
    else{
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = 0.;
        r2_low = 0.;
    }

    if (el_high){
        // common centre
        x0 = el_high->GetX1();
        y0 = el_high->GetY1();

        r1_high = el_high->GetR1();
        r2_high = el_high->GetR2();
    }

    double integral_low = 0.;
    double integral_high = 0.;
    double func_Xmin, func_Ymin, func_Xmax, func_Ymax;
    func->GetRange(func_Xmin, func_Ymin, func_Xmax, func_Ymax);

    double box_Xmin = func_Xmin; //x0 - r1 - 5*m_fdx; // //
    double box_Xmax = func_Xmax; //x0 + r1 + 5*m_fdx; // //
    double box_Ymin = func_Ymin; //y0 - r2 - 5*m_fdy; // //
    double box_Ymax = func_Ymax; //y0 + r2 + 5*m_fdy; // //

    double x = 1.;
    double y = 1.;

    int pix_counter_low = 0;
    int pix_counter_high = 0;

    while (x< 256.){     // TODO: #pix to be imported from global parameter
        y = 1.;
        while (y< 256){     // TODO: #pix to be imported from global parameter
            Double_t p[2] = {x,y};
            if ( func->IsInside(p) ){
                double phi = TMath::ATan2(y - y0, x - x0)*180./TMath::Pi();
                if (phi > phimin && phi < phimax ){
                    if (el_low){
                        if ( IsInsideTEllipse(el_low, x, y) ){
                            integral_low+=(func->Eval(x,y))*normalization;
                            pix_counter_low++;
                        }}
                    if (el_high){
                        if ( IsInsideTEllipse(el_high, x, y) ){
                            integral_high+=(func->Eval(x,y))*normalization;
                            pix_counter_high++;
                        }}
                }
            }
            y+=fdy;
        }
        x+=fdx;
    }
    return (integral_high - integral_low) / ((pix_counter_high - pix_counter_low) );
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::BinArea(int bin, double phimin, double phimax){

    // per pixel step
    double fdx = 1.;
    double fdy = 1.;
    double pix_area = 0.0055*0.0055; // cm^2; // TODO: to be imported from global parameter


    TEllipse* el_low = nullptr;
    TEllipse* el_high= nullptr;

    if (bin > m_bin_boundary_ellipses.size()){
        std::cout<< "[ERROR]::DutEllipticBinning::BinArea:: to highest bin given" << std::endl;
        return 0.;
    }

    double x0, y0;
    double r1_low, r2_low, r1_high, r2_high;

    if (bin>1){
        el_low = m_bin_boundary_ellipses[bin-2];
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = el_low->GetR1();
        r2_low = el_low->GetR2();
    }
    else{
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = 0.;
        r2_low = 0.;
    }

    if (el_high){
        // common centre
        x0 = el_high->GetX1();
        y0 = el_high->GetY1();

        r1_high = el_high->GetR1();
        r2_high = el_high->GetR2();
    }

    double x = 1.;
    double y = 1.;

    int pix_counter_low = 0;
    int pix_counter_high = 0;

    while (x< 256.){     // ToDo:: loop inside the box serrounding the ellipse (?)
        y = 1.;
        while (y< 256){
            Double_t p[2] = {x,y};
            double phi = TMath::ATan2(y - y0, x - x0)*180./TMath::Pi();
            if (phi > phimin && phi < phimax ){
                if (el_low){
                    if ( IsInsideTEllipse(el_low, x, y) ){
                        pix_counter_low++;
                    }}
                if (el_high){
                    if ( IsInsideTEllipse(el_high, x, y) ){
                        pix_counter_high++;
                    }}
            }
            y+=fdy;
        }
        x+=fdx;
    }
    return (pix_counter_high - pix_counter_low)*pix_area;
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::GetDutBinCentreX(){
    return m_bin_boundary_ellipses.at(0)->GetX1();
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::GetDutBinCentreY(){
    return m_bin_boundary_ellipses.at(0)->GetY1();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::InitializeDutPitchHitCounter(){
    if(m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NPixelsX(); x++) {
            std::vector<unsigned> row;
            for (int y = 0; y < Dut::NPixelsY(); y++)
                row.emplace_back(unsigned(0));
            m_dut_pixel_binning_counter.push_back(row);
        }
    } else {
        ResetDutPitchHitCounter();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::ResetDutPitchHitCounter(){
    if(!m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NPixelsX(); x++) {
            for (int y = 0; y < Dut::NPixelsY(); y++)
                m_dut_pixel_binning_counter.at(x).at(y) = 0;
        }
    } else {
        InitializeDutPitchHitCounter();
    }
}