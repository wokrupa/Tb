#include "DutUniformBinning.h"
#include "IRRADProfile.h"
#include "DutBinning.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "TbJob.h"

#include "memory.h"
#include <cmath>

// ROOT libriaryies
#include "TH2F.h"

////////////////////////////////////////////////////////////////////////////////////
///
DutUniformBinning::DutUniformBinning(Dut* owner, std::string name, unsigned nBins)
  : DutBinning(owner,name,nBins)
{
  std::cout<< "[INFO]:: DutUniformBinning construction for "<< name << " and nBins="<<nBins<<std::endl;

  Initialize(); // NOTE: there is no depandacy between fluence profile and this binning scheme,
                //        hence, it can be initialized during the instantation.
}

////////////////////////////////////////////////////////////////////////////////////
///
DutUniformBinning::~DutUniformBinning(){
    bin_boundary_col.clear();
    bin_boundary_row.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUniformBinning::Initialize(){
  if(!IsInitialized()) {
    // verify the dimension correctness
    if (NBins() > 0 && !(NBins() & (NBins() - 1))) { // power of 2 test
      if (!(NBins() & 0x55555555)) {            // power of 4 test, non-zero (=true) if any even bit it set
        std::cout << "[ERROR]:: DutUniformBinning:: You should specify number of bins being power of 4. Given: "
                  << NBins()
                  << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
      }
    }

    pixels_mapping();
    for(int i=0;i<NBins();++i)
      m_dut_bins.at(i)->DumpDutBinningMapping();

    int nBinsX = sqrt(NBins());
    int dpix = Dut::NPixelsX() / nBinsX;

    // Data needed to make lines of bins boundaries
    // and graph like results plotting
    bin_boundary_col.clear();
    bin_boundary_row.clear();
    m_middle_bin_column.clear();
    m_middle_bin_row.clear();

    for (int i = 1; i <= nBinsX; ++i) {
      if (i < nBinsX) {  // doesn't include sensor edge boundaries
        bin_boundary_col.push_back(i * dpix);
        bin_boundary_row.push_back(i * dpix);
      }
      m_middle_bin_column.push_back(i * dpix - dpix / 2);
      m_middle_bin_row.push_back(i * dpix - dpix / 2);

    }
    m_is_initialized = true;
    std::cout << "[INFO]:: DutUniformBinning::Initialization::DONE." << std::endl;
  }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUniformBinning::pixels_mapping(){
  // Simple mapping helper TH2F map
  auto binningMap = TH2F("BinningMap", "BinningMap"
                         ,Int_t(sqrt(NBins())),0.,Dut::NPixelsX()
                         ,Int_t(sqrt(NBins())),0.,Dut::NPixelsY());
  int binNumber = 0;
  for (unsigned row = 0; row < Dut::NPixelsX(); ++row) {
    for (unsigned col = 0; col < Dut::NPixelsY(); ++col) {

      Int_t colBin = binningMap.GetXaxis()->FindBin(col)-1;
      Int_t rowBin = binningMap.GetYaxis()->FindBin(row)-1;
      binNumber = colBin + sqrt(NBins()) * rowBin; // global/linearized bin number
      m_dut_bins[binNumber]->AddPixel(col, row);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUniformBinning::InitializeDutPitchHitCounter(){
  if(m_dut_pixel_binning_counter.empty()) {
    for (int x = 0; x < Dut::NPixelsX(); x++) {
      std::vector<unsigned> row;
      for (int y = 0; y < Dut::NPixelsY(); y++)
        row.emplace_back(unsigned(0));
      m_dut_pixel_binning_counter.push_back(row);
    }
  } else {
    ResetDutPitchHitCounter();
  }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUniformBinning::ResetDutPitchHitCounter(){
  if(!m_dut_pixel_binning_counter.empty()) {
    for (int x = 0; x < Dut::NPixelsX(); x++) {
      for (int y = 0; y < Dut::NPixelsY(); y++)
        m_dut_pixel_binning_counter.at(x).at(y) = 0;
    }
  } else {
    InitializeDutPitchHitCounter();
  }
}