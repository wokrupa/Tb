#include "DutBinning.h"
#include "Dut.h"
#include "TbGaudi.h"
#include "Globals.h"
#include "FluenceProfile.h"

////////////////////////////////////////////////////////////////////////////////////
DutBinningType DutBinning::m_binning_type  = DutBinningType::Uniform;

////////////////////////////////////////////////////////////////////////////////////
std::string DutBinning::m_global_fluence_profile_type = "IRRAD";

////////////////////////////////////////////////////////////////////////////////////
bool DutBinning::m_fluence_mapping=false;

////////////////////////////////////////////////////////////////////////////////////
unsigned DutBinning::m_global_nbins = 1; // by default the DUT area is not binned.

////////////////////////////////////////////////////////////////////////////////////
///
DutBinning::DutBinning(Dut* owner, std::string name, unsigned nBins)
    : m_owner(owner)
    , m_nbins(nBins)
    , m_fluence_profile(nullptr)
    , m_is_binning_stat_filled(false)
    , m_is_pixel_stat_filled(false)
    , m_is_initialized(false)
{
    for (int i=0; i < NBins(); ++i)
        m_dut_bins.emplace_back(new DutBin{name,i});
}

////////////////////////////////////////////////////////////////////////////////////
///
DutBinning::~DutBinning() {
    for (auto ib : m_dut_bins) delete ib;
    m_dut_bins.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
int DutBinning::GetBinNumber(unsigned X, unsigned Y){
    Initialize();
    if(NBins()==1) return 0;    // booster :)
    for (int i = 0; i < m_dut_bins.size() ; ++i) {
        if (m_dut_bins.at(i)->This(X, Y))
            return i;
    }
    return -1; // the given pixel was not mapped to any Dut bin
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::GlobalType(std::string type){
    bool flag = false;
    if (type.compare("Elliptic")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Elliptic"<< std::endl;
        DutBinning::m_binning_type =  DutBinningType::Elliptic;
        flag=true;
    }
    if (type.compare("Uniform")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Uniform"<< std::endl;
        DutBinning::m_binning_type =  DutBinningType::Uniform;
        flag=true;
    }
    if (type.compare("Strip")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Strip"<< std::endl;
        DutBinning::m_binning_type =  DutBinningType::Strip;
        flag=true;
    }
    if(!flag){
        std::cout<< "[ERROR]:: DutBinning::Type:: Unrecognized binning type: "<< type << std::endl;
        TbGaudi::RunTimeError("Error in User's analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutBinning::GlobalType(){
    switch (m_binning_type) {
        case DutBinningType::Uniform:   return "Uniform";
        case DutBinningType::Elliptic:  return "Elliptic";
        case DutBinningType::Strip:     return "Strip";
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::SetFluenceProfile(FluenceProfile* fluenceProfile){
    m_fluence_profile = FluenceProfile::AddProfile(this, fluenceProfile);
    m_fluence_profile->Initialize();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutBin(int binNumber){
    m_is_binning_stat_filled = true;
    if(binNumber<m_dut_bins.size()){
        m_dut_bins.at(binNumber)->Count();
    }
    else{
        std::cout<< "[ERROR]:: DutBinning:: You want count a bin which doesn't exist"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutBin(unsigned X, unsigned Y){
    m_is_binning_stat_filled = true;
    auto binNumber = GetBinNumber(X,Y);
    if(binNumber>=0) CountDutBin(binNumber);
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutPixel(unsigned X, unsigned Y){
    m_is_pixel_stat_filled = true;

    if(m_dut_pixel_binning_counter.empty())
        InitializeDutPitchHitCounter();

    if(X < m_dut_pixel_binning_counter.size()){
        if(Y<m_dut_pixel_binning_counter.at(X).size()){
            m_dut_pixel_binning_counter.at(X).at(Y)++;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutBinning::BinStatistic(unsigned binNumber){
    if(m_is_binning_stat_filled ) {
        if (binNumber < m_dut_bins.size()) {
            return m_dut_bins.at(binNumber)->Statistic();
        } else {
            std::cout << "[ERROR]:: DutBinning:: You call for statistic from a bin which doesn't exist" << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return 0; // make compiler happy
        }
    } else {
        std::cout << "[ERROR]:: DutBinning:: You call for statistic but it was not filled."<<std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return 0; // make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::ResetStatistic(){
    for(auto& bin : m_dut_bins) bin->ResetCounter();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::PrintStatistic(){
    unsigned bincounter{0};
    for(auto& bin : m_dut_bins){
        std::cout<< "[INFO]:: Statistics #"<<bincounter++<<" DUT bin: "
                 << bin->Statistic()<<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<double>& DutBinning::GetMiddleBinVector(std::string m_axis){

    bool getVectorX = m_axis.find("column")!=std::string::npos   ? true : false;
    bool getVectorY = m_axis.find("row")!=std::string::npos      ? true : false;
    if(getVectorX)
        return m_middle_bin_column;
    else if (getVectorY)
        return m_middle_bin_row;
    else {
        std::cout << "[ERROR]:: DutBinning:: Wrong axis specified for GetMiddleBinVector(...) "
                  << "given "<<m_axis<<". You can choose \"column\" or \"row\""<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}