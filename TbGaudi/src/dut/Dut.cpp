#include "Dut.h"
#include "DutBinning.h"
#include "DutUniformBinning.h"
#include "DutEllipticBinning.h"
#include "DutStripBinning.h"
#include "TbJob.h" // for current TbJob info

unsigned  Dut::m_n_pix_x = 256;
unsigned  Dut::m_n_pix_y = 256;
unsigned  Dut::m_pitch_size = 55;
unsigned  Dut::m_n_strip = 128;
float  Dut::m_strip_length = 98;
bool Dut::m_pix_study = true;
bool Dut::m_intra_pix_study = false;
bool Dut::m_strip_study = false;
bool Dut::m_intra_strip_study = false;
////////////////////////////////////////////////////////////////////////////////////
///
Dut::Dut() : m_dut_binning(nullptr), m_is_binned(false) {}

////////////////////////////////////////////////////////////////////////////////////
///
Dut::~Dut(){
    delete m_dut_binning;
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::CreateBinning(){

    auto name = TbJob::CurrentTbJob()->DutLabel();
    auto nBins = NBins();

    if(nBins==1) { // create single global Dut bin
        m_dut_binning = new DutUniformBinning{this, name, nBins};
        return;
    }

    std::cout << "[INFO]:: Dut::Creating the "
              << TbJob::CurrentTbJob()->GetConfig("DutBinningType")
              <<" DUT binning for "
              << TbJob::CurrentTbJob()->GetConfig("DutName")
              << " with number of bins: " << nBins << std::endl;

    if(IsUniformBinning())
        m_dut_binning = new DutUniformBinning{this,name,nBins};

    if(IsEllipticBinning())
        m_dut_binning = new DutEllipticBinning{this,name,nBins};

    if(IsStripBinning())
        m_dut_binning = new DutStripBinning{this, name, nBins};

    DutSizeInitialization();

    m_is_binned = true;
}
////////////////////////////////////////////////////////////////////////////////////
///
void Dut::DutSizeInitialization(){
    if(IsStripBinning() && !DutStripBinning::Is2DimMapping()){
        m_size_x = static_cast<float>(Dut::PitchSize() * Dut::NStrips()) / 1000;   // [mm]
        m_size_y = Dut::StripLength();                                             // [mm]
    }
    else {
        m_size_x = static_cast<float>(Dut::PitchSize() * Dut::NPixelsX()) / 1000; // [mm]
        m_size_y = static_cast<float>(Dut::PitchSize() * Dut::NPixelsY()) / 1000; // [mm]
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsStripBinning() const {
    return !TbJob::CurrentTbJob()->GetConfig("DutBinningType").compare("Strip");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsUniformBinning() const {
    return !TbJob::CurrentTbJob()->GetConfig("DutBinningType").compare("Uniform");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsEllipticBinning() const {
    return !TbJob::CurrentTbJob()->GetConfig("DutBinningType").compare("Elliptic");
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned Dut::NBins() const {
    return std::stoi(TbJob::CurrentTbJob()->GetConfig("DutNBins"));
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::PixStudy(bool val) {
    m_pix_study = val;
    if(m_pix_study){
        m_intra_pix_study = false;
        m_strip_study = false;
        m_intra_strip_study = false;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::IntraPixStudy(bool val) {
    m_intra_pix_study = val;
    if(m_intra_pix_study){
        m_pix_study = false;
        m_strip_study = false;
        m_intra_strip_study = false;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::StripStudy(bool val) {
    m_strip_study = val;
    if(m_pix_study){
        m_pix_study = false;
        m_intra_pix_study = false;
        m_intra_strip_study = false;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::IntraStripStudy(bool val) {
    m_intra_strip_study = val;
    if(m_intra_pix_study){
        m_pix_study = false;
        m_intra_pix_study = false;
        m_strip_study = false;
    }
}