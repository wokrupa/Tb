#include "DutBin.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "DutBinning.h"
#include "TbJob.h"
#include <algorithm>

DutFramesMap DutBin::m_binned_readout_channels = DutFramesMap{};
////////////////////////////////////////////////////////////////////////////////////
///
DutBin::DutBin(std::string name, unsigned int binNumber)
	: m_name(name)
	, m_bin_number(binNumber)
	, m_statistic_counter(0)
{}

////////////////////////////////////////////////////////////////////////////////////
///
DutReadoutChannelsFrame& DutBin::ReadoutChannelsFrame(std::string name,unsigned bin){
	if(Includes<std::string>(m_binned_readout_channels,name)) {
		if(DutBin::m_binned_readout_channels.at(name).size()>bin)
			return DutBin::m_binned_readout_channels.at(name).at(bin);
		else {
			std::cout << "[ERROR]:: DutBin:: You call for Pixels map for DUT bin which do not exists!"<< std::endl;
			std::cout << "[ERROR]:: "<<name<<": bin "<<bin<< std::endl;
			TbGaudi::RunTimeError("Error in analysis workflow.");
		}
	} else {
		std::cout << "[ERROR]:: DutBin:: You call for Pixels map ("<<name<<") which was not created!"<< std::endl;
		std::cout << "[INFO]:: DutBin:: Available maps: "<< std::endl;
		DutBin::PrintListOfDutBinningPixelsMaps();
		TbGaudi::RunTimeError("Error in analysis workflow.");
	}
}
////////////////////////////////////////////////////////////////////////////////////
///
std::vector<unsigned short>& DutBin::Strips(std::string name,unsigned bin){
	return DutBin::ReadoutChannelsFrame(name,bin).at(0);
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBin::PrintListOfDutBinningPixelsMaps() {
	for(auto& imap : DutBin::m_binned_readout_channels ) std::cout << "[INFO]:: DutBin:: PixMap:: " << imap.first << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBin::AddPixel(unsigned X, unsigned Y){

	if(X>Dut::NPixelsX() || Y > Dut::NPixelsY())
		std::cout << "[WARNING]:: DutBin:: Trying to add out of range pixel ?!"<< std::endl;

	// check if for given DUT name the map already exists, if not create one:
	if(!Includes<std::string>(DutBin::m_binned_readout_channels,m_name)) {
		auto nDutBins = TbJob::CurrentTbJob()->GetConfig<unsigned>("DutNBins");
		unsigned nColumns = Dut::NPixelsX();
		unsigned nRows    = Dut::NPixelsY();
		if(TbJob::CurrentTbJob()->GetConfig("DutBinningType")=="Strip") {
			nColumns = Dut::NStrips();
			nRows = 1;
		}
		//std::vector<unsigned short> pixels_in_row(nColumns,0); // initialize with zeros
		//DutReadoutChannelsFrame pixels_frame (nRows,pixels_in_row);
		std::vector<unsigned short> pixels_in_row(nRows,0); // initialize with zeros
		DutReadoutChannelsFrame pixels_frame (nColumns, pixels_in_row);
		DutBin::m_binned_readout_channels[m_name] = DutFramesVector(nDutBins,pixels_frame);
	}

	// Add/fire given pixel in the DutReadoutChannelsFrame:
	auto& pixels_frame = DutBin::m_binned_readout_channels.at(m_name).at(m_bin_number);
	pixels_frame.at(X).at(Y) = 1;

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBin::AddStrip(unsigned X){
	AddPixel(X,0); // this is the way the strips is mapped within the DutFramesVector
}

////////////////////////////////////////////////////////////////////////////////////
///
bool DutBin::This(unsigned X, unsigned Y){
    if(TbJob::CurrentTbJob()->GetConfig("DutBinningType")!="Strip"
    	&& (X>Dut::NPixelsX() || Y > Dut::NPixelsY())) {
        std::cout << "[WARNING]:: DutBin::This() - asking for out of range pixel?!" << std::endl;
        return false;
    }
	if(TbJob::CurrentTbJob()->GetConfig("DutBinningType")=="Strip"
		&& X>Dut::NStrips()) {
		std::cout << "[WARNING]:: DutBin::This() - asking for out of range pixel/strip ?!" << std::endl;
		return false;
	}
	auto pixels_frame = DutBin::ReadoutChannelsFrame(m_name, m_bin_number);
	return pixels_frame.at(X).at(Y);
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBin::DumpDutBinningMapping() const {
	auto& pixels_frame = DutBin::ReadoutChannelsFrame(m_name,m_bin_number);

	for(auto& icol : pixels_frame){
		if(TbJob::CurrentTbJob()->GetConfig("DutBinningType")!="Strip") { // and TwoDimMapping!
			for (auto &pix : icol) { // irow
				auto pix_char = pix ? "o" : "-";
				std::cout << pix_char;
			}
			std::cout <<std::endl;
		} else {
			auto pix_char = icol.at(0) ? "o" : "-";
			std::cout << pix_char;
		}
	}
	std::cout<<std::endl<<std::endl;
}