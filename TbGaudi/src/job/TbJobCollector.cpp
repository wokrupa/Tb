//
// Created by brachwal on 08.10.18.
//

#include "TbDataHandler.h"
#include "TbAnalysis.h"
#include "TbJobCollector.h"
#include "TbJob.h"
#include "TbGaudi.h"
#include "TbResults.h"
#include <memory>

TbJobCollector::TbJobCollector(){}

////////////////////////////////////////////////////////////////////////////////////
///

TbJobCollector::~TbJobCollector(){}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobCollector::LoadData(){
    auto tbdata_handler = std::make_unique<TbDataHandler>();
    if(!tbdata_handler->GetTbData(TbAnalysis::TbDataDB())){
        std::string error= "[ERROR]::TbDataHandler: There is sth wrong with the data file ";
        error+="specifying the test beam data to be analysed";
        TbGaudi::RunTimeError(error);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobCollector::AddTbJob(TbJobSharedPtr tbJob){
    std::cout<< "[INFO]:: TbJobCollector::Adding new TbJob... " << std::endl;
    TbJob::CurrentTbJob(tbJob);
    tbJob->Initialize();
    tbJob->DutPtr()->CreateBinning();
    m_tbjobs.push_back(tbJob);
    /*if(TbGaudi::Debug())
        tbJob->PrintAllInfo();
    else
        tbJob->PrintInfo();*/
}

////////////////////////////////////////////////////////////////////////////////////
///
TbJobSharedPtr TbJobCollector::GetTbJob(std::string runNumber,std::string runIdentifier){
    for(auto irun : m_tbjobs){
        if(runIdentifier.empty()) {
            if (irun->GetConfig("RunNumber").compare(runNumber) == 0) return irun;
        }
        else {
            if (irun->GetConfig("RunNumber").compare(runNumber) == 0){
                std::size_t separator = runIdentifier.find("::");
                if(separator== std::string::npos)
                    TbGaudi::RunTimeError("GetTbJob::Couldn't recognize the separator in runIdentifier, you should use'::'");
                auto key = runIdentifier.substr(0, separator);
                auto val = runIdentifier.substr(separator+2, runIdentifier.length());
                auto info = irun->GetConfig(key);
                if(info.empty())
                    TbGaudi::RunTimeError("GetTbJob::The specified run identifier key ("+key+") was not found.");
                if(info.compare(val)==0)
                    return irun;
            }
        }
    }
    return nullptr;

}