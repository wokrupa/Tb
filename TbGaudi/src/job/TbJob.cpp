#include "TbJob.h"
#include "RootData.h"
#include "DutPix.h"
#include "DutIntraPix.h"
#include "DutStrip.h"
#include "DutIntraStrip.h"
#include "EcsData.h"
#include "Globals.h"
#include "TH1D.h"
#include "TbHistMaker.h"
#include <memory>

////////////////////////////////////////////////////////////////////////////////////
///
TbJobSharedPtr TbJob::m_current_tbjob = nullptr;
int TbJob::m_global_cluster_size = -1;
////////////////////////////////////////////////////////////////////////////////////
///
TbJob::TbJob() : m_root_data(nullptr){}

////////////////////////////////////////////////////////////////////////////////////
///
TbJob::~TbJob(){
    delete m_root_data;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::Initialize() {

    // create and set a new Run
    m_run = std::make_shared<Run>();
    SetRun(m_run);

    AcknowledgeStudies();

    // create and set a new Dut
    if(Dut::PixStudy())
        m_dut = std::make_shared<DutPix>();
    else if (Dut::IntraPixStudy())
        m_dut = std::make_shared<DutIntraPix>();
    else if (Dut::StripStudy())
        m_dut = std::make_shared<DutStrip>();
    else if (Dut::IntraStripStudy())
        m_dut = std::make_shared<DutIntraStrip>();

    SetDut(m_dut);

    // Default TbHistMaker
    m_hmaker = std::make_shared<TbHistMaker>(this->shared_from_this());
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::AcknowledgeStudies() const {
    unsigned count{0};
    if(Dut::PixStudy()) ++count;
    if(Dut::IntraPixStudy()) ++count;
    if(Dut::StripStudy()) ++count;
    if(Dut::IntraStripStudy()) ++count;
    if(count != 1){
        std::cout << "[ERROR]:: TbJob:: AcknowledgeStudies:: "
                     " There is wrong number of DUT study type set." << std::endl;
        std::cout << "[INFO]:: You should set only one of the following:\n"
                     "\t(1) Dut::PixStudy(true) or\n"
                     "\t(2) Dut::IntraPixStudy(true) or\n"
                     "\t(3) Dut::StripStudy(true) or\n"
                     "\t(4) Dut::IntraStripStudy(true)" << std::endl;
        TbGaudi::RunTimeError("Ambiguous analysis setup.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::AcknowledgeConfig() const {

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::AddConfig(const std::string& name, const std::string& value){

    // Check if insertion is successful or not, if not replace the previous value
    if (m_config_map.insert(std::make_pair(name, value)).second == false) {
         std::cout << "[WARNING]:: TbJob::AddConfig:: The " << name << " value replaced already existed one."
                   << "\"" << m_config_map[name] << "\" ==> \"" << value << "\"" << std::endl;
        m_config_map[name] = value;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbJob::GetConfig(const std::string& name) const {
    auto search = m_config_map.find(name);
    if(search != m_config_map.end())
        return search->second;
    else
        return std::string("0");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::LoadTbNTuple(const std::string fullPathToNTuple, std::map<std::string,std::vector<std::string>> ttree_tbranches_map){
    std::cout<< "[INFO]:: TbJob loading the test-beam data stored in the NTuple: " << std::endl;
    std::cout<< "[INFO]:: " << fullPathToNTuple << std::endl;

    if(m_root_data){
        std::cout<< "[WARNING]:: The ROOT data already loaded for this run. Deleting it." << std::endl;
        delete m_root_data;
    }
    m_root_data = new RootData{fullPathToNTuple, ttree_tbranches_map};

    return true;
}

////////////////////////////////////////////////////////////////////////////////////
/// Link to the TFile and assign the corresponding TTree pointer
bool TbJob::LinkTbTTree(const std::string fullPathToNTuple, std::vector<std::string> ttrees){
    if(m_root_data){
        std::cout<< "[WARNING]:: The ROOT data already loaded for this run. Deleting it." << std::endl;
        delete m_root_data;
    }
    m_root_data = new RootData{fullPathToNTuple};
    m_root_data->LinkTTrees(ttrees);
    std::cout<< "[INFO]:: TbJob::LinkTbTTree:: The ROOT data linked with the TTrees:";
    for(const auto& itree : ttrees) std::cout<< " " << itree; std::cout<<std::endl;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::AddTbEcsFrame(const std::string& fullPathToEcsFrameFile){
    if(!m_root_data) m_root_data = new EcsData{};
    static_cast<EcsData*>(m_root_data)->AddFrame(fullPathToEcsFrameFile);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::PrintInfo() const {
    std::cout << std::endl;
    std::cout << "[INFO]:: >>  DUT: " << GetConfig("DutName") << " run: " <<GetConfig("RunNumber") << std::endl;
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::PrintAllInfo() const { // todo: rename to PrintConfig()
    std::cout << std::endl;
    //PrintDutInfo();
    //PrintRunInfo();
    for(auto& config : m_config_map )
        std::cout << "[INFO]:: >> " << config.first << " :: " << config.second << std::endl;
    std::cout << std::endl;

}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbJob::Label(std::string type) const {
    if(type.empty()) return DutLabel()+RunLabel();
    else if(type.compare("DUT")==0) return DutLabel();
    else if (type.compare("RUN")==0) return RunLabel();
    else {
        std::cout << "[ERROR]:: TbJob:: DutLabel:: Trying to extract label of type ("<<type<<") which do not exists!"
                     " You can choose \"DUT\" or \"RUN\" (or both of them with non argument given) " << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbJob::DutLabel() const {
    std::string label = GetConfig("DutName");
    label+="_nBins_"+GetConfig("DutNBins");
    label+="_"+GetConfig("DutBinningType");
    label+="_"+GetConfig("DutFluenceProfileType");
    return label;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbJob::RunLabel() const {
    return GetConfig("RunNumber")+"_"
           +"bias"+GetConfig("RunBiasVoltage")+"V_"
           +"clS"+GetConfig("RunClusterSize");
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbJob::Title() {
    std::string title;
    title+=GetConfig("DutName");
    title+=" "+GetConfig("RunNumber");
    title+=" "+GetConfig("RunBiasVoltage")+"[V]";
    title+=" clS"+GetConfig("RunClusterSize");
    return title;
}

////////////////////////////////////////////////////////////////////////////////////
///
RootData* TbJob::RootDataPtr(){
    return m_root_data ? m_root_data : nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
EcsData* TbJob::EcsDataPtr(){
    return m_root_data ? static_cast<EcsData*>(m_root_data) : nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
ROOT::RDataFrame* TbJob::RDataFramePtr(std::string name){
    if(m_root_data)
        return m_root_data->RDataFramePtr(name);
    else
        return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::ClearEventLoopCollectedData(){
    ClearAllDutHist();
    ClearAllDutDataSet();
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::CreateDutStatisticHist(bool draw) {
    m_hmaker->MakeDutPixelStatisticHist(draw);
    m_hmaker->MakeDutBinningStatisticHist(draw);
}