//
// Created by brachwal on 12.10.18.
//
#include "DutUtils.h"
#include "TbAnalysis.h"
#include "TbGaudi.h"
#include "Globals.h"
#include "TbJob.h"


////////////////////////////////////////////////////////////////////////////////////
///
const unsigned& DutUtils::GetCounterStatistics(unsigned idx) {
    if (Includes(m_event_counter, idx)) {
        return m_event_counter.at(idx);
    }
    else{
        std::cout << "[ERROR]:: DutUtils:: You are calling for event counter which don not exists" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUtils::PrintAllCountersStatistics() {

    for(auto& counter : m_event_counter){
        std::cout << "[INFO]:: Statistics:: "
                  << m_event_counter_name.at(counter.first)
                  << " : " << GetCounterStatistics(counter.first)<<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutUtils::CountDutHit(unsigned col, unsigned row){
    auto dutBinning = m_dut->DutBinningPtr();
    if(!dutBinning->IsInitialized())
        dutBinning->Initialize();
    if(m_dut->IsBinned()) // more than one Dut bin
        dutBinning->CountDutBin(col,row);
    dutBinning->CountDutPixel(col,row);
}

////////////////////////////////////////////////////////////////////////////////////
///
int DutUtils::DutBinNumber(unsigned X, unsigned Y){
    auto dutBinning = m_dut->DutBinningPtr();
    if(!dutBinning->IsInitialized())
        dutBinning->Initialize();
    return dutBinning->GetBinNumber(X,Y);
}

////////////////////////////////////////////////////////////////////////////////////
///
int DutUtils::DutBinNumber(unsigned X){
    auto dutBinning = m_dut->DutBinningPtr();
    if(!dutBinning->IsInitialized())
        dutBinning->Initialize();
    return dutBinning->GetBinNumber(X);
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned& DutUtils::EventCounter(unsigned idx) {
    if (Includes(m_event_counter, idx)) {
        return m_event_counter.at(idx);
    }
    else{
        std::cout << "[ERROR]::DutUtils:: You are calling for event counter which do not exists" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}
////////////////////////////////////////////////////////////////////////////////////
///
void DutUtils::DefineEventCounter(std::string name, unsigned idx ){
    if(!Includes(m_event_counter,idx)){
        m_event_counter[idx] = 0;
        m_event_counter_name[idx] = name;
    }
    else{
        std::cout << "[ERROR]::DutUtils:: You are defining already used index." << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}
////////////////////////////////////////////////////////////////////////////////////
/// TODO: move this definition to RunUtils
bool DutUtils::IsGoodClusterSize(UInt_t clSize){
    // Define associated event counter (for events not passing the requirement)
    if(!Includes(m_event_counter_priv_idx,999u)) { // put kind of unique counter index
        m_event_counter_priv_idx.push_back(999u);
        DefineEventCounter("Wrong cluster size", m_event_counter_priv_idx.back());
    }
    // Verify the actual condition
    int runClSize = stoi(TbJob::CurrentTbJob()->GetConfig("RunClusterSize"));
    if(runClSize==-1 || runClSize==clSize)
        return true;
    else{
        EventCounter(999u)++;
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool DutUtils::IsClusterInsideDutBin(Int_t* col,Int_t* row, UInt_t size){
    // Define associated event counter (for events not passing the requirement)
    if(!Includes(m_event_counter_priv_idx,998u)) { // put kind of unique counter index
        m_event_counter_priv_idx.push_back(998u);
        DefineEventCounter("In between DUT bin clusters", m_event_counter_priv_idx.back());
    }
    // Verify the actual condition
    std::vector<int> pixel_dut_bin;
    for(unsigned x=0; x<size; x++) { // take into account each pixel from the cluster
        auto icol = static_cast<unsigned>(*col++);
        auto irow = static_cast<unsigned>(*row++);
        pixel_dut_bin.push_back(m_dut->DutBinningPtr()->GetBinNumber(icol,irow));
    }
    for(auto ibin: pixel_dut_bin) {
        if (ibin < 0) {  // omit bin edge pixels
            EventCounter(998u)++;
            return false;
        }
        // accept cluster if its whole area belongs to the same DUT bin
        if(ibin != pixel_dut_bin.at(0)){
            EventCounter(998u)++;
            return false;
        }
    }
    return true;
}

