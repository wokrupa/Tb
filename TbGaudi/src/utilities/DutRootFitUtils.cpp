//
// Created by brachwal on 11.10.18.
//

#include "DutRooFitUtils.h"
#include "DutBinning.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include "Globals.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
DutRooFitUtils::~DutRooFitUtils() {
    if(!m_dut_binning_data_set.empty()) {
        std::cout << "[INFO]:: DutRooFitUtils:: Release memory. You can consider"
                  << " to call ClearEventLoopCollectedData() within your TbJobs loop."<<std::endl;
        ClearAllDutDataSet();
    }
}
////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::DefineDutBinningDataSet(std::string name, double xLow, double xUp, std::string unit){
    if(!Includes<std::string>(m_dut_binning_data_set,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRooFitUtils::Defining new set of RooDataSet DUT binning data: " << name << std::endl;
        m_dut_binning_data_set[name] = std::vector<RooDataSet*>{};
        m_dut_binning_data_var[name] = std::vector<RooRealVar*>{};
        m_dut_binning_data_arg_set[name] = std::vector<RooArgSet*>{};

        auto nDutBins = m_dut->NBins();
        for (unsigned i = 0; i < nDutBins; ++i) {
            auto rrvName = "RRV_"+name+"_Bin"+itos(i);
            auto rdsName = "RDS_"+name+"_Bin"+itos(i);
            auto title = name+" from DUT bin "+itos(i);
            //if(TbGaudi::Debug())
            //    std::cout << "[INFO]:: Adding new RooDataSet \"" << title << "\"" << std::endl;

            auto rrv = new RooRealVar(TString(rrvName),TString(title),xLow,xUp, TString(unit));
            auto ras = new RooArgSet{*rrv};
            auto rds = new RooDataSet(TString(rdsName),TString(title), *ras);

            m_dut_binning_data_set.at(name).push_back(rds);
            m_dut_binning_data_var.at(name).push_back(rrv);
            m_dut_binning_data_arg_set.at(name).push_back(ras);
        }
    }
    else{
        std::cout << "[ERROR]::DutRooFitUtils::DefineDutBinningDataSet: The specified set of RooDataSets"
                     " already exists. Call ClearDutDataSet("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::DefineDutDataSet(std::string name, double xLow, double xUp, std::string unit){
    if(!Includes<std::string>(m_dut_data_set,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRooFitUtils::Defining new DUT RooDataSet DUT: " << name << std::endl;

        auto rrvName = "RRV_"+name;
        auto rdsName = "RDS_"+name;
        auto title = name;
        m_dut_data_var[name] = new RooRealVar(TString(rrvName),TString(title),xLow,xUp, TString(unit));
        m_dut_data_arg_set[name] = new RooArgSet{*(m_dut_data_var.at(name))};
        m_dut_data_set[name] = new RooDataSet(TString(rdsName),TString(title), *(m_dut_data_arg_set.at(name)));
    }
    else{
        std::cout << "[ERROR]::DutRooFitUtils::DefineDutDataSet: The specified set of RooDataSets"
                     " already exists. Call ClearDutDataSet("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::DefineDutBinningDataSetType(std::string rdsName, std::string typeName, int idx){
    // Check if RooDataSet of interest exists
    if(Includes<std::string>(m_dut_binning_data_set,rdsName)){

        // Check if RooCategory for a given RooDataSet already exists, if not create one
        if(!Includes<std::string>(m_dut_binning_data_category,rdsName)){

            if(TbGaudi::Debug())
                std::cout << "[INFO]:: DutRooFitUtils::Defining new RooCategory Type for DUT binning data: "
                << rdsName << " : " << typeName << std::endl;

            m_dut_binning_data_category[rdsName] = std::vector<RooCategory*>{};
            auto nDutBins = m_dut->NBins();
            for (unsigned i = 0; i < nDutBins; ++i) {
                auto rcName = "RC_"+rdsName+"_Bin"+itos(i);
                auto rc = new RooCategory(TString(rcName),"Sample");
                m_dut_binning_data_category.at(rdsName).push_back(rc);
                // Add the new RooCategory to the corresponding RooArgSet
                m_dut_binning_data_arg_set.at(rdsName).at(i)->add(*(m_dut_binning_data_category.at(rdsName).back()));
                // Define actual type
                m_dut_binning_data_category.at(rdsName).back()->defineType(TString(typeName),Int_t(idx));
            }
        }
        else{
            if(TbGaudi::Debug())
                std::cout << "[INFO]:: DutRooFitUtils::Defining new RooCategory Type for DUT binning data: "
                          << rdsName << " : " << typeName << std::endl;

            for (auto& ic : m_dut_binning_data_category.at(rdsName)) {
                // Define actual type
                // NOTE: The method itself return Error status if state with given name
                // or index is already defined
                ic->defineType(TString(typeName),Int_t(idx));
            }
        }
    }
    else{
        std::cout << "[ERROR]::DutRooFitUtils::DefineDutBinningDataSetType: The specified set of RooDataSets"
                     " do not exists. Call DefineDutBinningDataSet("<<rdsName<<",...) first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::DefineDutDataSetType(std::string rdsName, std::string typeName, int idx){
    // Check if RooDataSet of interest exists
    if(Includes<std::string>(m_dut_data_set,rdsName)){

        // Check if RooCategory for a given RooDataSet already exists, if not create one
        if(!Includes<std::string>(m_dut_data_category,rdsName)){

            if(TbGaudi::Debug())
                std::cout << "[INFO]:: DutRooFitUtils::Defining new RooCategory Type for DUT binning data: "
                          << rdsName << " : " << typeName << std::endl;

            auto rcName = "RC_"+rdsName;
            m_dut_data_category[rdsName] = new RooCategory(TString(rcName),"Sample");
            // Add the new RooCategory to the corresponding RooArgSet
            m_dut_data_arg_set.at(rdsName)->add(*(m_dut_data_category.at(rdsName)));
            // Define actual type
            m_dut_data_category.at(rdsName)->defineType(TString(typeName),Int_t(idx));

        }
        else{
            if(TbGaudi::Debug())
                std::cout << "[INFO]:: DutRooFitUtils::Defining new RooCategory Type for DUT binning data: "
                          << rdsName << " : "
                          << typeName << std::endl;
            // Define actual type
            // NOTE: The method itself return Error status if state with given name
            // or index is already defined
            m_dut_data_category.at(rdsName)->defineType(TString(typeName),Int_t(idx));
        }
    }
    else{
        std::cout << "[ERROR]::DutRooFitUtils::DefineDutDataSetType: The specified set of RooDataSets"
                     " do not exists. Call DefineDutBinningDataSet("<<rdsName<<",...) first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::ClearAllDutDataSet(){

    for (auto &idutdataset : m_dut_binning_data_set) {
        ClearDutDataSet(idutdataset.first);
    }
    m_dut_binning_data_var.clear();
    m_dut_binning_data_category.clear();
    m_dut_binning_data_arg_set.clear();
    m_dut_binning_data_set.clear();

    for (auto &idutdataset : m_dut_data_set) {
        ClearDutDataSet(idutdataset.first);
    }
    m_dut_data_var.clear();
    m_dut_data_category.clear();
    m_dut_data_arg_set.clear();
    m_dut_data_set.clear();

}
////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::ClearDutDataSet(std::string name){

    if(Includes<std::string>(m_dut_binning_data_set,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRooFitUtils:: Removing the set of RooDataSet DUT binning data: "
            << name << " ("<< m_dut_binning_data_set.at(name).size()<< " RooDataSets)"<< std::endl;

        for(auto& ival:m_dut_binning_data_var.at(name))      delete ival;
        if(Includes<std::string>(m_dut_binning_data_category,name))
            {for (auto &ival:m_dut_binning_data_category.at(name)) delete ival;}
        for(auto& ival:m_dut_binning_data_arg_set.at(name))  delete ival;
        for(auto& ival:m_dut_binning_data_set.at(name))      delete ival;
    }

    if(Includes<std::string>(m_dut_data_set,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRooFitUtils:: Removing the RooDataSet DUT data: " << name << std::endl;
        delete m_dut_data_var.at(name);
        if(Includes<std::string>(m_dut_data_category,name))
            delete m_dut_data_category.at(name);
        delete m_dut_data_arg_set.at(name);
        delete m_dut_data_set.at(name);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
RooDataSet* DutRooFitUtils::DutDataSetPtr(std::string name, unsigned dutBinNumber){
    if(Includes<std::string>(m_dut_binning_data_set,name)) {
        if(m_dut_binning_data_set.at(name).size()>dutBinNumber)
            return m_dut_binning_data_set.at(name).at(dutBinNumber);
        else{
            std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooDataSet which do not exists:"
                         " RooDataSet set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooDataSet which do not exists:"
                     " RooDataSet set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
    return nullptr; // to make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
RooDataSet* DutRooFitUtils::DutDataSetPtr(std::string name){
    if(Includes<std::string>(m_dut_data_set,name)) {
        return m_dut_data_set.at(name);
    }
    else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooDataSet which do not exists:"
                     " RooDataSet: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
    return nullptr; // to make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
RooCategory* DutRooFitUtils::DutDataSetRCatPtr(std::string name, unsigned dutBinNumber){
    if(Includes<std::string>(m_dut_binning_data_category,name)) {
        if(m_dut_binning_data_category.at(name).size()>dutBinNumber)
            return m_dut_binning_data_category.at(name).at(dutBinNumber);
        else{
            std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooCategory which do not exists:"
                         " RooCategory set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooCategory which do not exists:"
                     " RooCategory set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    return nullptr; // make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
RooCategory* DutRooFitUtils::DutDataSetRCatPtr(std::string name){
    if(Includes<std::string>(m_dut_data_category,name)) {
        return m_dut_data_category.at(name);
    }
    else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooCategory which do not exists:"
                     " RooCategory: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
    return nullptr; // make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
RooRealVar* DutRooFitUtils::DutDataSetRRVarPtr(std::string name, unsigned dutBinNumber){
    if(Includes<std::string>(m_dut_binning_data_var,name)) {
        if(m_dut_binning_data_var.at(name).size()>dutBinNumber)
            return m_dut_binning_data_var.at(name).at(dutBinNumber);
        else{
            std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooRealVar which do not exists:"
                         " RooRealVar set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return m_dut_binning_data_var.at(name).at(0); // to make compiler happy
        }
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooRealVar which do not exists:"
                     " RooRealVar set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    return m_dut_binning_data_var.begin()->second.at(0); // make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
RooRealVar* DutRooFitUtils::DutDataSetRRVarPtr(std::string name){
    if(Includes<std::string>(m_dut_data_var,name)) {
        return m_dut_data_var.at(name);
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to extract DUT bin RooRealVar which do not exists:"
                     " RooRealVar: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    return m_dut_data_var.begin()->second; // make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::AddToDutDataSet(std::string name, unsigned dutBinNumber){

    if(Includes<std::string>(m_dut_binning_data_set,name)) {
        if(m_dut_binning_data_set.at(name).size()>dutBinNumber){
        auto rrvar = DutDataSetRRVarPtr(name,dutBinNumber);
        auto rcat = DutDataSetRCatPtr(name,dutBinNumber);
        m_dut_binning_data_set.at(name).at(dutBinNumber)->add(RooArgSet(*rrvar,*rcat));
        }
        else{
            std::cout << "[ERROR]:: DutRooFitUtils::Trying to add event to the DUT bin which do not exists:"
                         " RooDataSet set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
        }
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to add event to the set of DUT RooDataSet which do not exists:"
                     " RooDataSet set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRooFitUtils::AddToDutDataSet(std::string name){

    if(Includes<std::string>(m_dut_data_set,name)) {
        auto rrvar = DutDataSetRRVarPtr(name);
        auto rcat = DutDataSetRCatPtr(name);
        m_dut_data_set.at(name)->add(RooArgSet(*rrvar,*rcat));
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to add event to the set of DUT RooDataSet which do not exists:"
                     " RooDataSet: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<RooDataSet*>* DutRooFitUtils::DutBinningRDSVectorPtr(std::string name){
    if(Includes<std::string>(m_dut_binning_data_set,name)) {
        return &m_dut_binning_data_set.at(name);
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to get set of DUT RooDataSet which do not exists:"
                     " RooDataSet set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<RooRealVar*>* DutRooFitUtils::DutBinningRRVVectorPtr(std::string name){
    if(Includes<std::string>(m_dut_binning_data_var,name)) {
        return &m_dut_binning_data_var.at(name);
    } else{
        std::cout << "[ERROR]:: DutRooFitUtils::Trying to get set of DUT RooRealVar which do not exists:"
                     " RooDataSet set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

