//
// Created by brachwal on 04.02.19.
//

#include "TbGraphMaker.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "TbGaudi.h"
#include "TGraphErrors.h"

////////////////////////////////////////////////////////////////////////////////////
///
TbGraphMaker::TbGraphMaker(TbJobSharedPtr tbrun)
    : m_tbjob(tbrun) {
    m_tbresults  = TbResults::GetInstance();
}
