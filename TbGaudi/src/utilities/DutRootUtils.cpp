//
// Created by brachwal on 11.10.18.
//

#include "DutRootUtils.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "Dut.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbAnalysis.h"
#include "Globals.h"
#include "TH1D.h"
#include "TCanvas.h"


bool DutRootUtils::m_dumpBinningHistToNTuple = false;
HistUnit DutRootUtils::m_hist_unit = HistUnit::PitchSize;
////////////////////////////////////////////////////////////////////////////////////
///
DutRootUtils::DutRootUtils()
    : tbResults(TbResults::GetInstance())
{}

////////////////////////////////////////////////////////////////////////////////////
///
DutRootUtils::~DutRootUtils() {
    if(!m_dut_binning_data_hist.empty() ||
       !m_dut_data_hist.empty() ||
       !m_dut_data_2dhist.empty()) {
        std::cout << "[INFO]:: DutRootUtils:: Release memory. You can consider"
                  << " to call ClearEventLoopCollectedData() within your TbJobs loop."<<std::endl;
        ClearAllDutHist();
    }
    for(auto can : m_tcanvas) delete can;
    m_tcanvas.clear();

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::DefineDutBinningHist(std::string name, unsigned nBinsX, double xLow, double xUp){
    if(!Includes<std::string>(m_dut_binning_data_hist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRootUtils:: Defining new set of TH1D DUT binning data histograms: " << name << std::endl;
        m_dut_binning_data_hist[name] = std::vector<TH1D*>{};

        // TODO: refactor after classes hierarchy changed
        //TODO: auto nDutBins = m_dut->NBins();
        auto nDutBins = TbJob::CurrentTbJob()->GetConfig<unsigned>("DutNBins");

        for (unsigned i = 0; i < nDutBins; ++i) {
            auto hname = "TH1D_"+name+"_Bin"+itos(i);
            auto htitle = name+" DUT #bin "+itos(i)+";"+name+";Events";
            TH1D* hist = new TH1D(TString(hname),TString(htitle),nBinsX,xLow,xUp);
            m_dut_binning_data_hist.at(name).push_back(hist);
            m_dut_binning_data_hist.at(name).back()->SetDirectory(0); // have to release this object!
        }
    }
    else{
        std::cout << "[ERROR]::DutRootUtils::DefineDutBinningDataHist: The specified set of histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::ClearDutBinningHist(std::string name){
    if(TbGaudi::Debug())
        std::cout << "[INFO]:: DutRootUtils:: Removing the set of TH1D DUT binning data histograms: " << name << std::endl;

    if(Includes<std::string>(m_dut_binning_data_hist,name)){
        for(auto& ihist:m_dut_binning_data_hist.at(name)) delete ihist;
        m_dut_binning_data_hist.erase(name);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* DutRootUtils::DutHistPtr(std::string name, unsigned dutBinNumber){
    auto nDutBins = m_dut->NBins();
    if(nDutBins == 1){
        std::cout << "[WARNING]:: DutRootUtils::Trying to extract DUT bin histogram which do not exists; "
                     "The DUT is not binned (NBins=1). Trying to find single DutHist for"<< name << std::endl;
        return DutHistPtr(name);
    }
    if(Includes<std::string>(m_dut_binning_data_hist,name)) {
        if(m_dut_binning_data_hist.at(name).size()>dutBinNumber)
            return m_dut_binning_data_hist.at(name).at(dutBinNumber);
        else{
            std::cout << "[ERROR]:: DutRootUtils::Trying to extract DUT bin histogram which do not exists:"
                         " Histogram set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: DutRootUtils::Trying to extract DUT bin histogram which do not exists:"
                     " Histogram set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* DutRootUtils::DutHistPtr(std::string name){
    if(Includes<std::string>(m_dut_data_hist,name)) {
        return m_dut_data_hist.at(name);
    }
    else{
        std::cout << "[ERROR]:: DutRootUtils::Trying to extract DUT histogram which do not exists:"
                     " Histogram: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::DefineDutHist(std::string name, unsigned nBinsX, double xLow, double xUp){
    if(!Includes<std::string>(m_dut_data_hist,name)) {
        if (TbGaudi::Debug())
            std::cout << "[INFO]:: DutRootUtils:: Defining new TH1D DUT binning data histogram: " << name << std::endl;
        auto hname = "TH1D_" + name;
        auto htitle = name + " DUT ;" + name + ";Events";
        TH1D *hist = new TH1D(TString(hname), TString(htitle), nBinsX, xLow, xUp);
        m_dut_data_hist[name] = hist;
        m_dut_data_hist.at(name)->SetDirectory(0); // have to release this object!
    }
    else{
        std::cout << "[ERROR]::DutRootUtils::DefineDutBinningDataHist: The specified set of histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::Define2DDutHist(std::string name,
                                unsigned nBinsX, double xLow, double xUp,
                                unsigned nBinsY, double yLow, double yUp){

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::SetDutBinContent2DHist(std::string name, unsigned x, unsigned y, double value){

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::SetDutBinContent2DHist(std::string name, unsigned dutBin, double value){

}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::ClearAllDutHist(){
    // Remove binning related histograms
    std::cout << "[INFO]:: DutRootUtils:: Number of DUT binning data histograms to be deleted "
              << m_dut_binning_data_hist.size()*m_dut_binning_data_hist.begin()->second.size() << std::endl;
    for(auto iduthist: m_dut_binning_data_hist)
        ClearDutBinningHist(iduthist.first);
    m_dut_binning_data_hist.clear();
    // Remove general histograms
    std::cout << "[INFO]:: DutRootUtils:: Number of DUT data histograms (TH1D) to be deleted " << m_dut_data_hist.size() << std::endl;
    for(auto iduthist: m_dut_data_hist){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRootUtils:: Removing the TH1D DUT data histogram: " << iduthist.first << std::endl;
        delete iduthist.second;
    }
    m_dut_data_hist.clear();
    std::cout << "[INFO]:: DutRootUtils:: Number of DUT data histograms (TH2D) to be deleted " << m_dut_data_2dhist.size() << std::endl;
    for(auto iduthist: m_dut_data_2dhist){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: DutRootUtils:: Removing the TH2D DUT data histogram: " << iduthist.first << std::endl;
        delete iduthist.second;
    }
    m_dut_data_2dhist.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutRootUtils::WriteEventLoopHistToNTuple(std::string name){
    TbGaudi::PrintBanner("INFO","DutRootUtils::Exporting histograms to .root files.");
    auto tbJob = TbJob::CurrentTbJob();
    auto dutNBins = tbJob->GetConfig("DutNBins");
    auto idutNBins = std::stoi(dutNBins);
    if(idutNBins>1) {
        if (!Includes<std::string>(m_dut_binning_data_hist, name)) {
            std::cout << "[ERROR]:: DutRootUtils::Trying to export the set of DUT binning histograms "
                         " which do not exists: " << name << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
        }
    } else {
        if (!Includes<std::string>(m_dut_data_hist, name)) {
            std::cout << "[ERROR]:: DutRootUtils::Trying to export the set of DUT binning histogram "
                         " which do not exists: " << name << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
        }
    }
    std::string path = TbAnalysis::OutputLocation();
    if(path.at(path.length()-1)=='/') path+=currentDate();
    else                        path+="/"+currentDate();
    namespace fs = boost::filesystem;
    fs::path dp (path);
    if(!fs::exists (dp)){
        std::cout << "[INFO]:: DutRootUtils:: Created output directory "<<std::endl;
        std::cout << "[INFO]:: DutRootUtils:: "<< path <<std::endl;
        fs::create_directories(dp);
    }
    auto dutName = tbJob->GetConfig("DutName");
    auto dutVoltage = tbJob->GetConfig("RunBiasVoltage");
    auto runNumber = tbJob->GetConfig("RunNumber");
    auto dutBinning = tbJob->GetConfig("DutBinningType");
    auto clSize = tbJob->GetConfig("RunClusterSize");
    std::string fname;
    fname = dutName+"_"+runNumber+"_"+dutVoltage+"V_clS"+clSize;
    if(idutNBins>1)
        fname += "_"+dutBinning+dutNBins+"_DUTbinningHist.root";
    else
        fname += name+"_EventLoopHist.root";

    fs::path fp = path+"/"+fname;
    if(fs::exists(fp)){
        std::cout << "[INFO]:: DutRootUtils:: Remove existing results: " << fname <<std::endl;
        fs::remove(fp);
    }
    TFile* outfile = new TFile(TString(path+"/"+fname), "RECREATE");
    TList* hList = new TList();// list of histograms to store
    if(idutNBins>1) {
        for (auto &hist : m_dut_binning_data_hist.at(name)) hList->Add(hist);
    }
    else {
        hList->Add(m_dut_data_hist.at(name));
    }
    hList->Write();
    outfile->Close();
    std::cout << "[INFO]:: DutRootUtils:: Created " << fname <<std::endl<<std::endl;
    delete hList;
    delete outfile;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TH1D*>* DutRootUtils::DutBinningTH1VectorPtr(std::string name){
    if(Includes<std::string>(m_dut_binning_data_hist,name)) {
        return &m_dut_binning_data_hist.at(name);
    } else{
        std::cout << "[ERROR]:: DutRootUtils::Trying to get set of DUT TH1D which do not exists:"
                     " TH1D set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutRootUtils::TH2Xup()const{

    switch (DutRootUtils::GetHistUnit()){
        case HistUnit::PitchSize:
            if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
                return static_cast<double>(Dut::NStrips()); // LHCb UT like
            else
                return static_cast<double>(Dut::NPixelsX()); // LHCb VeloPix like
         case HistUnit::DutSize:
            return m_dut->DutSizeX();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutRootUtils::TH2Yup()const{

    switch (DutRootUtils::GetHistUnit()){
        case HistUnit::PitchSize:
            if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
                return static_cast<double>(1); // LHCb UT like
            else
                return static_cast<double>(Dut::NPixelsY()); // LHCb VeloPix like
        case HistUnit::DutSize:
            return m_dut->DutSizeY();
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutRootUtils::TH2NBinsX()const{
    auto run = TbJob::CurrentTbJob();
    if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
        return m_dut->NBins(); //Dut::NStrips(); // LHCb UT like
    else
        return sqrt(m_dut->NBins()); //Dut::NPixelsX(); // LHCb VeloPix like
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutRootUtils::TH2NBinsY()const{
    if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
        return 1; // LHCb UT like
    else
        return sqrt(m_dut->NBins());//Dut::NPixelsY(); // LHCb VeloPix like
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutRootUtils::TH2PitchNBinsX()const{
    if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
        return Dut::NStrips(); // LHCb UT like
    else
        return Dut::NPixelsX(); // LHCb VeloPix like
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutRootUtils::TH2PitchNBinsY()const{
    if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
        return 1; // LHCb UT like
    else
        return Dut::NPixelsY(); // LHCb VeloPix like
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutRootUtils::TH2AxesTitles() const{
    switch (DutRootUtils::GetHistUnit()){
        case HistUnit::PitchSize:
            if(m_dut->IsStripBinning() && !DutStripBinning::Is2DimMapping())
                return ";Strip Number;a.u."; // LHCb UT like
            else
                return ";column;row"; // LHCb VeloPix like

        case HistUnit::DutSize:
            return ";[mm];[mm]";
    }
}