//
// Created by brachwal on 04.02.19.
//

#include "TbHistMaker.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "TbGaudi.h"
#include "TH2D.h"

////////////////////////////////////////////////////////////////////////////////////
///
TbHistMaker::TbHistMaker(TbJobSharedPtr tbrun)
    : m_tbjob(tbrun) {
    m_tbresults  = TbResults::GetInstance();
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbHistMaker::DefineNewTH2D(const char* name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp) const {

    //TODO: auto dutName = m_dut->Name();
    auto sname = std::string(name);
    auto dutName = m_tbjob->GetConfig("DutName");
    auto runNumber = m_tbjob->GetConfig("RunNumber");
    //std::string hname = "DutBinStat_" + dutName + "_" + runNumber;
    std::string hname = sname+"_" + dutName + "_" + runNumber;
    std::string htitle = sname + " " + dutName + " " + runNumber;
    htitle += m_tbjob->TH2AxesTitles();

    auto hist = std::make_shared<TH2D>(TString(hname),TString(htitle),nX,xLow,xUp,nY,yLow,yUp);
    hist->SetDirectory(0);
    return hist;

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbHistMaker::MakeDutBinningStatisticHist(bool draw) const {

    if(!m_tbjob->DutPtr()->IsBinned()) return;
    auto dutBinning = m_tbjob->DutPtr()->DutBinningPtr();
    if(dutBinning->IsBinningStatFilled()){
        auto isDutPitchMapping = m_tbjob->DutPtr()->IsEllipticBinning() || DutStripBinning::Is2DimMapping();

        unsigned nX  = isDutPitchMapping ? m_tbjob->TH2PitchNBinsX() : m_tbjob->TH2NBinsX();
        double xUp = m_tbjob->TH2Xup();

        unsigned nY  = isDutPitchMapping ? m_tbjob->TH2PitchNBinsY() : m_tbjob->TH2NBinsY();
        double yUp = m_tbjob->TH2Yup();

        // Create histogram
        auto hist = DefineNewTH2D("DutBinningStatistics",nX,0.,xUp,nY,0.,yUp);

        // Fill the histogram with the data stored in the DutBin
        // Note: this is correct if User called Dut::CountDutBinHit() within the event loop
        std::cout << "[INFO]:: TbHistMaker:: Filling DUT binning statistic TH2D histogram..." << std::endl;
        for (unsigned x = 0; x < nX; x++) {
            for (unsigned y = 0; y < nY; y++) {
                auto binNumber = isDutPitchMapping ? dutBinning->GetBinNumber(x, y) : x+y;
                if (binNumber >= 0) {
                    auto dutBin = dutBinning->DutBins().at(binNumber);
                    int binStat = dutBin->Statistic();
                    hist->SetBinContent(x + 1, y + 1, binStat);
                    hist->SetBinError(x + 1, y + 1, 0.0001);
                }
            }
        }
        AddToTbResults<TH2D>(hist,draw);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
// TODO: refactor using DefineNewTH2D and AddToTbResults after the TbReuslts plotting
// will be unblocked from the usage of const hist names, e.g. "DutBinningStatistics"
void TbHistMaker::MakeDutPixelStatisticHist(bool draw) const {

    auto dutBinning = m_tbjob->DutPtr()->DutBinningPtr();
    if(dutBinning->IsPixelStatFilled()){
        std::string name = "DutPixelStatistics";
        auto dutName = m_tbjob->GetConfig("DutName");
        auto runNumber = m_tbjob->GetConfig("RunNumber");
        std::string hname = "DutPixStat_"+dutName+"_"+runNumber;
        std::string htitle = "Dut pixels statistic (hit map) "+dutName+" "+runNumber;
        htitle += m_tbjob->TH2AxesTitles();

        unsigned nX = m_tbjob->TH2PitchNBinsX();
        double xLow = 0.;
        double xUp = m_tbjob->TH2Xup();

        unsigned nY = m_tbjob->TH2PitchNBinsY();
        double yLow = 0.;
        double yUp = m_tbjob->TH2Yup();

        // Create histogram
        auto hist = std::make_shared<TH2D>(TString(hname),TString(htitle),nX,xLow,xUp,nY,yLow,yUp);
        hist->SetDirectory(0);

        // Fill the histogram with the data stored in the DutBin
        // Note: this is correct if User called Dut::CountDutBinHit() within the event loop
        std::cout << "[INFO]:: TbHistMaker:: Filling DUT pixel map statistic TH2D histogram..."<<std::endl;
        auto pixelBinningCounter = dutBinning->PixelBinningCounter();
        for (unsigned x = 0; x < nX; x++) {
            for (unsigned y = 0; y < nY; y++) {
                int binStat = pixelBinningCounter.at(x).at(y);
                hist->SetBinContent(x + 1, y + 1, binStat);
                hist->SetBinError(x + 1, y + 1, 0.0001);
            }
        }
        m_tbresults->AddMappedResult<TH2D>(m_tbjob,name,static_cast<TH2D*>(hist->Clone()));
        std::cout << "[INFO]:: TbHistMaker:: "<<name<<" TH2D histogram added to TbResults."<<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbHistMaker::MakeDutStripStatisticHist(bool draw) const {

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbHistMaker::MakeDutIntraPixelStatisticHist(bool draw) const {

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbHistMaker::MakeDutIntraStripStatisticHist(bool draw) const {

}
