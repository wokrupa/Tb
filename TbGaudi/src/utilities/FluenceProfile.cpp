//
// Created by brachwal on 28.11.18.
//

#include "FluenceProfile.h"
#include "IRRADProfile.h"
#include "TbGaudi.h"
#include "Dut.h"

FluenceUniqueProfilesMap FluenceProfile::m_unique_fluence_profiles=FluenceUniqueProfilesMap();
////////////////////////////////////////////////////////////////////////////////////
///
FluenceProfile::FluenceProfile(std::string name)
        : m_dut_binning(nullptr)
        , m_total_fluence(0.)
        , m_name(name)
        , m_isDefault(false)
        , m_isInitialized(false)
{}

////////////////////////////////////////////////////////////////////////////////////
///
#include "TbJob.h"
FlnProfileShPtr FluenceProfile::AddProfile(DutBinning* dutBinning, FluenceProfile* newProfile){

    newProfile->SetDutBinning(dutBinning);
    //auto name = dutBinning->OwnerDut()->DutLabel();
    auto name = TbJob::CurrentTbJob()->DutLabel();

    // check the available implementations
    if(name.find("IRRAD")==std::string::npos){
        std::cout << "[ERROR]:: FluenceProfile:: AddProfile:: Trying to register the fluence profile which is not implemented:\n"
                     "\tFluenceProfile name: "<< name << std::endl;
        std::cout << "[INFO]:: Available profiles: \"IRRAD\", \"KIT\"(to be implemented), \"JSI\"(to be implemented)."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    if(Includes<std::string>(FluenceProfile::MapOfUniqueProfiles(),name)) {
        std::cout << "[INFO]:: FluenceProfile:: AddProfile::"
                  << " The specified fluence profile already exists in the map: " << name << std::endl;
        delete newProfile; // there is no need to keep this alive.
    }
    else {
        FluenceProfile::MapOfUniqueProfiles()[name] = std::shared_ptr<FluenceProfile>(newProfile);
        // FluenceProfile::MapOfUniqueProfiles().at(name)->Initialize();
        // TODO:: FluenceProfile::MapOfUniqueProfiles().at(name)->ComputeFluenceMapping();
    }
    return FluenceProfile::m_unique_fluence_profiles.at(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
void FluenceProfile::SetDutBinning(DutBinning* ptr){
    if(!m_dut_binning.get()) m_dut_binning=std::shared_ptr<DutBinning>(ptr);
}

////////////////////////////////////////////////////////////////////////////////////
///
FlnProfileShPtr FluenceProfile::GetPointer(std::string name) {
    if (Includes<std::string>(FluenceProfile::m_unique_fluence_profiles, name))
        return FluenceProfile::m_unique_fluence_profiles.at(name);
    else {
        std::cout << "[ERROR]:: FluenceProfile:: GetPointer:: Trying to get the fluence profile which was not created:\n"
                     "\tname: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
#include "TbJob.h"
std::string FluenceProfile::LinkedDutName() const {
    return TbJob::CurrentTbJob()->GetConfig("DutName");
}