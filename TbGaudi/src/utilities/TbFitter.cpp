#include "TbFitter.h"
#include "TbJob.h"
#include "TbResults.h"

// ROOT libriaryies
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"


unsigned TbFitter::m_roofit_binning = 50000;
unsigned TbFitter::m_nentries_fit_threshold = 20;

////////////////////////////////////////////////////////////////////////////////////
//
TbFitter::TbFitter()
	: m_dut_binning_th1_data(nullptr)
	, m_dut_binning_rds_data(nullptr)
	, m_dut_binning_rrv_variable(nullptr)
	, m_tbjob(nullptr)
{
    // set defaults:
    m_fit_data_type = FitDataType::RooDataSet;
    m_tbResults = TbResults::GetInstance();
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::SetData(TbJobSharedPtr tbJob, std::string dataName, FitDataType dataType){
    SetFitDataType(dataType);
    LinkTbJobData(dataName,tbJob);
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::LinkTbJobData(std::string name, TbJobSharedPtr tbrun){
    m_tbjob = tbrun;
    m_data_name = name;
    switch (m_fit_data_type){
        case FitDataType::RooDataSet:
            InitializeRooDataSet();
            break;
        case FitDataType::TH1:
            InitializeTH1Data();
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::InitializeRooDataSet(){
    if(DutNBins() == 1) {
        m_dut_rds_data = m_tbjob->DutDataSetPtr(m_data_name);
        m_dut_rrv_variable = m_tbjob->DutDataSetRRVarPtr(m_data_name);
        return;
    }

    if(m_good_dataset.empty()) {
        m_dut_binning_rds_data = m_tbjob->DutBinningRDSVectorPtr(m_data_name);
        m_dut_binning_rrv_variable = m_tbjob->DutBinningRRVVectorPtr(m_data_name);
    }
    else{
        TbGaudi::PrintBanner("INFO", "TbFitter:: NEntriesFitThreshold has been changed, perform new mapping...");
        m_good_dataset.clear();
    }

    if (m_dut_binning_rds_data && !m_dut_binning_rds_data->empty()) {
        unsigned good_data_idx(0);
        for (int i = 0; i < m_dut_binning_rds_data->size(); ++i){
            if(IsGoodDataSet(i)){
                m_good_dataset[good_data_idx++] = i;
                std::cout<<"[DEBUG]:: TbFitter:: Good RDS mapping: bin "
                         <<  itos(i)<<" is above threshold: "
                         <<  m_dut_binning_rds_data->at(i)->sumEntries()<< " > "
                         <<  TbFitter::NEntriesFitThreshold() << std::endl;
            }
            else{
                std::cout<<"[DEBUG]:: TbFitter:: Good RDS mapping: bin "
                         <<  itos(i)<<" is below threshold: "
                         <<  m_dut_binning_rds_data->at(i)->sumEntries()<< " < "
                         <<  TbFitter::NEntriesFitThreshold() << std::endl;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::InitializeTH1Data(){
    if(DutNBins() == 1) {
        m_dut_th1_data = m_tbjob->DutHistPtr(m_data_name);
        return;
    }
    if(m_good_dataset.empty())
        m_dut_binning_th1_data = m_tbjob->DutBinningTH1VectorPtr(m_data_name);
    else {
        TbGaudi::PrintBanner("INFO", "TbFitter:: NEntriesFitThreshold has been changed, perform new mapping...");
        m_good_dataset.clear();
    }
    if (m_dut_binning_th1_data && !m_dut_binning_th1_data->empty()) {
        unsigned good_data_idx(0);
        for (int i = 0; i < m_dut_binning_th1_data->size(); ++i){
            if(IsGoodTHist(i)){
                m_good_dataset[good_data_idx++] = i;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//
size_t TbFitter::NGoodDataSets() const {
    unsigned n(0);
    if(m_dut_binning_th1_data && !m_dut_binning_th1_data->empty()){
        for (int i = 0; i < m_dut_binning_th1_data->size(); ++i){
            if(IsGoodTHist(i)) ++n;
        }
        return n;
    }

    else if(m_dut_th1_data){
        if (IsGoodTHist()) return 1;
    }

    else if (m_dut_binning_rds_data && !m_dut_binning_rds_data->empty()) {
        for (int i = 0; i < m_dut_binning_rds_data->size(); ++i){
            if(IsGoodDataSet(i)) ++n;
        }
        return n;
    }

    else if (m_dut_rds_data) {
        if(IsGoodDataSet()) return 1;
    }

    else return 0;
}


////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodTHist(unsigned bin) const {
    if(m_dut_binning_th1_data && m_dut_binning_th1_data->size()>bin) {
        if (m_dut_binning_th1_data->at(bin)->GetEntries() > TbFitter::NEntriesFitThreshold())
            return true;
        else
            return false;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodTHist() const {
    if(m_dut_th1_data) {
        if (m_dut_th1_data->GetEntries() > TbFitter::NEntriesFitThreshold())
            return true;
        else
            return false;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodDataSet(unsigned bin) const {
    // TODO: inlcude RooCategory in sumEntries evaluation?!
    if(m_dut_binning_rds_data && m_dut_binning_rds_data->size()>bin) {
        if (m_dut_binning_rds_data->at(bin)->sumEntries() > TbFitter::NEntriesFitThreshold()) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodDataSet() const {
    // TODO: inlcude RooCategory in sumEntries evaluation?!
    if(m_dut_rds_data) {
        if (m_dut_rds_data->sumEntries() > TbFitter::NEntriesFitThreshold()) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
TH1D* TbFitter::ITHistPtr(unsigned bin) {
    if(NGoodDataSets()!=m_good_dataset.size())  //  mapping was done for different threshold
        InitializeTH1Data();                    // initialize again...
    if(m_dut_binning_th1_data && m_good_dataset.size()>bin)
        return m_dut_binning_th1_data->at(m_good_dataset.at(bin));
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
//
RooDataSet* TbFitter::IRooDataSetPtr(unsigned bin) {
    if(NGoodDataSets()!=m_good_dataset.size())  // mapping was done for different threshold
        InitializeRooDataSet();                 // initialize again...
    if(m_dut_binning_rds_data && m_good_dataset.size()>bin) {
        return m_dut_binning_rds_data->at(m_good_dataset.at(bin));
    }
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
//
RooRealVar* TbFitter::IRooRealVarPtr(unsigned bin) {
    if(NGoodDataSets()!=m_good_dataset.size())  // initialized mapping for different threshold
        InitializeRooDataSet();                 // initialize again...
    if(m_dut_binning_rrv_variable && m_good_dataset.size()>bin)
        return m_dut_binning_rrv_variable->at(m_good_dataset.at(bin));
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
//
double TbFitter::TbVoltage() const {
    return m_tbjob->GetConfig<double>("RunBiasVoltage");
}

////////////////////////////////////////////////////////////////////////////////////
//
unsigned TbFitter::DutNBins() const {
    return m_tbjob->GetConfig<unsigned>("DutNBins");
}

////////////////////////////////////////////////////////////////////////////////////
//
std::string TbFitter::FitLabel() const {
    auto label = m_tbjob->GetConfig("DutName");
    label+="_"+m_data_name+"Fit";
    label+="_"+m_tbjob->GetConfig("RunNumber");
    label+="_"+m_tbjob->GetConfig("RunBiasVoltage");
    return label;
}

////////////////////////////////////////////////////////////////////////////////////
//
unsigned TbFitter::DutBin(unsigned goodDataIdx) const {
    return m_good_dataset.at(goodDataIdx);
}