#include "TbResults.h"
#include "TbGaudi.h"
#include "TbStyle.h"

tSeverity TbGaudi::m_Severity = tSeverity::Silent;
bool TbGaudi::m_BATCH = false;
std::string TbGaudi::m_project_location = PROJECT_LOCATION_PATH;


////////////////////////////////////////////////////////////////////////////////////
///
TbGaudi::TbGaudi() {

  std::cout<<"/////////////////////////////////////////////////////////"<<std::endl;
  std::cout<<"///   TbGaudi - test beam analysis framework.         ///"<<std::endl;
  std::cout<<"///   Date: " << currentDateTime() << "                       ///"<< std::endl;
  std::cout<<"/////////////////////////////////////////////////////////\n"<<std::endl;

  m_style = TbStyle::GetInstance();
}
////////////////////////////////////////////////////////////////////////////////////
///
void TbGaudi::PrintBanner(std::string type, std::string text){

  std::cout<<std::endl;
  std::cout<<"************************************************************"<<std::endl;
  std::cout<<"["<<type<<"]:: "<< text <<std::endl;
  std::cout<<"************************************************************"<<std::endl;
  std::cout<<std::endl;

}
