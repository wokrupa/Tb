//
// Created by brachwal on 08.10.18.
//

#include "Globals.h"
#include "RootData.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TbGaudi.h"
#include <memory>

bool RootData::m_use_RDataFrame = false;

////////////////////////////////////////////////////////////////////////////////////
///
RootData::RootData(const std::string ntuple)
        : m_NTuple(ntuple)
        , m_TFile(nullptr)
{
    m_TFile = new TFile(TString(m_NTuple.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
///
RootData::RootData(const std::string ntuple, std::map<std::string,std::vector<std::string>> ttree_tbranches)
    : m_NTuple(ntuple)
    , m_TFile(nullptr)
    , m_TTree_TBranches(ttree_tbranches)
{
    //ROOT::EnableImplicitMT();
    for(auto& itree : m_TTree_TBranches){
        std::string rdf_Name = "RDF"+TTreeName(itree.first);
        if(TbGaudi::Verbose() || TbGaudi::Debug())
            std::cout << "[INFO]:: RootData:: Creating RDataFrame: "<< rdf_Name <<std::endl;
        m_RDataFrames[rdf_Name] = new ROOT::RDataFrame{itree.first,m_NTuple,itree.second};
    }

    m_TFile = new TFile(TString(m_NTuple.c_str()));
    LinkTTrees();
}

////////////////////////////////////////////////////////////////////////////////////
///
RootData::~RootData(){
    for(auto& irdf : m_RDataFrames) delete irdf.second;
    for(auto& ittree : m_TTrees) delete ittree.second;
    delete m_TFile;
}

////////////////////////////////////////////////////////////////////////////////////
///
RDataFrameFiltered* RootData::ApplyFilters(std::string name, std::vector<std::string>* filters){
    if(RootData::UseRDataFrame()){
        for (auto ifilter : *filters)
            ApplyFilter(name, ifilter);
        return m_RDataFramesFiltered.at("RDFF"+name);
    }
    else{
        std::cout << "[ERROR]:: RootData:: ApplyFilter:: Trying to apply filter on RDF while the RootData::UseRDataFrame is OFF"<<std::endl;
        TbGaudi::RunTimeError("Wrong analysis strategy used.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
RDataFrameFiltered* RootData::ApplyFilter(std::string name, std::string filter){
    if(RootData::UseRDataFrame()){
        std::cout << "[INFO]:: RootData:: Employing the filter: " << name << ": " << filter << std::endl;

        if(m_RDataFrames.find("RDF"+name) == m_RDataFrames.end()){
            std::cout << "[ERROR]:: RootData:: ApplyFilter:: There is no correspondig RDataFrame you want filter on:  "<< "RDF"+name <<std::endl;
            TbGaudi::RunTimeError("Extracting no existing key from the std::map");
        }

        if(m_RDataFramesFiltered.find("RDFF"+name) == m_RDataFramesFiltered.end()){
            m_RDataFramesFiltered["RDFF"+name] = new RDataFrameFiltered{m_RDataFrames.at("RDF"+name)->Filter(filter)};
        }
        else { // New filter is added the the previous one.
            m_RDataFramesFiltered.at("RDFF"+name)->Filter(filter);
        }
        return m_RDataFramesFiltered.at("RDFF"+name);
    }
    else{
        std::cout << "[ERROR]:: RootData:: ApplyFilter:: Trying to apply filter on RDF while the RootData::UseRDataFrame is OFF"<<std::endl;
        TbGaudi::RunTimeError("Wrong analysis strategy used.");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
bool RootData::LinkTTrees(){
    for(auto& itree : m_TTree_TBranches) {
        std::string ttree_Name = TTreeName(itree.first);
        if(TbGaudi::Verbose() || TbGaudi::Debug())
            std::cout << "[INFO]:: RootData:: Linking TTree: "<< ttree_Name <<std::endl;
        m_TTrees[ttree_Name] = static_cast<TTree*>(m_TFile->Get(TString(itree.first.c_str())));
        if(!m_TTrees.at(ttree_Name))
            TbGaudi::RunTimeError("Couldn't find TTree "+itree.first);
        //m_TTrees.at(ttree_Name)->SetDirectory(0);

    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool RootData::LinkTTrees(std::vector<std::string> ttrees){
    for(auto& itree : ttrees) {
        std::string ttree_Name = TTreeName(itree);
        m_TTrees[ttree_Name] = static_cast<TTree*>(m_TFile->Get(TString(itree.c_str())));
        if(!m_TTrees[ttree_Name])
            TbGaudi::RunTimeError("Couldn't find TTree "+itree);
        //m_TTrees.at(ttree_Name)->SetDirectory(0);
        std::cout << "[INFO]:: RootData:: TTree is linked: "<< ttree_Name <<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string RootData::TTreeName(std::string ttree){
    if(ttree.find('/')!=std::string::npos)
        return ttree.substr(ttree.find('/')+1);
    else
        return ttree;
}

////////////////////////////////////////////////////////////////////////////////////
///
ROOT::RDataFrame* RootData::RDataFramePtr(std::string name){
    if(m_RDataFrames.find(name) != m_RDataFrames.end()){
        return m_RDataFrames.at(name);
    }
    else {
        std::cout << "[ERROR]::RootData:: You are trying to get access to not existing RDataFrame: "<< name << std::endl;
        ListRDataFrames();
        TbGaudi::RunTimeError("Accessing not existing key in the std::map");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void RootData::ListRDataFrames() const{
    std::cout << "[INFO]::RootData:: Available RDataFrames:"<<std::endl;
    unsigned idxRdf{0};
    unsigned idxCol{0};
    for(auto& irdf : m_RDataFrames){
        std::cout << "[INFO]::RootData:: ["<<idxRdf++<<"] " << irdf.first << " with the columns:" << std::endl;
        std::vector<std::string> colNames = irdf.second->GetColumnNames();
        idxCol=0;
        for(auto& ic:colNames)
            std::cout << "[INFO]::RootData:: [col "<<idxCol++<<"] " << ic << std::endl;

    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TTree* RootData::GetTTree(std::string name){
    std::cout << "[INFO]:: RootData:: GetTTree: "<< name <<std::endl;
    if(Includes<std::string>(m_TTrees,name))
        return m_TTrees.at(name);
    else{
        std::cout << "[ERROR]::RootData:: You are trying to get access to not existing TTree: "
        << name << std::endl;
        std::cout << "[INFO]::RootData:: Available TTrees: "<<std::endl;
        if(m_TTrees.empty()) std::cout << "[INFO]::RootData:: NONE!"<<std::endl;
        unsigned count{};
        for(auto& itree : m_TTrees)
            std::cout << "[INFO]::RootData:: ["<<count++<<"] "<<itree.second->GetName()<<std::endl;
        TbGaudi::RunTimeError("Accessing not existing key in the std::map");
    }
}