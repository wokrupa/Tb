#include "TbAnalysis.h"
#include "TbJobCollector.h"
#include "TbJob.h"
#include "TbResults.h"
#include <memory>

////////////////////////////////////////////////////////////////////////////////////
std::string TbAnalysis::m_db_file = "None";
std::map<std::string, std::vector<std::string>> TbAnalysis::m_rdf_filters;
std::string TbAnalysis::m_output_location = "None";

////////////////////////////////////////////////////////////////////////////////////
///
TbAnalysis::TbAnalysis()
    : m_tbGaudi(TbGaudi::GetInstance())
    , m_job_collector(TbJobCollector::GetInstance())
{
    TbAnalysis::OutputLocation(TbGaudi::ProjectLocation()+"/TbResults");
}

////////////////////////////////////////////////////////////////////////////////////
///
TbAnalysis::~TbAnalysis(){}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbAnalysis::LoadTbData(){

    // 1. Perform the User's defined data reading
    //_____________________________________________________________________________
    m_job_collector->LoadData();

    // 2. Perform the User's defined DUT binning initialization for each TbJob
    //_____________________________________________________________________________
    //for(auto& tbrun : TbJobs()) tbrun->CreateDutBinning();

    // 3. Perform the fluence mapping if it's requested by the User.
    //_____________________________________________________________________________
    // TODO:: for(auto& tbrun : TbJobs()) tbrun->PerformFluenceMapping();

    return true;

}
////////////////////////////////////////////////////////////////////////////////////
///
const std::vector<std::shared_ptr<TbJob>>& TbAnalysis::TbJobs(){ return m_job_collector->TbJobs(); }

////////////////////////////////////////////////////////////////////////////////////
///
void TbAnalysis::AddRdfFilter(std::string name, std::string filter){

    if(m_rdf_filters.find(name) == m_rdf_filters.end()){
        std::vector<std::string> vfilter = {filter};
        m_rdf_filters[name] = vfilter;
        return;
    }

    if(std::find(m_rdf_filters.at(name).begin(),m_rdf_filters.at(name).end(), filter) != m_rdf_filters.at(name).end())
        std::cout << "[WARNING]::TbAnalysis::AddRdfFilter:: Filter already present: "<<name << ": "<<filter << std::endl;
    else
        m_rdf_filters.at(name).push_back(filter);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbAnalysis::AddRdfFilters(std::string name, std::vector<std::string> filters){
    if(m_rdf_filters.find(name) != m_rdf_filters.end()) {
        std::cout << "[WARNING]::TbAnalysis::AddRdfFilters:: Filters already present: "<<name << " - being replaced." << std::endl;
        TbAnalysis::ClearRdfFilters(name);
        for (auto &ifilter : filters) m_rdf_filters.at(name).push_back(ifilter);
    }
    else {
        m_rdf_filters[name] = filters;

    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<std::string>* TbAnalysis::RdfFilters(std::string name){
    if(m_rdf_filters.find(name) != m_rdf_filters.end())
        return &(m_rdf_filters.at(name));
    else return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbAnalysis::PrintAllRdfFilters(){
    for (auto& ifilter : m_rdf_filters){
        std::cout << "[INFO]:: RDF Filter: " << ifilter.first << " : " << std::endl;
        for(auto& ival : ifilter.second)
            std::cout << "[INFO]::             " << ival << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbAnalysis::PrintRdfFilters(std::string name){
    if(m_rdf_filters.find(name) != m_rdf_filters.end()) {
        std::cout << "[INFO]:: RDF Filter: " << name << " : " << std::endl;
        for(auto& ival : m_rdf_filters.at(name))
            std::cout << "[INFO]::             " << ival << std::endl;
    } else {
        std::cout << "[ERROR]:: You are trying to get not exisitng RDF Filter: " <<  name << std::endl;
        TbGaudi::RunTimeError("Extracting not existing key from the std::map");
    }
}

