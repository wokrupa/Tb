#!/bin/bash
#############################################
#       SETUP ENVIROMENT ON LXPLUS          #
#############################################

# NOTE: example to be login at:
# ssh -Y niceaccount@lxplus6.cern.ch
#
# HOWTO:
# source setup-slc6-cvmfs.sh

# ===========================================
ARCH=x86_64-slc6
GCC=gcc8
# ===========================================

LCGVIEWVERSION=/cvmfs/sft.cern.ch/lcg/views/LCG_hsf/${ARCH}-${GCC}-opt

GCCVERSION=/cvmfs/sft.cern.ch/lcg/releases/gcc/8.1.0/${ARCH}

ROOTVERSION=/cvmfs/sft.cern.ch/lcg/releases/LCG_hsf/ROOT/6.14.02/${ARCH}-${GCC}-opt

echo "Setup the complete environment for this LCG view"
echo "Config script taken from ${LCGVIEWVERSION}"
source ${LCGVIEWVERSION}/setup.sh
echo

echo "Setup the appropriate ROOT version"
echo "ROOT taken from ${ROOTVERSION}"
source ${ROOTVERSION}/bin/thisroot.sh
echo

echo "Setup the appropriate g++ compiler"
echo "GCC taken from ${GCCVERSION}"
source ${GCCVERSION}/setup.sh
export CC=$(which gcc)
export CXX=$(which g++)
echo

echo "//////////////////////////////////////////////////////////////////"
echo "// Your enviroment is ready for TbGaudi framework - have fun! //"
echo "//////////////////////////////////////////////////////////////////"
echo
